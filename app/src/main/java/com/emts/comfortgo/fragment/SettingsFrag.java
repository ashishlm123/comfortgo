package com.emts.comfortgo.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.LocaleUtils;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class SettingsFrag extends Fragment {
    public static final String LANG_CHINESE = "zh";
    public static final String LANG_MALAYA = "ms";
    public static final String LANG_ENGLISH = "en";
    CardView termsOfServicesMenu;
    TextView tvTermsAndServices;
    RadioGroup rgLang;
    RadioButton rbEnglish, rbMalaya, rbChinese;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        termsOfServicesMenu = view.findViewById(R.id.menu_terms_of_services);
        TextView tvTermsOfServicesMenu = termsOfServicesMenu.findViewById(R.id.tv_menu_title);
        tvTermsOfServicesMenu.setTextSize(16);
        tvTermsOfServicesMenu.setText(getString(R.string.terms_of_services));
        final ImageView ivImg = view.findViewById(R.id.ic_img);
        ivImg.setImageResource(R.drawable.ic_arrow_down);

        tvTermsAndServices = view.findViewById(R.id.tv_terms_of_services);

        termsOfServicesMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvTermsAndServices.getVisibility() == View.GONE) {
                    ivImg.setImageResource(R.drawable.ic_arrow_up);
                    tvTermsAndServices.setVisibility(View.VISIBLE);
                } else {
                    ivImg.setImageResource(R.drawable.ic_arrow_down);
                    tvTermsAndServices.setVisibility(View.GONE);
                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getTermsAndConditions();
        } else {
            AlertUtils.showSnack(getActivity(), termsOfServicesMenu, getResources().getString(R.string.no_internet));
        }

        rgLang = view.findViewById(R.id.rgLang);
        rbEnglish = view.findViewById(R.id.rb_english);
        rbMalaya = view.findViewById(R.id.rb_malay);
        rbChinese = view.findViewById(R.id.rb_chinese);

        rgLang.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_chinese) {
//                    LocaleUtils.setLocale(getActivity().getApplication(), "zh_CN");
                    LocaleUtils.setLocale(getActivity().getApplication(), LANG_CHINESE);
                } else if (i == R.id.rb_malay) {
//                    LocaleUtils.setLocale(getActivity().getApplication(), "ms_MY");
                    LocaleUtils.setLocale(getActivity().getApplication(), LANG_MALAYA);
                } else {
//                    LocaleUtils.setLocale(getActivity().getApplication(), "en_US");
                    LocaleUtils.setLocale(getActivity().getApplication(), LANG_ENGLISH);
                }
            }
        });

        String langCode = LocaleUtils.getLanguage(getActivity());
        Logger.e("Current Locale language ", langCode);
        if (langCode.contains(LANG_CHINESE)) {
            rgLang.check(R.id.rb_chinese);
        } else if (langCode.contains(LANG_MALAYA)) {
            rgLang.check(R.id.rb_malay);
        } else {
            rgLang.check(R.id.rb_english);
        }
    }

    private void getTermsAndConditions() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().termsAndConditionsApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject termsData = res.getJSONObject("terms_data");
                                tvTermsAndServices.setText(Html.fromHtml(termsData.getString("content")));
                            } else {
                                tvTermsAndServices.setText(res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("getTermsAndConditions error ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getTermsAndConditions error ex", e.getMessage() + " ");
                        }
                        tvTermsAndServices.setText(errorMsg);
                    }
                }, "getTermsAndConditions");
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleyHelper.getInstance(getActivity()).cancelRequest("getTermsAndConditions");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        VolleyHelper.getInstance(getActivity()).cancelRequest("getTermsAndConditions");
    }
}
