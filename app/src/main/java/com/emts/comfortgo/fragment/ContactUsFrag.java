package com.emts.comfortgo.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ContactUsFrag extends Fragment {
    EditText edtMessage;
    String helpCenterNo = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edtMessage = view.findViewById(R.id.edt_message_field);

        TextView btnCallSupport = view.findViewById(R.id.btn_call_support);
        btnCallSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + helpCenterNo.trim()));
                startActivity(intent);
            }
        });

        final Button btnSend = view.findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtMessage.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.message_field_required));
                    return;
                }
                if (NetworkUtils.isInNetwork(getActivity()))
                    contactHelpCenterTask(edtMessage.getText().toString().trim());
                else
                    AlertUtils.showSnack(getActivity(), btnSend, getResources().getString(R.string.no_internet));
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getHelpCenterContactNo();
        } else {
            AlertUtils.showSnack(getActivity(), btnSend, getResources().getString(R.string.no_internet));
        }

    }

    private void contactHelpCenterTask(String message) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("message", message);

        vHelper.addVolleyRequestListeners(Api.getInstance().helpCenterApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                edtMessage.setText("");
                                AlertUtils.simpleAlert(getActivity(), getString(R.string.message_sent),
                                        res.getString("message"), getString(R.string.ok), "", null);
                            } else {
                                AlertUtils.simpleAlert(getActivity(), getString(R.string.message_send_failed),
                                        res.getString("message"), getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("contactHelpCenterTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("contactHelpCenterTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), getString(R.string.error), errorMsg, getString(R.string.ok),
                                "", null);
                    }
                }, "contactHelpCenterTask");
    }

    private void getHelpCenterContactNo() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().helpCenterApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
//                                helpCenterNo = res.getString("");
                            }
                        } catch (JSONException e) {
                            Logger.e("getHelpCenterContactNo json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getHelpCenterContactNo error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), getString(R.string.error), errorMsg, getString(R.string.ok),
                                "", null);
                    }
                }, "getHelpCenterContactNo");
    }
}
