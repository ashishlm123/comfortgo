package com.emts.comfortgo.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.R;
import com.emts.comfortgo.activity.LoginActivity;
import com.emts.comfortgo.activity.MainActivity;
import com.emts.comfortgo.activity.ProfileActivity;
import com.emts.comfortgo.activity.TripDayActivity;
import com.emts.comfortgo.adapter.NotificationAdapter;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationFrag extends Fragment {
    ArrayList<NotificationModel> notificationLists;
    NotificationAdapter notificationAdapter;
    RecyclerView rvNotificationListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;
    PreferenceHelper preferenceHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        preferenceHelper = PreferenceHelper.getInstance(getActivity());

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgressBar = view.findViewById(R.id.infinite_progress_bar);
        tvErrorText = view.findViewById(R.id.error_text);

        rvNotificationListings = view.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvNotificationListings.setLayoutManager(linearLayoutManager);

        notificationLists = new ArrayList<>();

        notificationAdapter = new NotificationAdapter(getActivity(), notificationLists);
        notificationAdapter.setOnRecyclerViewItemClickListener(new NotificationAdapter.RecyclerItemClickListener() {
            @Override
            public void onRecyclerItemClickListener(int position) {
                NotificationModel not = notificationLists.get(position);
                if (!not.isSeen()) {
                    readTask(position, not.getNotificationId());
                }
                switch (not.getNotType()) {
                    case NotificationModel.NOT_TYPE_TRIP_CANCEL:
                        //go to my bookings
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        //not really accept booking but this will open mybooking fragment
                        intent.putExtra("acceptBooking", true);
                        startActivity(intent);
                        break;
                    case NotificationModel.NOT_TYPE_TRIP_COMPLETE:
                        Intent intent1 = new Intent(getActivity(), TripDayActivity.class);
                        intent1.putExtra("tripDate", not.getNotificationDate());
                        startActivity(intent1);
                        break;
                    case NotificationModel.NOT_TYPE_UPDATE_APPROVE:
                        Intent intent2 = new Intent(getActivity(), ProfileActivity.class);
                        startActivity(intent2);
                        break;
                    case NotificationModel.NOT_DRIVER_APPROVED:
                        AlertUtils.simpleAlert(getActivity(), getString(R.string.are_you_sure), getString(R.string.login_again_to_refresh_details),
                                getString(R.string.yes), getString(R.string.no),
                                new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        if (isPositiveButton) {
                                            PreferenceHelper.getInstance(getActivity()).edit().clear().apply();
                                            Intent intent3 = new Intent(getActivity(), LoginActivity.class);
                                            intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent3);
                                        }
                                    }
                                });

                        break;
                    case NotificationModel.NOT_TYPE_UPDATE_REJECT:
                        if (getActivity() != null) {
                            if (getActivity() instanceof MainActivity) {
                                ((MainActivity) getActivity()).openContactUsFragment();
                            }
                        }
                        break;
                }
            }
        });
        rvNotificationListings.setAdapter(notificationAdapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            notificationListingTask();
        } else {
            tvErrorText.setText(R.string.no_internet);
            tvErrorText.setVisibility(View.VISIBLE);
            rvNotificationListings.setVisibility(View.GONE);
        }
    }

    private void readTask(final int position, String notificationId) {
        if (!NetworkUtils.isInNetwork(getActivity())) {
            return;
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("notification_id", notificationId);
        postParams.put("action", "read");

        vHelper.addVolleyRequestListeners(Api.getInstance().notReadUnReadApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                notificationLists.get(position).setSeen(true);
                                try {
                                    notificationAdapter.notifyItemChanged(position);
                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("readTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("readTask error ex", e.getMessage() + " ");
                        }
                    }
                }, "readTask");


    }

    private void notificationListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().notificationApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray notArray = res.getJSONArray("all_notifications");
                                for (int i = 0; i < notArray.length(); i++) {
                                    JSONObject eachNot = notArray.getJSONObject(i);
                                    NotificationModel not = new NotificationModel();
                                    not.setNotificationBody(eachNot.getString("notification_msg"));
                                    not.setNotificationDate(eachNot.getString("date"));
                                    not.setNotificationId(eachNot.getString("id"));
                                    not.setSeen(eachNot.getString("status").equals("1"));
                                    not.setNotType(eachNot.getString("notification_code"));

                                    notificationLists.add(not);
                                }

                                notificationAdapter.notifyDataSetChanged();
                                rvNotificationListings.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                                rvNotificationListings.setVisibility(View.GONE);
                            }
                            progressBar.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Logger.e("notificationListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("notificationListingTask error ex", e.getMessage() + " ");
                        }
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                        rvNotificationListings.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }
                }, "notificationListingTask");
    }


}
