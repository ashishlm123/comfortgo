package com.emts.comfortgo.fragment;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingParser;
import com.emts.comfortgo.LocationHelper;
import com.emts.comfortgo.MapProvider;
import com.emts.comfortgo.PassengerNotShowReceiver;
import com.emts.comfortgo.R;
import com.emts.comfortgo.activity.CancelBookingActivity;
import com.emts.comfortgo.activity.DropOffConfirmActivity;
import com.emts.comfortgo.activity.MainActivity;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static com.emts.comfortgo.PassengerNotShowReceiver.PASSENGER_NOT_SHOWN_FOR_BOOKING;

/**
 * Created by User on 2017-08-02.
 */

public class TripFragment extends Fragment {
    public static final int REQUEST_DROP_OFF_CONFIRM = 4532;
    View rootView;
    Booking booking;
    GoogleMap map;
    AlertUtils.OnAlertButtonClickListener listener;
    TextView tvLocation;
    Marker marker;
    Button btnEndTrip;
    Button btnStartTrip, btnCancelTrip;
    private boolean isArrived = false;

    //load progress
    TextView tvError;
    Button btnTryAgain;
    ProgressBar progress;

    View holderNoJob, holderYesJob;

    ProgressDialog pDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pick_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;

        holderNoJob = view.findViewById(R.id.holderNoJob);
        holderYesJob = view.findViewById(R.id.detail);

        tvError = view.findViewById(R.id.error_text);
        btnTryAgain = view.findViewById(R.id.btnRetry);
        progress = view.findViewById(R.id.progress);

        //clear map fragment
        ((FrameLayout) view.findViewById(R.id.map)).removeAllViews();

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkJob();
            }
        });
        checkJob();
    }

    public void checkJob() {
        if (NetworkUtils.isInNetwork(getActivity())) {
            btnTryAgain.setVisibility(View.GONE);
            tvError.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkMyBookingTask();
                }
            }, 2000);
        } else {
            progress.setVisibility(View.GONE);
            tvError.setText(getString(R.string.no_internet));
            tvError.setVisibility(View.VISIBLE);
            btnTryAgain.setVisibility(View.VISIBLE);
        }
    }

    private void checkMyBookingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getMyBookingsApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
//                        if (!isFragmentVisible) {
//                            return;
//                        }
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject bookingObj = res.getJSONObject("booking");
                                booking = BookingParser.getBooking(bookingObj.toString());
                                if (booking.getStatus().equalsIgnoreCase(Booking.TRIP_STATUS_SCHEDULED)) {
                                    goForPickUp();
                                } else {
                                    goForDropOff();
                                }
                            } else {
                                noBooking();
                            }
                        } catch (JSONException e) {
                            Logger.e("checkMyBookingTask json ex", e.getMessage());
                            noBooking();
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("checkMyBookingTask error ex", e.getMessage() + " ");
                        }
                        showBookingError(errorMsg);
                    }
                }, "checkMyBookingTask");
    }

    public void showBookingError(String error) {
        tvError.setText(error);
        tvError.setVisibility(View.VISIBLE);
        btnTryAgain.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
    }

    public void goForPickUp() {
        tvError.setVisibility(View.GONE);
        btnTryAgain.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);

        setUpTripInfo();
    }

    private void setUpTripInfo() {
        LinearLayout llNav = rootView.findViewById(R.id.llNav);
        llNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
                    //navigate to pick up
                    startNavigationActivity(booking.getPickUpLat(), booking.getPickUpLong());
                } else {
                    //navigate to dropoff
                    startNavigationActivity(booking.getDropOffLat(), booking.getDropOffLong());
                }
            }
        });

        tvLocation = rootView.findViewById(R.id.tvLocationName);
        TextView lvlFare = rootView.findViewById(R.id.lvlFare);
        TextView tvFare = rootView.findViewById(R.id.tvFare);
        if (booking.getFareType().equalsIgnoreCase(Booking.FARE_TYPE_FIXED)) {
            lvlFare.setText(getString(R.string.fixed_fare_colon) + " ");
            tvFare.setText(getString(R.string.currency_rm) + " " + booking.getTripFare());
        } else {
            lvlFare.setText(R.string.metered_fare);
        }
        final View holderDetail = rootView.findViewById(R.id.holderDetail);
        TextView btnCall = rootView.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + booking.getPassengerContact()));
                startActivity(intent);
            }
        });
        TextView btnChat = rootView.findViewById(R.id.btnChat);
        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.showToast(getActivity(), "TODO : module remaining");
            }
        });
        TextView btnDetails = rootView.findViewById(R.id.btnDetails);
        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holderDetail.getVisibility() == View.VISIBLE) {
                    holderDetail.setVisibility(View.GONE);
                } else {
                    holderDetail.setVisibility(View.VISIBLE);
                }
            }
        });
        TextView tvPassengerName = rootView.findViewById(R.id.tv_passenger_name);
        tvPassengerName.setText(booking.getPassengerName());
        TextView tvBookingNo = rootView.findViewById(R.id.tv_booking_no);
        tvBookingNo.setText(booking.getBookingNo());
        TextView tvDropOffLocation = rootView.findViewById(R.id.tv_drop_off_location);
        tvDropOffLocation.setText(booking.getDropOffLocation());
        TextView tvNote = rootView.findViewById(R.id.tvNote);
        if (TextUtils.isEmpty(booking.getNote())) {
            tvNote.setText(getString(R.string.na));
        } else {
            tvNote.setText(Html.fromHtml(booking.getNote()));
        }
        ImageView iconShowLess = rootView.findViewById(R.id.iconShowLess);
        iconShowLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holderDetail.setVisibility(View.GONE);
            }
        });

        btnStartTrip = rootView.findViewById(R.id.btnPickUp);
        btnEndTrip = rootView.findViewById(R.id.btnEndTrip);

        btnCancelTrip = rootView.findViewById(R.id.btnCancelTrip);
        btnCancelTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CancelBookingActivity.class);
                intent.putExtra("booking", booking);
                intent.putExtra("isArrived", isArrived);
                startActivityForResult(intent, REQUEST_CANCEL_BOOKING);
            }
        });

        if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
            tvLocation.setText(booking.getPickUpLocation());
            btnStartTrip.setVisibility(View.VISIBLE);
            btnCancelTrip.setVisibility(View.VISIBLE);
            btnEndTrip.setVisibility(View.GONE);

            //set title
            ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.pick_up));
        } else {
            tvLocation.setText(booking.getDropOffLocation());
            btnStartTrip.setVisibility(View.GONE);
            btnCancelTrip.setVisibility(View.GONE);
            btnEndTrip.setVisibility(View.VISIBLE);

            //set title
            ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.drop_off));
        }
        btnStartTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                showProgressDialog(getString(R.string.getting_gps_location));
                if (SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    SmartLocation.with(getActivity()).location().oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(final Location location) {
                            pDialog.dismiss();
                            if (NetworkUtils.isInNetwork(getActivity())) {

                                if (isArrived) {
                                    pDialog.setMessage(getString(R.string.starting_trip));
                                    startTripTask(location.getLatitude(), location.getLongitude());
                                    return;
                                }

                                Location pickUpLoc = new Location("");
                                pickUpLoc.setLatitude(booking.getPickUpLat());
                                pickUpLoc.setLongitude(booking.getPickUpLong());

                                float distance = location.distanceTo(pickUpLoc);
//                                isArrived = distance < 1001;
                                if (distance < 1001) {
                                    btnStartTrip.setText(R.string.pick_up);
                                    btnCancelTrip.setVisibility(View.VISIBLE);
                                    isArrived = true;
                                    setAlarmForPassengerNotArrive();
                                } else {
//                                    pDialog.setMessage("Checking location...");
//                                    arrivePickUpTask(location.getLatitude(), location.getLongitude());
                                    AlertUtils.simpleAlert(getActivity(), getString(R.string.location_check), getString(R.string.have_not_arrive_at_pickup) +
                                                    getString(R.string.location_not_with_1_pickup),
                                            getString(R.string.proceed), getString(R.string.cancel), new AlertUtils.OnAlertButtonClickListener() {
                                                @Override
                                                public void onAlertButtonClick(boolean isPositiveButton) {
                                                    if (isPositiveButton) {
                                                        isArrived = true;
                                                        btnStartTrip.setText(R.string.pick_up);
                                                        btnCancelTrip.setVisibility(View.VISIBLE);
                                                        setAlarmForPassengerNotArrive();
                                                    }
                                                }
                                            });
                                }
                            } else {
                                pDialog.dismiss();
                                AlertUtils.showSnack(getActivity(), view, getString(R.string.no_internet));
                            }
                        }
                    });
                } else {
                    pDialog.dismiss();
                    AlertUtils.simpleAlert(getActivity(), getString(R.string.title_enable_gps),
                            getString(R.string.gps_before_procced),
                            getString(R.string.ok), "", null);
                }
            }
        });
        btnEndTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                showProgressDialog(getString(R.string.getting_gps_location));
                if (SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    SmartLocation.with(getActivity()).location().oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(final Location location) {
                            pDialog.dismiss();
                            if (NetworkUtils.isInNetwork(getActivity())) {
//                                showProgressDialog("Checking location...");
//                                arriveDropOffTask(location.getLatitude(), location.getLongitude());

                                Location dropoffLocation = new Location("");
                                dropoffLocation.setLatitude(booking.getDropOffLat());
                                dropoffLocation.setLongitude(booking.getDropOffLong());

                                float distance = location.distanceTo(dropoffLocation);
                                if (distance < 1001) {
                                    goForDropOffConfirm();
                                } else {
                                    AlertUtils.simpleAlert(getActivity(), getString(R.string.location_check), getString(R.string.have_not_arrive_dropoff) +
                                                    getString(R.string.location_not_within_1_dropoff),
                                            getString(R.string.proceed), getString(R.string.cancel), new AlertUtils.OnAlertButtonClickListener() {
                                                @Override
                                                public void onAlertButtonClick(boolean isPositiveButton) {
                                                    if (isPositiveButton) {
                                                        goForDropOffConfirm();
                                                    }
                                                }
                                            });
                                }

                            } else {
                                pDialog.dismiss();
                                AlertUtils.showSnack(getActivity(), view, getString(R.string.no_internet));
                            }
                        }
                    });
                } else {
                    pDialog.dismiss();
                    AlertUtils.simpleAlert(getActivity(), getString(R.string.title_enable_gps),
                            getString(R.string.gps_before_procced),
                            getString(R.string.ok), "", null);
                }
            }
        });

        holderNoJob.setVisibility(View.GONE);
        holderYesJob.setVisibility(View.VISIBLE);

        setUpMap();
    }

    public void goForDropOffConfirm() {
        Intent intent = new Intent(getActivity(), DropOffConfirmActivity.class);
        intent.putExtra("booking", booking);
        startActivityForResult(intent, REQUEST_DROP_OFF_CONFIRM);
    }

    public void goForDropOff() {
        tvError.setVisibility(View.GONE);
        btnTryAgain.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);

        setUpTripInfo();
    }

    public void noBooking() {
        btnTryAgain.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        tvError.setVisibility(View.GONE);

        holderYesJob.setVisibility(View.GONE);
        holderNoJob.setVisibility(View.VISIBLE);

        TextView tvDriverName = rootView.findViewById(R.id.tv_driver_name);
        tvDriverName.setText(PreferenceHelper.getInstance(getActivity()).getAppUserFullName());

        setUpMap();

        //set title
        if (getActivity() != null)
            ((MainActivity) Objects.requireNonNull(getActivity())).setToolbarTitle("");
    }

    private void setUpMap() {
//        if (!isFragmentVisible) {
//            return;
//        }
        SupportMapFragment mapFragment = new SupportMapFragment();
//        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        FragmentManager fm;

        try {
            fm = getChildFragmentManager();
        } catch (Exception e) {
            return;
        }
        try {
            fm.beginTransaction().replace(R.id.map, mapFragment).commit();
        } catch (Exception e) {
            return;
        }

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;

                //rider location
                if (!SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    listener = new AlertUtils.OnAlertButtonClickListener() {
                        @Override
                        public void onAlertButtonClick(boolean isPositiveButton) {
                            if (!LocationHelper.checkIfGPSEnable(getActivity())) {
                                MapProvider.showGPSEnableAlert(getActivity(), listener);
                            }
                        }
                    };
                    MapProvider.showGPSEnableAlert(getActivity(), listener);
                }

                final MapProvider mapProvider = new MapProvider();
                mapProvider.with(getActivity())
                        .onMap(map);

                //remove markers, overviews, lines
                googleMap.clear();

                SmartLocation.with(getActivity()).location().continuous().start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        Logger.e("TripFragment() onMapReady location continuous update", location.toString());
                        if (marker != null) {
                            marker.remove();
                        }
                        marker = mapProvider.addMarker(location, getString(R.string.your_current_location), "",
                                R.drawable.icon_pin_marker, true);
                        mapProvider.animateTo(location, 15);
                    }
                });

                if (booking == null) {
                    return;
                }

//                mapProvider.showRout(pickUpLoc, dropOffLoc);
                if (booking.getStatus().equalsIgnoreCase(Booking.TRIP_STATUS_SCHEDULED)) {
                    Location pickUpLoc = new Location("");
                    pickUpLoc.setLatitude(booking.getPickUpLat());
                    pickUpLoc.setLongitude(booking.getPickUpLong());
                    mapProvider.setStartLocation(pickUpLoc, booking.getPickUpLocation(), R.drawable.icon_pickup);
                    mapProvider.moveTo(pickUpLoc, 15);
                } else {
                    Location dropOffLoc = new Location("");
                    dropOffLoc.setLatitude(booking.getDropOffLat());
                    dropOffLoc.setLongitude(booking.getDropOffLong());
                    mapProvider.setEndLocation(dropOffLoc, booking.getDropOffLocation(), R.drawable.icon_dropoff);
                    mapProvider.moveTo(dropOffLoc, 15);
                }
            }
        });

    }

    int REQUEST_CANCEL_BOOKING = 9837;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
//        if (requestCode == REQUEST_CANCEL_BOOKING && resultCode == Activity.RESULT_OK) {
//            btnCancelTrip.setVisibility(View.GONE);
//            btnStartTrip.setVisibility(View.GONE);
//            btnEndTrip.setVisibility(View.GONE);
//        } else if (requestCode == REQUEST_DROP_OFF_CONFIRM && resultCode == Activity.RESULT_OK) {
            holderNoJob.setVisibility(View.GONE);
            holderYesJob.setVisibility(View.GONE);

            btnCancelTrip.setVisibility(View.GONE);
            btnEndTrip.setVisibility(View.GONE);
            btnStartTrip.setVisibility(View.GONE);

            FragmentManager fm = getChildFragmentManager();
            fm.beginTransaction().remove(fm.findFragmentById(R.id.map)).commit();

            progress.setVisibility(View.VISIBLE);
            tvError.setVisibility(View.GONE);
            btnTryAgain.setVisibility(View.GONE);

            booking = null;

            checkMyBookingTask();
        }
    }

    private void arrivePickUpTask(double latitude, double longitude) {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().arriveForPickUp, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
//                                showStartTripAlert();
                                btnStartTrip.setText(R.string.pick_up);
                                btnCancelTrip.setVisibility(View.VISIBLE);
                                isArrived = true;

                                AlertUtils.showToast(getActivity(), responseData.getString("message"));
                            } else {
                                AlertUtils.simpleAlert(getActivity(), false, getString(R.string.error),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("arrivePickUpTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("arrivePickUpTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), getString(R.string.error),
                                errorMsg, getString(R.string.ok),
                                "", null);
                    }
                }, "arrivePickUpTask");

    }

    private void arriveDropOffTask(double latitude, double longitude) {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().arriveForDropOff, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                Intent intent = new Intent(getActivity(), DropOffConfirmActivity.class);
                                intent.putExtra("booking", booking);
                                startActivityForResult(intent, REQUEST_DROP_OFF_CONFIRM);
                            } else {
                                AlertUtils.simpleAlert(getActivity(), false, getString(R.string.error),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("arrivePickUpTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("arrivePickUpTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), getString(R.string.error),
                                errorMsg, getString(R.string.ok),
                                "", null);
                    }
                }, "arrivePickUpTask");

    }

    private void startTripTask(double latitude, double longitude) {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));
        postParams.put("action", "start");

        volleyHelper.addVolleyRequestListeners(Api.getInstance().startEndTripApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                MainActivity act = (MainActivity) getActivity();
                                if (act == null) {
                                    return;
                                }
                                act.emitJobAccept(response);
                                //end trip
                                //change toolbar title, show end trip swipe button, update booking status
                                booking.setStatus(Booking.TRIP_STATUS_IN_PROGRESS);
                                act.setToolbarTitle(getString(R.string.end_trip));
                                btnEndTrip.setVisibility(View.VISIBLE);
                                btnStartTrip.setVisibility(View.GONE);
                                btnCancelTrip.setVisibility(View.GONE);
                                tvLocation.setText(booking.getDropOffLocation());
                            } else {
                                AlertUtils.simpleAlert(getActivity(), false, getString(R.string.error),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("startTripTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("startTripTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), getString(R.string.error),
                                errorMsg, getString(R.string.ok),
                                "", null);
                    }
                }, "startTripTask");

    }

    private void startNavigationActivity(double lat, double lon) {
//        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lon);
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");

        String uri = "geo:0,0?q=" + lat + "," + lon;
        Uri gmmIntentUri = Uri.parse(uri);
        final Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        try {
            startActivity(mapIntent);
        } catch (Exception e) {
            try {
                Intent navigation = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lon));
                startActivity(navigation);
            } catch (Exception e1) {
            }
        }
    }

    @Override
    public void onStop() {
        cleanup();
        super.onStop();
    }

    @Override
    public void onDetach() {
        cleanup();
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        cleanup();
        super.onDestroy();
    }

//    @Override
//    public void onPause() {
//        cleanup();
//        super.onPause();
//    }

    private void cleanup() {
//        VolleyHelper.getInstance(getActivity()).cancelRequest("checkMyBookingTask");
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                VolleyHelper.getInstance(getActivity()).cancelRequest("checkMyBookingTask");
//            }
//        }, 2001);
    }

    public void showProgressDialog(String message) {
        if (pDialog == null) {
            pDialog = AlertUtils.showProgressDialog(getActivity(), message);
        } else {
            pDialog.setMessage(message);
            pDialog.show();
        }
    }

    public void setAlarmForPassengerNotArrive() {
        //alarm for 5 minutes
        Intent downloader = new Intent(getActivity(), PassengerNotShowReceiver.class);
        downloader.putExtra("bookingNo", booking.getBookingNo());
        PendingIntent recurringDownload = PendingIntent.getBroadcast(getActivity(), 1, downloader,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) getActivity().getSystemService(
                Context.ALARM_SERVICE);

        boolean isWorking = (PendingIntent.getBroadcast(getActivity(), 1, downloader,
                PendingIntent.FLAG_NO_CREATE) != null);//just changed the flag

//        if (!isWorking) {
            int time = 5 * 60 * 1000;
            if (Build.VERSION.SDK_INT > 22) {
                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                        System.currentTimeMillis() + time, recurringDownload);
            } else {
                alarms.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + time, recurringDownload);
            }
//        }
    }
}
