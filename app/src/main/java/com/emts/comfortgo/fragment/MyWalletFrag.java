package com.emts.comfortgo.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emts.comfortgo.GetBalanceCallback;
import com.emts.comfortgo.R;
import com.emts.comfortgo.activity.TopUpActivity;
import com.emts.comfortgo.activity.TransactionHistoryActivity;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;

public class MyWalletFrag extends Fragment {
    PreferenceHelper prefsHelper;
    View holderContent;
    ProgressBar progressBar;
    TextView errorText;

    TextView tvCreditBalance;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_wallet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        holderContent = view.findViewById(R.id.holderContent);
        holderContent.setVisibility(View.GONE);
        progressBar = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.error_text);

        tvCreditBalance = view.findViewById(R.id.tv_credit_balance);
        CardView menuTopUpCredit, menuTransactionHistory;
        menuTopUpCredit = view.findViewById(R.id.menu_top_up_credit);
        menuTransactionHistory = view.findViewById(R.id.menu_transaction_history);

        TextView tvMenuTopUpCredit, tvMenuTransactionHistory;
        tvMenuTopUpCredit = menuTopUpCredit.findViewById(R.id.tv_menu_title);
        tvMenuTopUpCredit.setText(R.string.top_up_credit);
        tvMenuTransactionHistory = menuTransactionHistory.findViewById(R.id.tv_menu_title);
        tvMenuTransactionHistory.setText(R.string.transaction_history);

        menuTopUpCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TopUpActivity.class));
            }
        });

        menuTransactionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TransactionHistoryActivity.class));
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            GetBalanceCallback.getMyBalanceTask(getActivity(), new GetBalanceCallback.BalanceSuccessCallback() {
                @Override
                public void onBalance(boolean isSuccess, String msg) {
                    if (isSuccess) {
                        tvCreditBalance.setText(getString(R.string.currency_myr) + " " + prefsHelper.getUserCreditBalance());
                        holderContent.setVisibility(View.VISIBLE);
                        errorText.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    } else {
                        errorText.setText(msg);
                        errorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        holderContent.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    public void updateBalance() {
        tvCreditBalance.setText(getString(R.string.currency_myr) + " " + prefsHelper.getUserCreditBalance());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        VolleyHelper.getInstance(getActivity()).cancelRequest("getMyBalanceTask");
    }
}
