package com.emts.comfortgo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.PreferenceHelper;

public class PassengerNotShowReceiver extends BroadcastReceiver {
    public static String PASSENGER_NOT_SHOWN_FOR_BOOKING = "passenger_not_show_bno";

    @Override
    public void onReceive(Context context, Intent intent) {
        PreferenceHelper.getInstance(context).edit().putString(PASSENGER_NOT_SHOWN_FOR_BOOKING, intent.getStringExtra("bookingNo")).commit();
        Logger.e("PassengerNotShowReceiver onReceive", " Booking NO: " + intent.getStringExtra("bookingNo"));
    }
}
