package com.emts.comfortgo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;

import com.emts.comfortgo.activity.MainActivity;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.model.Booking;

import org.json.JSONObject;

import java.util.Date;
import java.util.Map;

public class BookingParser {

    public static Booking getBooking(String response) {
        try {
            JSONObject bookingObj = new JSONObject(response);
            Booking booking = new Booking();
            booking.setBookingNo(bookingObj.getString("booking_no"));
//            booking.setTimeToLive(bookingObj.getInt("max_time_to_driver_response"));
            booking.setTripTime(bookingObj.getString("journey_time"));
            booking.setPickUpLat(bookingObj.getDouble("pickup_lat"));
            booking.setPickUpLong(bookingObj.getDouble("pickup_lng"));
            booking.setPickUpLocation(bookingObj.getString("pickup_location"));
            booking.setDropOffLat(bookingObj.getDouble("dropoff_lat"));
            booking.setDropOffLong(bookingObj.getDouble("dropoff_lng"));
            booking.setDropOffLocation(bookingObj.getString("dropoff_location"));
            booking.setTripFare(bookingObj.getString("fare_min"));
            booking.setTripFareMax(bookingObj.getString("fare_max"));
            booking.setPassengerName(bookingObj.getString("passenger_name"));
            booking.setPassengerContact(bookingObj.getString("mobile_no"));
            booking.setStatus(bookingObj.getString("trip_status"));
            booking.setDistance(bookingObj.getString("distance"));
            booking.setDate(bookingObj.getString("datetime"));
            booking.setNote(bookingObj.getString("note"));
            booking.setFareType(bookingObj.getString("fare_type"));
            booking.setBookingFee(bookingObj.getString("booking_fee"));
            return booking;
        } catch (Exception e) {
            Logger.e("BookingParser getBooking ex", e.getMessage() + " ");
        }
        return null;
    }

    public static Booking bookingFromPush(Map<String, String> bookingPush) {
        Booking booking = new Booking();
        booking.setBookingNo(bookingPush.get("booking_no"));
        booking.setTimeToLive(Integer.parseInt(bookingPush.get("max_time_to_driver_response")));
        booking.setTripTime(bookingPush.get("journey_time"));
        booking.setPickUpLat(Double.parseDouble(bookingPush.get("pickup_lat")));
        booking.setPickUpLong(Double.parseDouble(bookingPush.get("pickup_lng")));
        booking.setPickUpLocation(bookingPush.get("pickup_location"));
        booking.setDropOffLat(Double.parseDouble(bookingPush.get("dropoff_lat")));
        booking.setDropOffLong(Double.parseDouble(bookingPush.get("dropoff_lng")));
        booking.setDropOffLocation(bookingPush.get("dropoff_location"));
        booking.setTripFare(bookingPush.get("fare_min"));
        booking.setTripFareMax(bookingPush.get("fare_max"));
        booking.setPassengerName(bookingPush.get("passenger_name"));
        booking.setPassengerContact(bookingPush.get("mobile_no"));
        booking.setStatus(bookingPush.get("trip_status"));
        booking.setDistance(bookingPush.get("distance"));
        booking.setDate(bookingPush.get("datetime"));
        booking.setNote(bookingPush.get("note"));
        booking.setFareType(bookingPush.get("fare_type"));
        booking.setBookingFee(bookingPush.get("booking_fee"));

//        //bookingPush.get("promotion_discount");
        return booking;
    }

    //TODO : test cases
    public static Booking getTestBooking() {
        Booking booking = new Booking();
        booking.setBookingNo("d10001");
        booking.setTimeToLive(10);
        booking.setTripTime("123");
        booking.setPickUpLat(27.6888562);
        booking.setPickUpLong(85.3135451);
        booking.setPickUpLocation("E-multitech Solutions Pvt. Ltd. Kupondole Lalitpur");
        booking.setDropOffLat(27.6888562);
        booking.setDropOffLong(85.3135451);
        booking.setDropOffLocation("E-multitech Solutions Pvt. Ltd. Kupondole Lalitpur");
        booking.setTripFare("2332");
        booking.setTripFareMax("2342");
        booking.setPassengerName("Sharma Vivek");
        booking.setPassengerContact("9803681065");
        booking.setStatus("1");
        booking.setDistance("21");
        booking.setDate("2818-10-25 9:37:09");
        booking.setNote("this is a note");
        booking.setFareType("0");
        booking.setBookingFee("212");
        return booking;
    }

    public static void generateNotification(Context context) {

//        Booking booking = getTestBooking();
//        Intent intent1 = new Intent(BookingBaseActivity.INTENT_BOOKING_BROADCAST);
//        intent1.putExtra("booking", booking);
//        context.sendBroadcast(intent1);
//        if (true) return;


        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("booking", getTestBooking());
        intent.putExtra("fromPush", true);

        String title = "test Notification Title";
        String body = "test Notificatin Body";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "123123");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 123,
                intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(Html.fromHtml(body));
        bigText.setBigContentTitle(Html.fromHtml(title));
        bigText.setSummaryText("The booking is valid for 10 seconds only");

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.noti_icon);
        mBuilder.setContentTitle(Html.fromHtml(title));
        mBuilder.setContentText(Html.fromHtml(body));
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);
        mBuilder.setSound(defaultSoundUri);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("123123",
                    "New Booking Trip Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        }

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        mNotificationManager.notify(m, mBuilder.build());
        Logger.e("firebase notificatio", "notification fired ID:" + m + " TITLE: " + title
                + " BODY:" + body + " ICON:");
    }
}
