package com.emts.comfortgo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.comfortgo.R;
import com.emts.comfortgo.activity.TripDetailsActivity;
import com.emts.comfortgo.model.TripModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EachTripAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<TripModel> eachTripLists;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm a");

    public EachTripAdapter(Context context, ArrayList<TripModel> eachTripLists) {
        this.context = context;
        this.eachTripLists = eachTripLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_each_trip, viewGroup, false));

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        ViewHolder viewHolder = (ViewHolder) holder;
        TripModel tripModel = eachTripLists.get(i);

        try {
            Date date = sdf.parse(tripModel.getTripDate());
            viewHolder.tvTime.setText(sdfTime.format(date));
        } catch (ParseException e) {
            viewHolder.tvTime.setText(tripModel.getTripDate());
        }

        viewHolder.tvBookingNo.setText(tripModel.getTripBookingNo());
        viewHolder.tvIncome.setText(context.getString(R.string.currency_myr) + " " + tripModel.getTotalIncome());

        if (tripModel.getTripBookingStatus().equals("2")) {
            viewHolder.tvTripStatus.setText(context.getString(R.string.cancelled));
            viewHolder.tvTripStatus.setTextColor(context.getResources().getColor(R.color.appOrange));
            viewHolder.tvIncome.setVisibility(View.GONE);
        } else {
            if (tripModel.getTripStatus().equals(TripModel.TRIP_PENDING)) {
                viewHolder.tvTripStatus.setText(context.getString(R.string.pending));
                viewHolder.tvTripStatus.setTextColor(context.getResources().getColor(R.color.appRedDark));
            } else if (tripModel.getTripStatus().equals(TripModel.TRIP_SCHEDULED)) {
                viewHolder.tvTripStatus.setText(context.getString(R.string.scheduled));
                viewHolder.tvTripStatus.setTextColor(context.getResources().getColor(R.color.appBlue));
            } else if (tripModel.getTripStatus().equals(TripModel.TRIP_IN_PROGRESS)) {
                viewHolder.tvTripStatus.setText(context.getString(R.string.in_progress));
                viewHolder.tvTripStatus.setTextColor(context.getResources().getColor(R.color.appOrange));
            } else if (tripModel.getTripStatus().equals(TripModel.TRIP_COMPLETED)) {
                viewHolder.tvTripStatus.setText(context.getString(R.string.completed));
                viewHolder.tvTripStatus.setTextColor(context.getResources().getColor(R.color.appGreen2));
            }
        }
    }

    @Override
    public int getItemCount() {
        return eachTripLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvBookingNo, tvTime, tvIncome, tvTripStatus;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvBookingNo = itemView.findViewById(R.id.tv_trip_booking_no);
            tvTime = itemView.findViewById(R.id.tv_trip_time);
            tvIncome = itemView.findViewById(R.id.tv_trip_price);
            tvTripStatus = itemView.findViewById(R.id.tv_trip_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, TripDetailsActivity.class);
                    intent.putExtra("bookingNo", eachTripLists.get(getLayoutPosition()).getTripBookingNo());
                    if (eachTripLists.get(getLayoutPosition()).getTripBookingStatus().equals("2")) {
                        intent.putExtra("isCancelled", true);
                    }
                    context.startActivity(intent);
                }
            });
        }
    }
}
