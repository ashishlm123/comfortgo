package com.emts.comfortgo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.comfortgo.R;
import com.emts.comfortgo.model.CreditsModel;

import java.util.ArrayList;

public class TopUpCreditAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CreditsModel> creditLists;

    public TopUpCreditAdapter(Context context, ArrayList<CreditsModel> creditLists) {
        this.context = context;
        this.creditLists = creditLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_top_up_credit, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CreditsModel creditsModel = creditLists.get(position);

        viewHolder.tvCreditPackage.setText(creditsModel.getPackageName().trim());
        viewHolder.tvCreditQty.setText(creditsModel.getCreditQty() + " " + context.getString(R.string.credit));

        Double creditPrice = Double.parseDouble(creditsModel.getCreditPrice());
        viewHolder.tvCreditAmt.setText(context.getString(R.string.currency_myr) + " " + String.format("%.2f", creditPrice));
    }

    @Override
    public int getItemCount() {
        return creditLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCreditPackage, tvCreditAmt, tvCreditQty;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCreditPackage = itemView.findViewById(R.id.tv_package);
            tvCreditAmt = itemView.findViewById(R.id.tv_credit_price);
            tvCreditQty = itemView.findViewById(R.id.tv_credit_qty);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
