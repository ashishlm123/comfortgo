package com.emts.comfortgo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.comfortgo.R;
import com.emts.comfortgo.activity.RequestTopUpActivity;
import com.emts.comfortgo.activity.TripDetailsActivity;
import com.emts.comfortgo.model.TransactionModel;

import java.util.ArrayList;

public class TransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<TransactionModel> transactionLists;

    public TransactionAdapter(Context context, ArrayList<TransactionModel> transactionLists) {
        this.context = context;
        this.transactionLists = transactionLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_transaction_history, parent, false));

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        TransactionModel transactionModel = transactionLists.get(position);

        viewHolder.tvTxnDate.setText(transactionModel.getTxnDate());
        if (transactionModel.getTxnType().equals(TransactionModel.TRANSACTION_TOP_UP)) {

            viewHolder.tvTxnType.setText(context.getString(R.string.top_up));
            viewHolder.tvTxnType.setBackground(context.getResources().getDrawable(R.drawable.bg_green_rectangle));
            viewHolder.tvTxnType.setTextColor(context.getResources().getColor(R.color.white));

            viewHolder.layTopUpID.setVisibility(View.VISIBLE);
            viewHolder.tvTopUpId.setText(transactionModel.getTopUpID());

            viewHolder.layCreditsPurchase.setVisibility(View.VISIBLE);
            viewHolder.tvCreditsPurchase.setText(context.getString(R.string.currency_myr)+ " " + transactionModel.getCreditPurchaseAmt());

            viewHolder.layStatus.setVisibility(View.VISIBLE);
            switch (transactionModel.getTxnStatus()) {
                case TransactionModel.TRANSACTION_PENDING:
                    viewHolder.tvStatus.setText(context.getString(R.string.pending));
                    viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.textBlue));
                    break;
                case TransactionModel.TRANSACTION_APPROVED:
                    viewHolder.tvStatus.setText(context.getString(R.string.approved));
                    viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.appGreen2));
                    break;
                case TransactionModel.TRANSACTION_REJECTED:
                    viewHolder.tvStatus.setText(context.getString(R.string.rejected));
                    viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.appRedDark));
                    break;
            }

            viewHolder.layCommissionFee.setVisibility(View.GONE);
            viewHolder.layBookingNo.setVisibility(View.GONE);
        } else {
            viewHolder.tvTxnType.setText(context.getString(R.string.deduction));
            viewHolder.tvTxnType.setBackground(context.getResources().getDrawable(R.drawable.bg_yellow_rectangle));
            viewHolder.tvTxnType.setTextColor(context.getResources().getColor(R.color.black));
            viewHolder.layCommissionFee.setVisibility(View.VISIBLE);
            viewHolder.tvCommissionDeduction.setText(context.getString(R.string.currency_myr) + " " + transactionModel.getCommissionDeductionAmt());
            viewHolder.layBookingNo.setVisibility(View.VISIBLE);
            viewHolder.tvBookingNo.setText(transactionModel.getBookingNo());


            viewHolder.layTopUpID.setVisibility(View.GONE);
            viewHolder.layCreditsPurchase.setVisibility(View.GONE);
            viewHolder.layStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return transactionLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTxnType, tvTxnDate, tvTopUpId, tvCreditsPurchase, tvStatus, tvCommissionDeduction, tvBookingNo;
        LinearLayout layTopUpID, layCreditsPurchase, layStatus, layCommissionFee, layBookingNo;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            layTopUpID = itemView.findViewById(R.id.lay_top_up_id);
            layCreditsPurchase = itemView.findViewById(R.id.lay_credits_purchase);
            layStatus = itemView.findViewById(R.id.lay_status);
            layCommissionFee = itemView.findViewById(R.id.lay_commission_fee);
            layBookingNo = itemView.findViewById(R.id.lay_booking_no_holder);

            tvTxnType = itemView.findViewById(R.id.tv_txn_type);
            tvTxnDate = itemView.findViewById(R.id.tv_txn_date);
            tvTopUpId = itemView.findViewById(R.id.tv_top_up_id);
            tvCreditsPurchase = itemView.findViewById(R.id.tv_credits_purchase);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvCommissionDeduction = itemView.findViewById(R.id.tv_commission_deduction);
            tvBookingNo = itemView.findViewById(R.id.tv_booking_no);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TransactionModel transactionModel = transactionLists.get(getLayoutPosition());
                    if (transactionModel.getTxnType().equals(TransactionModel.TRANSACTION_TOP_UP)) {
                        Intent intent = new Intent(context, RequestTopUpActivity.class);
                        intent.putExtra("isHistory", true);
                        intent.putExtra("trans", transactionLists.get(getLayoutPosition()));
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, TripDetailsActivity.class);
                        intent.putExtra("bookingNo", transactionModel.getBookingNo());
                        context.startActivity(intent);
                    }
                }
            });
        }
    }


}
