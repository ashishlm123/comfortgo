package com.emts.comfortgo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.comfortgo.R;
import com.emts.comfortgo.activity.TripDayActivity;
import com.emts.comfortgo.model.TripModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TripHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<TripModel> tripHistoryLists;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");

    public TripHistoryAdapter(Context context, ArrayList<TripModel> tripHistoryLists) {
        this.context = context;
        this.tripHistoryLists = tripHistoryLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_trip_history, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        TripModel tripModel = tripHistoryLists.get(position);

        try {
            Date date = sdf.parse(tripModel.getTripDate());
            viewHolder.tvTripDate.setText(sdf1.format(date));
        } catch (ParseException e) {
            viewHolder.tvTripDate.setText(tripModel.getTripDate());
        }

        viewHolder.tvTripIncome.setText(context.getString(R.string.currency_myr) + " " + tripModel.getTotalIncome());
        viewHolder.tvTotalTrips.setText(tripModel.getTotalTrips());


    }

    @Override
    public int getItemCount() {
        return tripHistoryLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTripDate, tvTripIncome, tvTotalTrips;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTripDate = itemView.findViewById(R.id.tv_trip_date);
            tvTripIncome = itemView.findViewById(R.id.tv_total_trip_income);
            tvTotalTrips = itemView.findViewById(R.id.tv_total_trips_made);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, TripDayActivity.class);
                    intent.putExtra("tripDate", tripHistoryLists.get(getLayoutPosition()).getTripDate());
                    intent.putExtra("eachDayTotal", tripHistoryLists.get(getLayoutPosition()).getTotalIncome());
                    context.startActivity(intent);
                }
            });
        }
    }
}
