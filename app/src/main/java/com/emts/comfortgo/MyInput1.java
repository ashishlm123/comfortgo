package com.emts.comfortgo;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MyInput1 extends LinearLayout {
    private View view;
    private Context context;

    public static final float DEFAULT_TEXT_SIZE = dpToPx(14);

    private CharSequence edtHint = "HINT";
    private CharSequence inputText = "TEXT";
    @ColorInt
    private int textColorHint;
    @ColorInt
    private int textColor;
    private int edtIcon;
    private float textSize = DEFAULT_TEXT_SIZE;
    private int inputType;
    private int imeOptions;


    EditText editText;
    ImageView ivIcon;


    public MyInput1(Context context) {
        super(context);
        this.context = context;
        setAttrs(context, null);
        init();
    }

    public MyInput1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setAttrs(context, attrs);
        init();
    }

    public MyInput1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setAttrs(context, attrs);
        init();
    }

    public MyInput1(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        setAttrs(context, attrs);
        init();
    }


    public void init() {
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.layout_my_input_1, this, true);
        editText = findViewById(R.id.edt);
        ivIcon = findViewById(R.id.iv);

        editText.setHint(edtHint);
        editText.setInputType(inputType);
        editText.setImeOptions(imeOptions);
        ivIcon.setImageResource(edtIcon);

        editText.setTypeface(ResourcesCompat.getFont(context, R.font.gotham_medium));
    }

    private void setAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.MyInput1,
                0, 0);
        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.MyInput1_android_imeOptions:
                    imeOptions = a.getInt(attr, EditorInfo.IME_ACTION_NEXT);
                    break;
                case R.styleable.MyInput1_android_inputType:
                    inputType = a.getInt(attr, EditorInfo.TYPE_TEXT_VARIATION_NORMAL);
            }
        }

        try {
            final String editTextHint = a.getString(R.styleable.MyInput1_hint);
            if (editTextHint != null)
                edtHint = editTextHint;

            final String edtTextString = a.getString(R.styleable.MyInput1_text);
            if (edtTextString != null)
                inputText = edtTextString;

            textColorHint = a.getColor(R.styleable.MyInput1_text_color_hint, ContextCompat.getColor(context, R.color.textDisableFaded));
            textColor = a.getColor(R.styleable.MyInput1_text_color, ContextCompat.getColor(context, R.color.black));

            edtIcon = a.getResourceId(R.styleable.MyInput1_icon, R.drawable.icon_logout);
            textSize = a.getDimensionPixelSize(R.styleable.MyInput1_text_size, (int) DEFAULT_TEXT_SIZE);


        } finally {
            a.recycle();
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public CharSequence getText() {
        return this.editText.getText().toString().trim();
    }

    public void setText(String text) {
        this.editText.setText(text);
    }
}
