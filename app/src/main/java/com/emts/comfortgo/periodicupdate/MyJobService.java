package com.emts.comfortgo.periodicupdate;

import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.location.Location;
import android.provider.Settings;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;

import java.util.HashMap;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class MyJobService extends JobService {
    static OnLocationUpdatedListener locationUpdatedListener;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        Logger.e("MyJobService onStartJob", "Time: " + System.currentTimeMillis()
                + "\nParams : " + jobParameters.toString());
        if (!PreferenceHelper.getInstance(this).isLogin()) {
            JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.cancel(LocUpdateScheduler.MYJOBID);
            return false;
        }
        int locationMode = 0;
        try {
            locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
        }
        //check location mode
        if (locationMode != Settings.Secure.LOCATION_MODE_OFF && locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) {
            Logger.e("location update loc", "GPS on high accuracy");
            if (locationUpdatedListener == null) {
                locationUpdatedListener = new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        Logger.e("location update get loc", location.toString());
                        if (NetworkUtils.isInNetwork(getApplicationContext())) {
                            updateLocation(MyJobService.this, location.getLatitude(), location.getLongitude());
                            jobFinished(jobParameters, false);
                        }
                    }
                };
            }
            SmartLocation.with(MyJobService.this).location().oneFix().start(locationUpdatedListener);
        } else {
            // location enabled but not at high accuracy so use other location available
            Location location = SmartLocation.with(MyJobService.this).location().getLastLocation();
            Logger.e("location update last loc", "cannot get loc from GPS so get last known location");
            if (location != null) {
                Logger.e("Location Update", "Last Known Location + " + location);
                updateLocation(MyJobService.this, location.getLatitude(), location.getLongitude());
                jobFinished(jobParameters, false);
            }
        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Logger.e("MyJobService onStopJob", "Params : " + jobParameters.toString());
        SmartLocation.with(MyJobService.this).location().oneFix().stop();
        return false;
    }

    public static void updateLocation(Context context, double latitude, double longitude) {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().locationUpdateApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
//                        try {
//                            JSONObject responseData = new JSONObject(response);
//                            if (responseData.getBoolean("status")) {
//
//                            }
//                        } catch (JSONException e) {
//                            Logger.e("updateLocation json exception ", e.getMessage());
//                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "updateLocation");

    }
}
