package com.emts.comfortgo;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GetBalanceCallback {
    public static final String DIAMOND_DRIVER = "DRIVER_ID_2";

    public static void getMyBalanceTask(final Context context, final BalanceSuccessCallback callback) {
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getMyBalanceApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                PreferenceHelper.getInstance(context).edit().putString(PreferenceHelper.USER_CREDIT_BALANCE, res.getString("credit_balance")).commit();
                                if (callback != null) {
                                    String msg = "";
                                    if (res.getString("my_vehicle_type").equals("2")) {
                                        msg = DIAMOND_DRIVER;
                                    }
                                    callback.onBalance(true, msg);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getMyBalanceTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = context.getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getMyBalanceTask error ex", e.getMessage() + " ");
                        }
                        if (callback != null) {
                            callback.onBalance(true, errorMsg);
                        }
                    }
                }, "getMyBalanceTask");
    }

    public interface BalanceSuccessCallback {
        void onBalance(boolean isSuccess, String msg);
    }
}
