package com.emts.comfortgo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.activity.MainActivity;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Objects;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public abstract class BookingBaseActivity extends ComfortBaseActivity {
    private static final String TAG = "BookingBaseActivity";
    public static BookingReceiver bookingReceiver;
    public static final String INTENT_BOOKING_BROADCAST = "com.emts.comfortgo.INTENT_BOOKING_BROADCAST";
    private Dialog bookingDialog;
    CountDownTimer countDownTimer;
    TextView textViewShowTime;
    boolean isAutoDismissed = true;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(bookingReceiver, new IntentFilter(INTENT_BOOKING_BROADCAST));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bookingReceiver != null)
            unregisterReceiver(bookingReceiver);
        Logger.e("baseActivity unRegisterReceiver", "booking receiver unregistered");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //if not init every time will show dialog in MainActivity only i.e below other activity that are opened on top of Main
//        if (bookingDialog == null) {
//        createAlert(this);
//        }
        bookingReceiver = new BookingReceiver();

        //connect to socket here
        connectToServerSocket();
    }

    private void createAlert(Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        View alertView = LayoutInflater.from(context).inflate(R.layout.alert_instant_booking, null);
        alert.setView(alertView);
        bookingDialog = alert.create();
        Objects.requireNonNull(bookingDialog.getWindow()).setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        Logger.e("Booking Alert", "222 create booking alert !!!");
    }

    private void dismissBookingAlert() {
        if (bookingDialog != null) {
            bookingDialog.dismiss();
        }
    }

    private class BookingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.e("baseActivity bookingReceiver onReceive", "Booking Received :" + intent);
            try {
                Booking booking = (Booking) intent.getSerializableExtra("booking");
                showBookingAlert(context, booking);
            } catch (Exception e) {
                Logger.e("BookingReceiver onReceive ex", e.getMessage() + " ");
            }
        }
    }

    public void showBookingAlert(final Context context, final Booking booking) {
//        try {
//            WindowManager windowManager = getWindowManager();
//            windowManager.removeView(bookingDialog);
//        } catch (IllegalArgumentException e) {
//        }
//        if (bookingDialog == null)

        if (bookingDialog != null) {
            dismissBookingAlert();
        }

        if (countDownTimer != null) countDownTimer.cancel();
        createAlert(context);

        isAutoDismissed = true;

        if (!bookingDialog.isShowing()) {
            vibrate(context);
            playNotificationSound(context);
            bookingDialog.show();
            wakeUpTheScreen(context);
            Objects.requireNonNull(bookingDialog.getWindow()).setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            Logger.e("Booking Alert", "333 dialog not showing ...");
        }

        textViewShowTime = bookingDialog.findViewById(R.id.tv_timer);

        startTimer(booking.getTimeToLive());
        TextView tvPassengerName = bookingDialog.findViewById(R.id.tv_passenger_name);
        tvPassengerName.setText(booking.getPassengerName());
        TextView tvBookingNo = bookingDialog.findViewById(R.id.tv_booking_no);
        tvBookingNo.setText(booking.getBookingNo());
        TextView tvFareType = bookingDialog.findViewById(R.id.fareType);
        if (booking.getFareType().equalsIgnoreCase(Booking.FARE_TYPE_FIXED)) {
            tvFareType.setText(R.string.fixed_fare);
        } else {
            tvFareType.setText(R.string.metered_fare);
        }

        TextView tvPickUpLoc = bookingDialog.findViewById(R.id.tv_pickup_location);
        tvPickUpLoc.setText(booking.getPickUpLocation());
        TextView tvDropOffLoc = bookingDialog.findViewById(R.id.tv_drop_off_location);
        tvDropOffLoc.setText(booking.getDropOffLocation());

        Button btnAccept, btnReject;
        btnAccept = bookingDialog.findViewById(R.id.btn_accept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAutoDismissed = false;
                acceptBookingTask(context, booking);
            }
        });
        btnReject = bookingDialog.findViewById(R.id.btn_reject);
        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAutoDismissed = false;
                dismissBookingAlert();
                rejectBookingTask(context, booking);
            }
        });
        Logger.e("Booking Alert", "444 data set to dialog : TTL :" + booking.getTimeToLive()
                + "\nPassenger Name: " + booking.getPassengerName() + "\nBooking No: " + booking.getBookingNo() +
                "\nFare Type: " + booking.getFareType() + "\n PickUp: " + booking.getPickUpLocation() + "\n DropOff: "
                + booking.getDropOffLocation());

        bookingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (isAutoDismissed) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        autoCloseBookingTask();
                    }
                }
            }
        });
    }

    private void wakeUpTheScreen(Context context) {
        //wake the screen
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "com.emts.cabbi:mywakelockTag");
        wakeLock.acquire(1000);

        //release the wake lock
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
        Logger.e("Booking Alert", "777 get the wake lock and wake screen");
    }

    private void playNotificationSound(Context context) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.e("Booking Alert", "666.555 play notification sound");
    }

    private void vibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (v == null) return;
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
        Logger.e("Booking Alert", "666 vibrate for notification");
    }

    private void acceptBookingTask(final Context context, Booking booking) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(context, getString(R.string.accepting_booking));
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().acceptBooking, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                emitJobAccept(response);
                                if (countDownTimer != null) {
                                    countDownTimer.cancel();
                                }
                                dismissBookingAlert();
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                            } else {
                                AlertUtils.simpleAlert(context, false, getString(R.string.booking_unsuccess),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                dismissBookingAlert();
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Logger.e("acceptBookingTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("acceptBookingTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(context, getString(R.string.booking_unsuccess),
                                errorMsg, getString(R.string.ok),
                                "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        dismissBookingAlert();
                                    }
                                });
                    }
                }, "acceptBookingTask");

    }

    private void rejectBookingTask(final Context context, Booking booking) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(context, getString(R.string.rejecting_booking));
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().rejectBooking, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                if (countDownTimer != null) {
                                    countDownTimer.cancel();
                                }
                                dismissBookingAlert();
                                AlertUtils.simpleAlert(context, false, getString(R.string.success),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                            }
                                        });
                            } else {
                                AlertUtils.simpleAlert(context, false, getString(R.string.error),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Logger.e("rejectBookingTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("rejectBookingTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(context, getString(R.string.error),
                                errorMsg, getString(R.string.ok),
                                "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                    }
                                });
                    }
                }, "rejectBookingTask");

    }

    private void startTimer(int time) {
        long totalTimeCountInMilliseconds = time * 1000;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 1) {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;

                String textTime = String.format("%02d", seconds / 60) + "." + String.format("%02d", seconds % 60)
                        + " " + getString(R.string.seconds_to_accept_the_booking);
                SpannableString ss1 = new SpannableString(textTime);
                ss1.setSpan(new RelativeSizeSpan(1.8f), 0, 5, 0); // set size
                textViewShowTime.setText(ss1);
            }

            @Override
            public void onFinish() {
                textViewShowTime.setText("");
                textViewShowTime.setVisibility(View.VISIBLE);
                dismissBookingAlert();
            }
        }.start();
        Logger.e("Booking Alert", "8888 start timer for time:" + time);
    }

    public void recycle() {
        try {
            unregisterReceiver(bookingReceiver);
            if (socket != null) {
                if (socket.connected()) {
                    socket.disconnect();
                }
                socket = null;
            }
        } catch (Exception e) {
        }
        bookingReceiver = null;
        if (bookingDialog != null) {
            if (bookingDialog.isShowing()) {
                dismissBookingAlert();
            }
            bookingDialog = null;
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    public void checkFromPush(Intent intent) {
        try {
            if (intent.getBooleanExtra("fromPush", false)) {
                Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
            }
        } catch (Exception e) {
        }
    }

    //    //test booking generate here
    public void generateBooking() {
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("other_charges", "12.22");
        dataMap.put("operator_service_fee", "32.23");
        dataMap.put("booking_no", "0001L");
        dataMap.put("max_time_to_driver_response", "36000");
        dataMap.put("journey_time", "23");
        dataMap.put("cabbi_service_fee", "46.76");
        dataMap.put("pickup_lat", "27.689163");
        dataMap.put("pickup_lng", "85.312430");
        dataMap.put("pickup_location", "GuruDwar Kupondal Lalitpur Nepal");
        dataMap.put("dropoff_lat", "27.688498");
        dataMap.put("dropoff_lng", "85.335974");
        dataMap.put("dropoff_location", "BICC New Baneshowor Kathamandu Nepal");
        dataMap.put("trip_fare", "432");
        dataMap.put("passenger_name", "theone sharma");
        dataMap.put("mobile_no", "980763242");
        dataMap.put("trip_status", "0");
        dataMap.put("distance", "8");
        dataMap.put("corporate_service_fee", "0.00");
        dataMap.put("datetime", "2018-10-10 08:20:49");
        dataMap.put("booking_type", "0");


        Booking booking = BookingParser.bookingFromPush(dataMap);
        booking.setFareType("Fixed Fare");
        booking.setNote("this is some note of booking ***");
        Intent intent1 = new Intent(BookingBaseActivity.INTENT_BOOKING_BROADCAST);
        intent1.putExtra("booking", booking);
        sendBroadcast(intent1);
    }

    public void autoCloseBookingTask() {
        isAutoDismissed = true;
        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();

        volleyHelper.addVolleyRequestListeners(Api.getInstance().autoCloseApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                    }
                }, "autoCloseBookingTask");
    }


    //socket io
    public static Socket socket;

    private void connectToServerSocket() {
        if (socket != null && socket.connected()) {
            return;
        }
        try {
            socket = IO.socket(Api.getInstance().socketUrl);
            Logger.e(TAG, "connectToServerSocket URL:" + Api.getInstance().socketUrl);
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e(TAG + " connectToServerSocket event_connect", "****Connected : " + args);
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e(TAG + " connectToServerSocket event_disconnected", "Connection lost )))) : " + args);
                }

            });
            socket.connect();
        } catch (URISyntaxException e) {
            Logger.e(TAG + " connectToServerSocket ex", e.getMessage() + " ");
        }
    }

    //    START NODE JS LISTENERS
    public void emitJobAccept(String response) {
        if (socket.connected()) {
            Logger.e(TAG + " emitJobAccept emit", "emitter for socket connected and join to room");
            //emitter to post data
            try {
                JSONObject roomObj = new JSONObject(response);
                Logger.e(TAG + " nodeJs emitJobAccept emit data", roomObj.toString() + " **");

                socket.emit("send_driver_response", roomObj, new Ack() {
                    @Override
                    public void call(Object... args) {
                        Logger.e(TAG + " emitJobAccept ack",
                                "Ack received for send_driver_response :" +
                                        (args.length > 0 ? args[0].toString() : "0"));
                    }
                });
            } catch (JSONException e) {
                Logger.e(TAG + " emitJobAccept ex15", e.getMessage() + "");
            }
        }
    }

//    public void listenForReadyTeam() {
//        if (socket.connected()) {
//            Logger.e(TAG + " nodeJs listener listenForReadyTeam", "listener set for listenForReadyTeam");
//
//            socket.on("get_team_join_information", new Emitter.Listener() {
//                @SuppressLint("NewApi")
//                @Override
//                public void call(final Object... args) {
//                    Logger.e(TAG + " nodeJs response listenForReadyTeam -->", args.length + " __");
//
//                    if (args.length > 0) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Logger.e(TAG + " nodeJs listenForReadyTeam res", args[0].toString() + " **");
//                                try {
//                                    JSONObject resObj = new JSONObject(args[0].toString());
//                                } catch (JSONException e) {
//                                    Logger.e(TAG + " nodeJs listenForReadyTeam listen ex", e.getMessage() + " ");
//                                }
//                            }
//                        });
//                    }
//                }
//            });
//        } else {
//            Logger.e(TAG + " nodeJs listenForReadyTeam", "not connected !!!");
//        }
//    }

}
