package com.emts.comfortgo.model;

import java.io.Serializable;

public class CityModel implements Serializable {
    private String id;
    private String city;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return this.city;
    }
}
