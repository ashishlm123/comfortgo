package com.emts.comfortgo.model;

import java.io.Serializable;

public class RaceModel implements Serializable {
    private String id;
    private String race;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    @Override
    public String toString() {
        return this.race;
    }
}
