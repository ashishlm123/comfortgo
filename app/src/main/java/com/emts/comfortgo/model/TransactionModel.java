package com.emts.comfortgo.model;

import java.io.Serializable;

public class TransactionModel implements Serializable {
    public static final String TRANSACTION_PENDING = "0";
    public static final String TRANSACTION_APPROVED = "1";
    public static final String TRANSACTION_REJECTED = "2";

    public static final String TRANSACTION_TOP_UP = "1";
    public static final String TRANSACTION_DEDUCTION = "2";

    private String id;
    private String topUpID;
    private String bookingNo;
    private String commissionDeductionAmt;
    private String creditPurchaseAmt;
    private String creditPackageName;
    private String txnDate;
    private String txnStatus;
    private String txnType;
    private String receiptImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopUpID() {
        return topUpID;
    }

    public void setTopUpID(String topUpID) {
        this.topUpID = topUpID;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getCommissionDeductionAmt() {
        return commissionDeductionAmt;
    }

    public void setCommissionDeductionAmt(String commissionDeductionAmt) {
        this.commissionDeductionAmt = commissionDeductionAmt;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getCreditPurchaseAmt() {
        return creditPurchaseAmt;
    }

    public void setCreditPurchaseAmt(String creditPurchaseAmt) {
        this.creditPurchaseAmt = creditPurchaseAmt;
    }

    public String getReceiptImage() {
        return receiptImage;
    }

    public void setReceiptImage(String receiptImage) {
        this.receiptImage = receiptImage;
    }

    public String getCreditPackageName() {
        return creditPackageName;
    }

    public void setCreditPackageName(String creditPackageName) {
        this.creditPackageName = creditPackageName;
    }
}
