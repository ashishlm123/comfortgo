package com.emts.comfortgo.model;

import java.io.Serializable;

public class CreditsModel implements Serializable {
    private String id;
    private String packageName;
    private String creditPrice;
    private String creditQty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getCreditPrice() {
        return creditPrice;
    }

    public void setCreditPrice(String creditPrice) {
        this.creditPrice = creditPrice;
    }

    public String getCreditQty() {
        return creditQty;
    }

    public void setCreditQty(String creditQty) {
        this.creditQty = creditQty;
    }

    @Override
    public String toString() {
        return this.packageName;
    }
}
