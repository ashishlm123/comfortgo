package com.emts.comfortgo.model;

import java.io.Serializable;

public class BankModel implements Serializable {
    private String id;
    private String bank;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Override
    public String toString() {
        return this.bank;
    }
}
