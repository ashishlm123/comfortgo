package com.emts.comfortgo.model;

import java.io.Serializable;

public class Booking implements Serializable {
    public static final String TRIP_STATUS_WAITING = "0";
    public static final String TRIP_STATUS_SCHEDULED = "1";
    public static final String TRIP_STATUS_IN_PROGRESS = "2";
    public static final String TRIP_STATUS_COMPLETED = "3";

    public static final String FARE_TYPE_FIXED = "0";
    public static final String FARE_TYPE_METERED = "1";

    public static final int VEHICLE_TYPE_CFGO = 1;
    public static final int VEHICLE_TYPE_CFCAR = 2;
    public static final int VEHICLE_TYPE_CFTAXI = 3;
    public static final int VEHICLE_TYPE_CFPREMIUM = 4;
    public static final int VEHICLE_TYPE_CF6SEATERS = 5;

    private String bookingNo, date, status, pickUpLocation, dropOffLocation, distance, tripTime, tripFare,
            tripFareMax, passengerName, passengerContact, bookingFee, tollCharge, commissionDeducted, calLCenterFee,
            fareType, note;
    private double pickUpLat, pickUpLong, dropOffLat, dropOffLong;
    private int timeToLive;

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(String dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTripTime() {
        return tripTime;
    }

    public void setTripTime(String tripTime) {
        this.tripTime = tripTime;
    }

    public String getTripFare() {
        return tripFare;
    }

    public void setTripFare(String tripFare) {
        this.tripFare = tripFare;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(int timeToLive) {
        this.timeToLive = timeToLive;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerContact() {
        return passengerContact;
    }

    public void setPassengerContact(String passengerContact) {
        this.passengerContact = passengerContact;
    }

    public double getPickUpLat() {
        return pickUpLat;
    }

    public void setPickUpLat(double pickUpLat) {
        this.pickUpLat = pickUpLat;
    }

    public double getPickUpLong() {
        return pickUpLong;
    }

    public void setPickUpLong(double pickUpLong) {
        this.pickUpLong = pickUpLong;
    }

    public double getDropOffLat() {
        return dropOffLat;
    }

    public void setDropOffLat(double dropOffLat) {
        this.dropOffLat = dropOffLat;
    }

    public double getDropOffLong() {
        return dropOffLong;
    }

    public void setDropOffLong(double dropOffLong) {
        this.dropOffLong = dropOffLong;
    }

    public String getBookingFee() {
        return bookingFee;
    }

    public void setBookingFee(String bookingFee) {
        this.bookingFee = bookingFee;
    }

    public String getTollCharge() {
        return tollCharge;
    }

    public void setTollCharge(String tollCharge) {
        this.tollCharge = tollCharge;
    }

    public String getCommissionDeducted() {
        return commissionDeducted;
    }

    public void setCommissionDeducted(String commissionDeducted) {
        this.commissionDeducted = commissionDeducted;
    }

    public String getCalLCenterFee() {
        return calLCenterFee;
    }

    public void setCalLCenterFee(String calLCenterFee) {
        this.calLCenterFee = calLCenterFee;
    }

    public String getFareType() {
        return fareType;
    }

    public void setFareType(String fareType) {
        this.fareType = fareType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTripFareMax() {
        return tripFareMax;
    }

    public void setTripFareMax(String tripFareMax) {
        this.tripFareMax = tripFareMax;
    }
}
