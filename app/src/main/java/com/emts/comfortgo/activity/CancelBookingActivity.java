package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.PassengerNotShowReceiver;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CancelBookingActivity extends BookingBaseActivity {
    PreferenceHelper activityPrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cancel_booking);

        activityPrefs = PreferenceHelper.getInstance(this);

        ImageView closeIcon = findViewById(R.id.close);
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
            return;
        }
        final Booking booking = (Booking) intent.getSerializableExtra("booking");
        if (booking == null) {
            onBackPressed();
            return;
        }
        boolean isArrived = intent.getBooleanExtra("isArrived", false);

        final RadioGroup rgReason = findViewById(R.id.rgReason);
        final RadioButton rbNotShown = findViewById(R.id.rbNotShown);
        rbNotShown.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.gotham_medium));
        final RadioButton rbOtherReason = findViewById(R.id.rbOtherReason);
        rbOtherReason.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.gotham_medium));
        final RadioButton rbCannotArrive = findViewById(R.id.rcCannotArive);
        rbCannotArrive.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.gotham_medium));

        final EditText etRejectReason = findViewById(R.id.et_reason);
        rgReason.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.rbOtherReason) {
                    etRejectReason.setEnabled(true);
                } else {
                    etRejectReason.setEnabled(false);
                }
            }
        });

        if (!isArrived) {
            rbNotShown.setVisibility(View.GONE);
        } else {
            rbNotShown.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (activityPrefs.getString(PassengerNotShowReceiver.PASSENGER_NOT_SHOWN_FOR_BOOKING, "").equals(booking.getBookingNo())) {
                        return false;
                    } else {
                        Toast.makeText(CancelBookingActivity.this, "You can enable this option after 5 minutes only",
                                Toast.LENGTH_LONG).show();
                        return true;
                    }
                }
            });
        }
        rgReason.check(rbCannotArrive.getId());

        final Button btnSubmit = findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbOtherReason.isChecked() && TextUtils.isEmpty(etRejectReason.getText().toString().trim())) {
                    AlertUtils.showSnack(CancelBookingActivity.this, btnSubmit, getString(R.string.enter_reason_first));
                } else {
                    cancelBookingTask(booking.getBookingNo(), rgReason, etRejectReason.getText().toString().trim());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        try {
            VolleyHelper.getInstance(this).cancelRequest("cancelBookingTask");
        } catch (Exception e) {
            Logger.e("cancelBookingTask onBackPressed Ex", e.getMessage());
        }
        super.onBackPressed();
    }

    private void cancelBookingTask(String bookingId, RadioGroup rg, String reason) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(CancelBookingActivity.this,
                getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(CancelBookingActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("booking_id", bookingId);

        int checkedButtonId = rg.getCheckedRadioButtonId();

        if (checkedButtonId == R.id.rbNotShown) {
            postParams.put("passenger_not_seen", "1");
        } else if (checkedButtonId == R.id.rcCannotArive) {
            postParams.put("cannot_arrive", "1");
        } else {
            postParams.put("cancel_reason", reason);
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().cancelBooking, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                emitJobAccept(response);
                                AlertUtils.simpleAlert(CancelBookingActivity.this, getString(R.string.booking_cancelled),
                                        resObj.getString("message"), getString(R.string.ok), "",
                                        new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                CancelBookingActivity.this.finish();
                                            }
                                        });
                                setResult(RESULT_OK);
                            } else {
                                AlertUtils.simpleAlert(CancelBookingActivity.this, getString(R.string.error),
                                        resObj.getString("message"), getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("cancelBookingTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.simpleAlert(CancelBookingActivity.this, getString(R.string.error),
                                    errorObj.getString("message"),
                                    getString(R.string.ok), "", null);
                        } catch (JSONException e) {
                            Logger.e("cancelBookingTask error ex", e.getMessage() + " ");
                        }
                    }
                }, "cancelBookingTask");
    }

}
