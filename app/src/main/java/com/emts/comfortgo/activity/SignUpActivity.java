package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.MyInput1;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity {
    MyInput1 edtMobile, edtPassword, edtEmailID;
    TextView tvErrorMobile, tvErrorPassword, tvErrorEmail;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(R.string.title_signup);

        edtMobile = findViewById(R.id.edt_mobile);
        edtPassword = findViewById(R.id.edt_password);
        edtEmailID = findViewById(R.id.edt_email_address);

        tvErrorEmail = findViewById(R.id.error_email);
        tvErrorPassword = findViewById(R.id.error_password);
        tvErrorMobile = findViewById(R.id.error_mobile);

        final Button btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String deviceId = instanceIdResult.getToken();
                                signUpTask(deviceId);
                            }
                        });
                    } else {
                        AlertUtils.showSnack(SignUpActivity.this, btnNext, getResources().getString(R.string.no_internet));
                    }
                }
            }
        });
    }

    private void signUpTask(String deviceId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(SignUpActivity.this, getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("email", edtEmailID.getText().toString().trim());
        postParams.put("mobile_no", edtMobile.getText().toString().trim());
        postParams.put("password", edtPassword.getText().toString().trim());
        postParams.put("device_id", deviceId);

        vHelper.addVolleyRequestListeners(Api.getInstance().signUpApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent(getApplicationContext(), OtpVerificationActivity.class);

//                                prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_TOKEN, res.getString("user_token")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_ID, res.getString("user_id")).apply();

                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                final String errorMsg = res.getString("message");
                                AlertUtils.simpleAlert(SignUpActivity.this, "SignUp Error", errorMsg, "Ok", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("signUpTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("signUpTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(SignUpActivity.this, getString(R.string.error), errorMsg);
                    }
                }, "signUpTask");


    }

    private boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(edtMobile.getText())) {
            isValid = false;
            tvErrorMobile.setVisibility(View.VISIBLE);
            tvErrorMobile.setText(R.string.error_field_required);
        } else {
            if (!Patterns.PHONE.matcher(edtMobile.getText().toString().trim()).matches()) {
                isValid = false;
                tvErrorMobile.setVisibility(View.VISIBLE);
                tvErrorMobile.setText(getString(R.string.invalid_number));
            } else {
                tvErrorMobile.setVisibility(View.GONE);
            }
        }

        if (TextUtils.isEmpty(edtEmailID.getText().toString().trim())) {
            isValid = false;
            tvErrorEmail.setVisibility(View.VISIBLE);
            tvErrorEmail.setText(R.string.error_field_required);
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(edtEmailID.getText().toString().trim()).matches()) {
                isValid = false;
                tvErrorEmail.setVisibility(View.VISIBLE);
                tvErrorEmail.setText(R.string.error_valid_email);
            } else {
                tvErrorEmail.setVisibility(View.GONE);
            }
        }

        if (TextUtils.isEmpty(edtPassword.getText().toString().trim())) {
            isValid = false;
            tvErrorPassword.setVisibility(View.VISIBLE);
            tvErrorPassword.setText(R.string.error_field_required);
        } else {
            if (edtPassword.getText().toString().trim().length() >= 6) {
                tvErrorPassword.setVisibility(View.GONE);
            } else {
                isValid = false;
                tvErrorPassword.setText(R.string.password_should_be_more_than_six);
                tvErrorPassword.setVisibility(View.VISIBLE);
            }
        }

        return isValid;
    }

    //checking if the password is alphanumeric or not
    public boolean containAlphanumeric(final String str) {
        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=()!?<>';:|`~*/]).{5,13}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z]).{5,13}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }
}
