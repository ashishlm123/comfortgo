package com.emts.comfortgo.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.adapter.TripHistoryAdapter;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.TripModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class TripHistoryActivity extends BookingBaseActivity {
    int REQUEST_CODE = 111;
    PreferenceHelper prefsHelper;
    ArrayList<TripModel> tripHistoryLists, filterLists;
    TripHistoryAdapter tripHistoryAdapter;
    RecyclerView rvTripHistoryListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;

    TextView tvTotalEarning, tvTotalTripsMade;
    ImageView filterIcon;

    LinearLayout layTripInfo;

    String startDate = "", endDate = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_trip_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.trip_history));

        progressBar = findViewById(R.id.progress_bar);
        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
        tvErrorText = findViewById(R.id.error_text);
        layTripInfo = findViewById(R.id.lay_trip_info);
        tvTotalEarning = findViewById(R.id.tv_total_earning_from_trip);
        tvTotalTripsMade = findViewById(R.id.tv_total_trips_made);

        rvTripHistoryListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvTripHistoryListings.setLayoutManager(linearLayoutManager);

        tripHistoryLists = new ArrayList<>();
        filterLists = new ArrayList<>();
        tripHistoryAdapter = new TripHistoryAdapter(TripHistoryActivity.this, filterLists);
        rvTripHistoryListings.setAdapter(tripHistoryAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            tripHistoryListingTask();
        } else {
            progressBar.setVisibility(View.GONE);
            rvTripHistoryListings.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }

        filterIcon = findViewById(R.id.ic_filter);
        filterIcon.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FilterTripHistoryActivity.class);
                intent.putExtra("startDate", startDate);
                intent.putExtra("endDate", endDate);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

    }


    private void tripHistoryListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        layTripInfo.setVisibility(View.GONE);
        rvTripHistoryListings.setVisibility(View.GONE);


        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().tripHistoryApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray tripsArray = res.getJSONArray("trips_data");
                        filterLists.clear();

                        if (tripsArray.length() > 0) {
                            for (int i = 0; i < tripsArray.length(); i++) {
                                JSONObject eachTrips = tripsArray.getJSONObject(i);
                                TripModel tripModel = new TripModel();
                                tripModel.setTotalTrips(eachTrips.getString("trips_per_day"));
                                tripModel.setTripDate(eachTrips.getString("trip_date"));
                                tripModel.setTotalIncome(eachTrips.getString("trip_income"));
                                tripModel.setTripStatus(eachTrips.getString("trip_status"));
                                tripModel.setTripBookingStatus(eachTrips.getString("booking_status"));

                                tripHistoryLists.add(tripModel);
                            }
                            tvTotalTripsMade.setText(res.getString("total_trips"));
                            tvTotalEarning.setText(getString(R.string.currency_myr) + " " + res.getString("total_income"));
                            layTripInfo.setVisibility(View.VISIBLE);
                            filterLists.addAll(tripHistoryLists);
                            tripHistoryAdapter.notifyDataSetChanged();
                            rvTripHistoryListings.setVisibility(View.VISIBLE);
                            tvErrorText.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        } else {
                            tvErrorText.setText(getString(R.string.trip_history_not_available));
                            layTripInfo.setVisibility(View.GONE);
                            tvErrorText.setVisibility(View.VISIBLE);
                            rvTripHistoryListings.setVisibility(View.GONE);
                        }
                    } else {
                        rvTripHistoryListings.setVisibility(View.GONE);
                        layTripInfo.setVisibility(View.GONE);
                        tvErrorText.setText(res.getString("message"));
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("tripHistoryListingTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errorMsg = getString(R.string.unexpected_error_try_again);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("tripHistoryListingTask error ex", e.getMessage() + " ");
                }
                tvErrorText.setText(errorMsg);
                tvErrorText.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                rvTripHistoryListings.setVisibility(View.GONE);
                layTripInfo.setVisibility(View.GONE);
            }
        }, "tripHistoryListingTask");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                startDate = data.getStringExtra("startDate");
                endDate = data.getStringExtra("endDate");

                Logger.e("startDate", String.valueOf(startDate));
                Logger.e("endDate", String.valueOf(endDate));
                filterMethod();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == REQUEST_CODE) {
                endDate = "";
                startDate = "";
//                filterIcon.setImageResource(R.drawable.btn_filter);
            }
        }
    }

    public void filterMethod() {
        filterLists.clear();
        Date dateStart = null;
        Date dateEnd = null;
        SimpleDateFormat sD = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat sD1 = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            if (!TextUtils.isEmpty(startDate)) {
                dateStart = sD1.parse(startDate);
                // to include the start date also check start date minus a day (but must be last second of prev day)
                cal.setTime(dateStart);
                cal.add(Calendar.DATE, -1);
                cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
                cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
                cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
                dateStart = cal.getTime();
            }
            dateEnd = sD1.parse(endDate);
            // to include the start date also check start date minus a day
            cal.setTime(dateEnd);
            cal.add(Calendar.DATE, +1);
            cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
            cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
            dateEnd = cal.getTime();
        } catch (ParseException e) {
            Logger.e("inputDate Parse Exception: ", e.getMessage());
        }

        Logger.e("filter date", dateStart + " " + dateEnd);

        if (dateStart == null && dateEnd == null) {
            filterLists.addAll(tripHistoryLists);
            tripHistoryAdapter.notifyDataSetChanged();
//            filterIcon.setImageResource(R.drawable.btn_filter);
            return;
        }
        for (int i = 0; i < tripHistoryLists.size(); i++) {
            Date dateToCheck = null;
            try {
                dateToCheck = sD1.parse(tripHistoryLists.get(i).getTripDate());
            } catch (ParseException e) {
                Logger.e("dateToCheck Parse Exception: ", e.getMessage());
            }
            Logger.e("date to check parse date ", tripHistoryLists.get(i).getTripDate() + " " + dateToCheck);
            if (dateToCheck != null) {
                if (dateStart != null && !dateToCheck.after(dateStart)) {
                    continue;
                }
                if (dateEnd != null && !dateToCheck.before(dateEnd)) {
                    continue;
                }
            }
            filterLists.add(tripHistoryLists.get(i));
        }

        tripHistoryAdapter.notifyDataSetChanged();
//        filterIcon.setImageResource(R.drawable.btn_filter_notification);
    }


}
