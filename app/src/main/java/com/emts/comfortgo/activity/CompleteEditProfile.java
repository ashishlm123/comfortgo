package com.emts.comfortgo.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.ImageHelper;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.Utils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.BankModel;
import com.emts.comfortgo.model.CityModel;
import com.emts.comfortgo.model.LanguageModel;
import com.emts.comfortgo.model.RaceModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class CompleteEditProfile extends AppCompatActivity {
    EditText etDriverId, etFullName, etICNumber, etAddress, etClass, etLicenseStartDate, etLicenseEndDate, etBankName,
            etVehiclePlate, etVehicleModel, etRegistrationYear, etJoinedDate, etBankAcNo, etRecepientName;
    Spinner spRace, spLanguages, spCity, spBank;

    ImageView btnAddProfilePic, btnAddNric, btnAddLicense, btnAddVocationalLicense, btnAddCarInsuranceCoverNote,
            btnAddCarGrant, btnAddCarPermit;
    Button btnSubmit;

    String profileImagePath = "";
    String nricImagePath = "";
    String licenseImagePath = "";
    String vocationalLicenseImagePath = "";
    String carInsuranceCoverNoteImagePath = "";
    String carGrantImagePath = "";
    String carPermitImagePath = "";

    public static Uri profileFileUri, nricFileUri, licenseFileUri, vocationalLicenseFileUri, carInsuranceCoverNoteFileUri,
            carGrantFileUri, carPermitFileUri;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    //camera capture request codes for all images
    public static final int CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE = 6542;
    public static final int CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE = 6543;
    public static final int CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE = 6544;
    public static final int CAMERA_CAPTURE_VOCATIONAL_LICENSE_IMAGE_REQUEST_CODE = 6545;
    public static final int CAMERA_CAPTURE_CAR_INSURANCE_COVER_NOTE_IMAGE_REQUEST_CODE = 6546;
    public static final int CAMERA_CAPTURE_CAR_GRANT_IMAGE_REQUEST_CODE = 6547;
    public static final int CAMERA_CAPTURE_CAR_PERMIT_IMAGE_REQUEST_CODE = 6548;

    //gallery load image request codes for all images
    public static final int RESULT_LOAD_IMAGE_PROFILE = 7653;
    public static final int RESULT_LOAD_IMAGE_NRIC = 7654;
    public static final int RESULT_LOAD_IMAGE_LICENSE = 7655;
    public static final int RESULT_LOAD_IMAGE_VOCATIONAL_LICENSE = 7656;
    public static final int RESULT_LOAD_IMAGE_CAR_INSURANCE_COVER_NOTE = 7657;
    public static final int RESULT_LOAD_IMAGE_CAR_GRANT = 7658;
    public static final int RESULT_LOAD_IMAGE_CAR_PERMIT = 7669;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");

    Calendar startDateCalendar, endDateCalendar, joinedDateCalendar, calendarNow;

    ArrayList<LanguageModel> languageLists = new ArrayList<>();
    ArrayList<CityModel> cityLists = new ArrayList<>();
    ArrayList<RaceModel> raceLists = new ArrayList<>();
    ArrayList<BankModel> bankLists = new ArrayList<>();

    PreferenceHelper prefsHelper;
    boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_complete_edit_profile);
        calendarNow = Calendar.getInstance();

        Intent intent = getIntent();
        isEdit = intent.getBooleanExtra("isEdit", false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);

        init();
        String response = intent.getStringExtra("response");
        if (isEdit) {
            toolbarTitle.setText(getString(R.string.edit_profile));
            try {
                JSONObject res = new JSONObject(response);
                JSONObject driverDetails = res.getJSONObject("driver_info");
                parseLanguage(res.getJSONArray("language").toString(), driverDetails.getString("languages"));
                parseCity(res.getJSONArray("city").toString(), driverDetails.getString("city"));
                parseRace(res.getJSONArray("race").toString(), driverDetails.getString("race"));
                parseBank(res.getJSONArray("banks").toString(), "0");


                String profileDirectory = driverDetails.getString("profile_img_directory");
                String profilePic = driverDetails.getString("profile_photo");

                if (!TextUtils.isEmpty(profilePic) && !profilePic.equalsIgnoreCase("null")) {
                    prefsHelper.edit().putString(PreferenceHelper.USER_PROFILE_PIC, profileDirectory + profilePic).apply();
                }
                Glide.with(CompleteEditProfile.this).load(prefsHelper.getAppUserImg()).into(btnAddProfilePic);
                profileImagePath = profileDirectory + profilePic;

                prefsHelper.edit().putString(PreferenceHelper.DRIVER_ID, driverDetails.getString("user_name")).apply();
                etDriverId.setText(driverDetails.getString("user_name"));

                prefsHelper.edit().putString(PreferenceHelper.USER_FULL_NAME, driverDetails.getString("full_name")).apply();
                etFullName.setText(driverDetails.getString("full_name"));

                prefsHelper.edit().putString(PreferenceHelper.USER_CREDIT_BALANCE, driverDetails.getString("credit_balance")).apply();

                etICNumber.setText(driverDetails.getString("lc_no"));
                etAddress.setText(driverDetails.getString("home_address"));
//                etMobileNo.setText(driverDetails.getString("mobile_no"));
//                etEmail.setText(driverDetails.getString("email"));

                etClass.setText(driverDetails.getString("class"));

                String licenseStartDate = driverDetails.getString("lc_valid_from");
                String licenseEndDate = driverDetails.getString("lc_valid_to");
                String joinDate = driverDetails.getString("joined_date");

                try {
                    Date startDate = sdf.parse(licenseStartDate);
                    Date endDate = sdf.parse(licenseEndDate);
                    Date joinedDate = sdf.parse(joinDate);

                    etLicenseStartDate.setText(sdf1.format(startDate));
                    etLicenseEndDate.setText(sdf1.format(endDate));
                    etJoinedDate.setText(sdf1.format(joinedDate));
                } catch (ParseException e) {
                    Logger.e("date exception", e.getMessage());
                }

                etVehiclePlate.setText(driverDetails.getString("plate_no"));
                etVehicleModel.setText(driverDetails.getString("vehicle_model"));
                etRegistrationYear.setText(driverDetails.getString("year_of_mfg"));
                etBankName.setText(driverDetails.getString("bank_name"));
                etBankAcNo.setText(driverDetails.getString("ac_no"));
                etRecepientName.setText(driverDetails.getString("recipient_name"));

                String nricPhoto = driverDetails.getString("nric_photo");
                String nricDirectory = driverDetails.getString("nric_img_directory");
                if (!TextUtils.isEmpty(nricPhoto) && !nricPhoto.equalsIgnoreCase("null")) {
                    Glide.with(CompleteEditProfile.this).load(nricDirectory + nricPhoto).into(btnAddNric);
                }
                nricImagePath = nricDirectory + nricPhoto;

                String licensePhoto = driverDetails.getString("lc_photo");
                String licenseDirectory = driverDetails.getString("driving_license_img_directory");
                if (!TextUtils.isEmpty(licensePhoto) && !licensePhoto.equalsIgnoreCase("null")) {
                    Glide.with(CompleteEditProfile.this).load(licenseDirectory + licensePhoto).into(btnAddLicense);
                }
                licenseImagePath = licenseDirectory + licensePhoto;

                String vocationalLicensePhoto = driverDetails.getString("vocational_lc");
                String vocationalDirectory = driverDetails.getString("vocational_img_directory");
                if (!TextUtils.isEmpty(vocationalLicensePhoto) && !vocationalLicensePhoto.equalsIgnoreCase("null")) {
                    Glide.with(CompleteEditProfile.this).load(vocationalDirectory + vocationalLicensePhoto).into(btnAddVocationalLicense);
                }
                vocationalLicenseImagePath = vocationalDirectory + vocationalLicensePhoto;

                String carInsuranceCoverNotePhoto = driverDetails.getString("insurance_photo");
                String carInsuranceDirectory = driverDetails.getString("vehicle_insurance_img_directory");
                if (!TextUtils.isEmpty(carInsuranceCoverNotePhoto) && !carInsuranceCoverNotePhoto.equalsIgnoreCase("null")) {
                    Glide.with(CompleteEditProfile.this).load(carInsuranceDirectory + carInsuranceCoverNotePhoto).into(btnAddCarInsuranceCoverNote);
                }
                carInsuranceCoverNoteImagePath = carInsuranceDirectory + carInsuranceCoverNotePhoto;

                String carGrantPhoto = driverDetails.getString("car_grant");
                String carGrantDirectory = driverDetails.getString("car_grant_img_directory");
                if (!TextUtils.isEmpty(carGrantPhoto) && !carGrantPhoto.equalsIgnoreCase("null")) {
                    Glide.with(CompleteEditProfile.this).load(carGrantDirectory + carGrantPhoto).into(btnAddCarGrant);
                }
                carGrantImagePath = carGrantDirectory + carGrantPhoto;

                String carPermitPhoto = driverDetails.getString("tax_permit");
                String carPermitDirectory = driverDetails.getString("tax_permit_img_directory");
                if (!TextUtils.isEmpty(carPermitPhoto) && !carPermitPhoto.equalsIgnoreCase("null")) {
                    Glide.with(CompleteEditProfile.this).load(carPermitDirectory + carPermitPhoto).into(btnAddCarPermit);
                }
                carPermitImagePath = carPermitDirectory + carPermitPhoto;

            } catch (JSONException ex) {
                Logger.e("Response Json ex", ex.getMessage());
            }
        } else {
            toolbarTitle.setText(getString(R.string.complete_profile));
            parseLanguage(intent.getStringExtra("languages"), "");
            parseCity(intent.getStringExtra("cities"), "");
            parseRace(intent.getStringExtra("races"), "");
            parseBank(intent.getStringExtra("banks"), "");
        }


    }

    private boolean validation() {
//        if (TextUtils.isEmpty(etDriverId.getText().toString().trim())) {
//            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, "Please insert Driver Id.");
//            return false;
//        }
        if (TextUtils.isEmpty(etFullName.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_full_name));
            return false;
        }
        if (TextUtils.isEmpty(etICNumber.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_ic_number));
            return false;
        }
        if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_address));
            return false;
        }
        if (TextUtils.isEmpty(etClass.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_class));
            return false;
        }
        if (TextUtils.isEmpty(etLicenseStartDate.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_license_start_date));
            return false;
        }
        if (TextUtils.isEmpty(etLicenseEndDate.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_license_end_date));
            return false;
        }
        if (TextUtils.isEmpty(etVehiclePlate.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_plate_no));
            return false;
        }
        if (TextUtils.isEmpty(etVehicleModel.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_vehicle_model));
            return false;
        }
        if (TextUtils.isEmpty(etRegistrationYear.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_registration_year));
            return false;
        }
        if (TextUtils.isEmpty(etJoinedDate.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_joined_date));
            return false;
        }

        if (TextUtils.isEmpty(etBankName.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_bank_name));
            return false;
        }

        if (TextUtils.isEmpty(etBankAcNo.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_bank_ac_no));
            return false;
        }
        if (TextUtils.isEmpty(etRecepientName.getText().toString().trim())) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_receipent_name));
            return false;
        }
        if (TextUtils.isEmpty(profileImagePath)) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_profile_photo));
            return false;
        }
        if (TextUtils.isEmpty(nricImagePath)) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_nric));
            return false;
        }
        if (TextUtils.isEmpty(licenseImagePath)) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_license));
            return false;
        }
//        if (TextUtils.isEmpty(vocationalLicenseImagePath)) {
//            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, "Please insert Vocational License.");
//            return false;
//        }
        if (TextUtils.isEmpty(carInsuranceCoverNoteImagePath)) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_car_insurance));
            return false;
        }
        if (TextUtils.isEmpty(carGrantImagePath)) {
            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, getString(R.string.insert_car_grant));
            return false;
        }
//        if (TextUtils.isEmpty(carGrantImagePath)) {
//            AlertUtils.showSnack(CompleteEditProfile.this, btnSubmit, "Please insert Car Permit.");
//            return false;
//        }

        return true;
    }

    View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btn_add_profile_pic) {
                showImageChooseDialog(R.id.btn_add_profile_pic);
            } else if (view.getId() == R.id.btn_add_nric) {
                showImageChooseDialog(R.id.btn_add_nric);
            } else if (view.getId() == R.id.btn_add_nric) {
                showImageChooseDialog(R.id.btn_add_nric);
            } else if (view.getId() == R.id.btn_add_license) {
                showImageChooseDialog(R.id.btn_add_license);
            } else if (view.getId() == R.id.btn_add_vocational_license) {
                showImageChooseDialog(R.id.btn_add_vocational_license);
            } else if (view.getId() == R.id.btn_add_car_insurance_cover_note) {
                showImageChooseDialog(R.id.btn_add_car_insurance_cover_note);
            } else if (view.getId() == R.id.btn_add_car_grant) {
                showImageChooseDialog(R.id.btn_add_car_grant);
            } else if (view.getId() == R.id.btn_add_car_permit) {
                showImageChooseDialog(R.id.btn_add_car_permit);
            }
        }
    };

    private void init() {
        //Edit text
        etDriverId = findViewById(R.id.edt_driver_id);
        etFullName = findViewById(R.id.edt_full_name);
        etICNumber = findViewById(R.id.edt_ic_number);
        etAddress = findViewById(R.id.edt_address);
//        etEmail = findViewById(R.id.edt_email_address);
//        etEmail.setVisibility(View.GONE);
//        etMobileNo = findViewById(R.id.edt_mobile);
//        etMobileNo.setVisibility(View.GONE);
        etClass = findViewById(R.id.edt_class);
        etLicenseStartDate = findViewById(R.id.et_license_start_Date);
        etLicenseEndDate = findViewById(R.id.et_license_end_Date);
        etVehiclePlate = findViewById(R.id.edt_vehicle_plate_no);
        etVehicleModel = findViewById(R.id.edt_vehicle_model);
        etRegistrationYear = findViewById(R.id.et_year_registration);
        etJoinedDate = findViewById(R.id.edt_joined_date);
        etBankName = findViewById(R.id.edt_bank_name);
        etBankAcNo = findViewById(R.id.edt_bank_ac_no);
        etRecepientName = findViewById(R.id.edt_recipient_name);

        //Spinner
        spRace = findViewById(R.id.sp_race);
        spBank = findViewById(R.id.sp_bank);
        spCity = findViewById(R.id.sp_city);
        spLanguages = findViewById(R.id.sp_languages);

        //Imageview
        btnAddProfilePic = findViewById(R.id.btn_add_profile_pic);
        btnAddProfilePic.setOnClickListener(imageClickListener);

        btnAddNric = findViewById(R.id.btn_add_nric);
        btnAddNric.setOnClickListener(imageClickListener);

        btnAddLicense = findViewById(R.id.btn_add_license);
        btnAddLicense.setOnClickListener(imageClickListener);

        btnAddVocationalLicense = findViewById(R.id.btn_add_vocational_license);
        btnAddVocationalLicense.setOnClickListener(imageClickListener);

        btnAddCarInsuranceCoverNote = findViewById(R.id.btn_add_car_insurance_cover_note);
        btnAddCarInsuranceCoverNote.setOnClickListener(imageClickListener);

        btnAddCarGrant = findViewById(R.id.btn_add_car_grant);
        btnAddCarGrant.setOnClickListener(imageClickListener);

        btnAddCarPermit = findViewById(R.id.btn_add_car_permit);
        btnAddCarPermit.setOnClickListener(imageClickListener);

        //calendar
        startDateCalendar = Calendar.getInstance();
        endDateCalendar = Calendar.getInstance();
        joinedDateCalendar = Calendar.getInstance();

        pickStartEndDate();

        //Button
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String deviceId = instanceIdResult.getToken();
                                editProfileTask(deviceId);
                            }
                        });
                    } else {
                        AlertUtils.showSnack(CompleteEditProfile.this, etLicenseEndDate, getResources().getString(R.string.no_internet));
                    }
                }
            }
        });
    }

    private void editProfileTask(String deviceId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(CompleteEditProfile.this, getString(R.string.please_wait));

        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);

        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("device_id", deviceId);
        postParams.put("user_name", etDriverId.getText().toString().trim());
        postParams.put("full_name", etFullName.getText().toString().trim());
        postParams.put("lc_no", etICNumber.getText().toString().trim());
        postParams.put("user_id", prefsHelper.getUserId());
        postParams.put("user_token", prefsHelper.getUserToken());

        postParams.put("race", raceLists.get(spRace.getSelectedItemPosition()).getId());

//        for (int i = 0; i < languageLists.size(); i++) {
        postParams.put("language[" + 0 + "]", languageLists.get(spLanguages.getSelectedItemPosition()).getId());
//        }
        postParams.put("city", cityLists.get(spCity.getSelectedItemPosition()).getId());

        postParams.put("home_address", etAddress.getText().toString().trim());
//        postParams.put("mobile_no", etAddress.getText().toString().trim());
//        postParams.put("email", etEmail.getText().toString().trim());
        postParams.put("class", etClass.getText().toString().trim());
        postParams.put("valid_from_date", etLicenseStartDate.getText().toString().trim());
        postParams.put("valid_to_date", etLicenseEndDate.getText().toString().trim());
        postParams.put("vehicle_plate_no", etVehiclePlate.getText().toString().trim());
        postParams.put("vehicle_model", etVehicleModel.getText().toString().trim());
        postParams.put("join_date", etJoinedDate.getText().toString().trim());
        postParams.put("year_of_mfg", etRegistrationYear.getText().toString().trim());

        //Todo :- ahile name pathairako xa pachhi id pathaunu parxa

        if (!TextUtils.isEmpty(etBankName.getText().toString().trim())) {
            postParams.put("bank_name", etBankName.getText().toString().trim());
        }
//        postParams.put("bank_name", bankLists.get(spBank.getSelectedItemPosition()).getBank());
        postParams.put("ac_no", etBankAcNo.getText().toString().trim());
        postParams.put("recipient_name", etRecepientName.getText().toString().trim());

//        profile_photo, nric_photo, lc_photo, insurance_photo, vocational_lc, car_grant, tax_permit
        HashMap<String, String> fileParams = new HashMap<>();
        if (!TextUtils.isEmpty(profileImagePath) && !profileImagePath.contains("http")) {
            fileParams.put("profile_photo", profileImagePath);
        }
        if (!TextUtils.isEmpty(nricImagePath) && !nricImagePath.contains("http")) {
            fileParams.put("nric_photo", nricImagePath);
        }
        if (!TextUtils.isEmpty(licenseImagePath) && !licenseImagePath.contains("http")) {
            fileParams.put("lc_photo", licenseImagePath);
        }
        if (!TextUtils.isEmpty(carInsuranceCoverNoteImagePath) && !carInsuranceCoverNoteImagePath.contains("http")) {
            fileParams.put("insurance_photo", carInsuranceCoverNoteImagePath);
        }
        if (!TextUtils.isEmpty(vocationalLicenseImagePath) && !vocationalLicenseImagePath.contains("http")) {
            fileParams.put("vocational_lc", vocationalLicenseImagePath);
        }
        if (!TextUtils.isEmpty(carGrantImagePath) && !carGrantImagePath.contains("http")) {
            fileParams.put("car_grant", carGrantImagePath);
        }
        if (!TextUtils.isEmpty(carPermitImagePath) && !carPermitImagePath.contains("http")) {
            fileParams.put("tax_permit", carPermitImagePath);
        }

        volleyHelper.addMultipartRequest(Api.getInstance().editProfileApi, Request.Method.POST, postParams, fileParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject userDetails = res.getJSONObject("driver_details");
                                prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();
                                prefsHelper.edit().putBoolean(PreferenceHelper.PROFILE_COMPLETENESS, true).apply();
                                prefsHelper.edit().putString(PreferenceHelper.DRIVER_ID, userDetails.getString("user_name")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_ID, userDetails.getString("user_id")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_FULL_NAME, userDetails.getString("full_name")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_EMAIL, userDetails.getString("email")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_MOBILE_NO, userDetails.getString("mobile_no")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_CREDIT_BALANCE, userDetails.getString("credit_balance")).apply();
                                String plateNo = userDetails.optString("plate_no");
                                if (!TextUtils.isEmpty(plateNo)) {
                                    prefsHelper.edit().putString(PreferenceHelper.USER_VEHICLE_PLATE_NO, plateNo).apply();
                                }

                                String profilePic = userDetails.getString("profile_photo");
                                String profilePicDirectory = userDetails.getString("profile_img_directory");

                                if (!TextUtils.isEmpty(profilePic) && !profilePic.equals("null")) {
                                    prefsHelper.edit().putString(PreferenceHelper.USER_PROFILE_PIC, profilePicDirectory + profilePic).apply();
                                }

                                if (!isEdit) {
                                    AlertUtils.simpleAlert(CompleteEditProfile.this, getString(R.string.registration_successful),
                                            getString(R.string.success_sign_up_will_notify_on_approv), getString(R.string.ok), "",
                                            new AlertUtils.OnAlertButtonClickListener() {
                                                @Override
                                                public void onAlertButtonClick(boolean isPositiveButton) {
                                                    prefsHelper.edit().putBoolean(PreferenceHelper.OFFLINE, false).commit();
                                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                }
                                            });
                                } else {
                                    AlertUtils.simpleAlert(CompleteEditProfile.this, getString(R.string.success), res.getString("message"),
                                            getString(R.string.ok), "", null);
                                }
                            } else {
                                AlertUtils.simpleAlert(CompleteEditProfile.this, getString(R.string.error), res.getString("message"),
                                        getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("editProfileTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("editProfileTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(CompleteEditProfile.this, getString(R.string.error), errorMsg);
                    }
                }, "editProfileTask");
    }

    @SuppressLint("ClickableViewAccessibility")
    private void pickStartEndDate() {
        final DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                startDateCalendar.set(Calendar.YEAR, year);
                startDateCalendar.set(Calendar.MONTH, month);
                startDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                etLicenseStartDate.setText(day + "/" + (month + 1) + "/" + year);
            }
        };

        etLicenseStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(CompleteEditProfile.this, startDate, startDateCalendar
                            .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                            startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });

        final DatePickerDialog.OnDateSetListener expiryDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                endDateCalendar.set(Calendar.YEAR, year);
                endDateCalendar.set(Calendar.MONTH, month);
                endDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                etLicenseEndDate.setText(day + "/" + (month + 1) + "/" + year);
            }
        };

        etLicenseEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(CompleteEditProfile.this, expiryDate, endDateCalendar
                            .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                            endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });

        final DatePickerDialog.OnDateSetListener joinedDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                joinedDateCalendar.set(Calendar.YEAR, year);
                joinedDateCalendar.set(Calendar.MONTH, month);
                joinedDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                etJoinedDate.setText(day + "/" + (month + 1) + "/" + year);
            }
        };

        etJoinedDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(CompleteEditProfile.this, joinedDate, joinedDateCalendar
                            .get(Calendar.YEAR), joinedDateCalendar.get(Calendar.MONTH),
                            joinedDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE_PROFILE || requestCode == RESULT_LOAD_IMAGE_NRIC ||
                    requestCode == RESULT_LOAD_IMAGE_LICENSE || requestCode == RESULT_LOAD_IMAGE_VOCATIONAL_LICENSE ||
                    requestCode == RESULT_LOAD_IMAGE_CAR_INSURANCE_COVER_NOTE || requestCode == RESULT_LOAD_IMAGE_CAR_GRANT ||
                    requestCode == RESULT_LOAD_IMAGE_CAR_PERMIT) {

                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                assert selectedImageUri != null;
                CursorLoader cursorLoader = new CursorLoader(CompleteEditProfile.this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);

                Bitmap bm = ImageHelper.decodeImageFileWithCompression(selImgPath,
                        (int) Utils.pxFromDp(getApplicationContext(), 85f), (int) Utils.pxFromDp(getApplicationContext(), 85f));
                if (requestCode == RESULT_LOAD_IMAGE_PROFILE) {
                    btnAddProfilePic.setImageBitmap(bm);
                    profileImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(profileImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_NRIC) {
                    btnAddNric.setImageBitmap(bm);
                    nricImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(nricImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_LICENSE) {
                    btnAddLicense.setImageBitmap(bm);
                    licenseImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(licenseImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_VOCATIONAL_LICENSE) {
                    btnAddVocationalLicense.setImageBitmap(bm);
                    vocationalLicenseImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(vocationalLicenseImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_CAR_INSURANCE_COVER_NOTE) {
                    btnAddCarInsuranceCoverNote.setImageBitmap(bm);
                    carInsuranceCoverNoteImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(carInsuranceCoverNoteImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_CAR_PERMIT) {
                    btnAddCarPermit.setImageBitmap(bm);
                    carPermitImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(carPermitImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_CAR_GRANT) {
                    btnAddCarGrant.setImageBitmap(bm);
                    carGrantImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(carGrantImagePath, bm);
                }

            } else if (requestCode == CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE || requestCode == CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE
                    || requestCode == CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE || requestCode == CAMERA_CAPTURE_VOCATIONAL_LICENSE_IMAGE_REQUEST_CODE
                    || requestCode == CAMERA_CAPTURE_CAR_INSURANCE_COVER_NOTE_IMAGE_REQUEST_CODE
                    || requestCode == CAMERA_CAPTURE_CAR_GRANT_IMAGE_REQUEST_CODE || requestCode == CAMERA_CAPTURE_CAR_PERMIT_IMAGE_REQUEST_CODE) {
                if (requestCode == CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(profileFileUri).into(btnAddProfilePic);
                    ImageHelper.saveBitMapToFile(profileFileUri.getPath(), ImageHelper.bitmapFromFile(profileFileUri.getPath()));
                    profileImagePath = profileFileUri.getPath();
                } else if (requestCode == CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(nricFileUri).into(btnAddNric);
                    ImageHelper.saveBitMapToFile(nricFileUri.getPath(), ImageHelper.bitmapFromFile(nricFileUri.getPath()));
                    nricImagePath = nricFileUri.getPath();
                } else if (requestCode == CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(licenseFileUri).into(btnAddLicense);
                    ImageHelper.saveBitMapToFile(licenseFileUri.getPath(), ImageHelper.bitmapFromFile(licenseFileUri.getPath()));
                    licenseImagePath = licenseFileUri.getPath();
                } else if (requestCode == CAMERA_CAPTURE_VOCATIONAL_LICENSE_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(vocationalLicenseFileUri).into(btnAddVocationalLicense);
                    ImageHelper.saveBitMapToFile(vocationalLicenseFileUri.getPath(), ImageHelper.bitmapFromFile(vocationalLicenseFileUri.getPath()));
                    vocationalLicenseImagePath = vocationalLicenseFileUri.getPath();
                } else if (requestCode == CAMERA_CAPTURE_CAR_INSURANCE_COVER_NOTE_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(carInsuranceCoverNoteFileUri).into(btnAddCarInsuranceCoverNote);
                    ImageHelper.saveBitMapToFile(carInsuranceCoverNoteFileUri.getPath(), ImageHelper.bitmapFromFile(carInsuranceCoverNoteFileUri.getPath()));
                    carInsuranceCoverNoteImagePath = carInsuranceCoverNoteFileUri.getPath();
                } else if (requestCode == CAMERA_CAPTURE_CAR_GRANT_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(carGrantFileUri).into(btnAddCarGrant);
                    ImageHelper.saveBitMapToFile(carGrantFileUri.getPath(), ImageHelper.bitmapFromFile(carGrantFileUri.getPath()));
                    carGrantImagePath = carGrantFileUri.getPath();
                } else if (requestCode == CAMERA_CAPTURE_CAR_PERMIT_IMAGE_REQUEST_CODE) {
                    Glide.with(CompleteEditProfile.this).load(carPermitFileUri).into(btnAddCarPermit);
                    ImageHelper.saveBitMapToFile(carPermitFileUri.getPath(), ImageHelper.bitmapFromFile(carPermitFileUri.getPath()));
                    carPermitImagePath = carPermitFileUri.getPath();
                }
            }
        }
    }

    private void showImageChooseDialog(final int imagViewID) {
        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(CompleteEditProfile.this);
        builder.setTitle(R.string.choose_media);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getString(R.string.take_photo))) {
                    int imageRequestCode = 0;
                    if (imagViewID == R.id.btn_add_profile_pic) {
                        imageRequestCode = CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_nric) {
                        imageRequestCode = CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_license) {
                        imageRequestCode = CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_vocational_license) {
                        imageRequestCode = CAMERA_CAPTURE_VOCATIONAL_LICENSE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_car_insurance_cover_note) {
                        imageRequestCode = CAMERA_CAPTURE_CAR_INSURANCE_COVER_NOTE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_car_grant) {
                        imageRequestCode = CAMERA_CAPTURE_CAR_GRANT_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_car_permit) {
                        imageRequestCode = CAMERA_CAPTURE_CAR_PERMIT_IMAGE_REQUEST_CODE;
                    }
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(CompleteEditProfile.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                imageRequestCode);

                    } else {
                        takePhoto(imageRequestCode);
                    }
                } else if (options[item].equals(getString(R.string.choose_photo))) {
                    int imageRequestCode = 0;
                    if (imagViewID == R.id.btn_add_profile_pic) {
                        imageRequestCode = RESULT_LOAD_IMAGE_PROFILE;
                    } else if (imagViewID == R.id.btn_add_nric) {
                        imageRequestCode = RESULT_LOAD_IMAGE_NRIC;
                    } else if (imagViewID == R.id.btn_add_license) {
                        imageRequestCode = RESULT_LOAD_IMAGE_LICENSE;
                    } else if (imagViewID == R.id.btn_add_vocational_license) {
                        imageRequestCode = RESULT_LOAD_IMAGE_VOCATIONAL_LICENSE;
                    } else if (imagViewID == R.id.btn_add_car_insurance_cover_note) {
                        imageRequestCode = RESULT_LOAD_IMAGE_CAR_INSURANCE_COVER_NOTE;
                    } else if (imagViewID == R.id.btn_add_car_permit) {
                        imageRequestCode = RESULT_LOAD_IMAGE_CAR_PERMIT;
                    } else if (imagViewID == R.id.btn_add_car_grant) {
                        imageRequestCode = RESULT_LOAD_IMAGE_CAR_GRANT;
                    }

                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(CompleteEditProfile.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                imageRequestCode);

                    } else {
                        chooseFrmGallery(imageRequestCode);
                    }
                } else if (options[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(int requestImageCode) {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, requestImageCode);
    }

    public void takePhoto(int imageRequestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        // start the image capture Intent
        if (imageRequestCode == CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE) {
            profileFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, profileFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE) {
            nricFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, nricFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE) {
            licenseFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, licenseFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_VOCATIONAL_LICENSE_IMAGE_REQUEST_CODE) {
            vocationalLicenseFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, vocationalLicenseFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_CAR_INSURANCE_COVER_NOTE_IMAGE_REQUEST_CODE) {
            carInsuranceCoverNoteFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, carInsuranceCoverNoteFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_CAR_GRANT_IMAGE_REQUEST_CODE) {
            carGrantFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, carGrantFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_CAR_PERMIT_IMAGE_REQUEST_CODE) {
            carPermitFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, carPermitFileUri);
        }

        startActivityForResult(intent, imageRequestCode);

    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(getActivity(),
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(getActivity(),
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_comfort_go");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_comfort_go" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }
        return mediaFile;
    }

    private void parseLanguage(String language, String id) {
        try {
            JSONArray languageArray = new JSONArray(language);
            int position = 0;
            for (int i = 0; i < languageArray.length(); i++) {
                JSONObject eachModel = languageArray.getJSONObject(i);
                LanguageModel languageModel = new LanguageModel();
                languageModel.setId(eachModel.getString("id"));
                languageModel.setLanguage(eachModel.getString("language"));
                languageLists.add(languageModel);
                if (languageModel.getId().equals(id)) {
                    position = i;
                }
            }

            ArrayAdapter<LanguageModel> spinnerAdapter = new ArrayAdapter<>(CompleteEditProfile.this, R.layout.sp_layout_textview, languageLists);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spLanguages.setAdapter(spinnerAdapter);
            spLanguages.setSelection(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseCity(String cities, String id) {
        try {
            JSONArray cityArray = new JSONArray(cities);
            int position = 0;
            for (int i = 0; i < cityArray.length(); i++) {
                JSONObject eachModel = cityArray.getJSONObject(i);
                CityModel cityModel = new CityModel();
                cityModel.setId(eachModel.getString("id"));
                cityModel.setCity(eachModel.getString("name"));
                cityLists.add(cityModel);

                if (cityModel.getId().equals(id)) {
                    position = i;
                }
            }

            ArrayAdapter<CityModel> spinnerAdapter = new ArrayAdapter<>(CompleteEditProfile.this, R.layout.sp_layout_textview, cityLists);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spCity.setAdapter(spinnerAdapter);
            spCity.setSelection(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseRace(String races, String id) {
        try {
            JSONArray raceArray = new JSONArray(races);
            int position = 0;
            for (int i = 0; i < raceArray.length(); i++) {
                JSONObject eachModel = raceArray.getJSONObject(i);
                RaceModel raceModel = new RaceModel();
                raceModel.setId(eachModel.getString("id"));
                raceModel.setRace(eachModel.getString("race"));
                raceLists.add(raceModel);
                if (isEdit) {
                    if (raceModel.getId().equals(id)) {
                        position = i;
                    }
                }
            }
            ArrayAdapter<RaceModel> spinnerAdapter = new ArrayAdapter<>(CompleteEditProfile.this, R.layout.sp_layout_textview, raceLists);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spRace.setAdapter(spinnerAdapter);
            spRace.setSelection(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseBank(String banks, String id) {

        try {
            JSONArray bankArray = new JSONArray(banks);
            int position = 0;
            for (int i = 0; i < bankArray.length(); i++) {
//                JSONObject eachModel = bankArray.getJSONObject(i);
                BankModel bankModel = new BankModel();
//                bankModel.setId(eachModel.getString("id"));
                //todo bank name aako mileko xaina uta bata static array pathairako xa
//                bankModel.setBank(eachModel.getString("bank"));

                bankModel.setBank(bankArray.getString(i));
                bankLists.add(bankModel);
//                if (cityModel.getId().equals(userDetails.getString("vehicle_type"))) {
//                    position = i;
//                }
            }
            ArrayAdapter<BankModel> spinnerAdapter = new ArrayAdapter<>(CompleteEditProfile.this, R.layout.sp_layout_textview, bankLists);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spBank.setAdapter(spinnerAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RESULT_LOAD_IMAGE_PROFILE:
            case RESULT_LOAD_IMAGE_NRIC:
            case RESULT_LOAD_IMAGE_LICENSE:
            case RESULT_LOAD_IMAGE_VOCATIONAL_LICENSE:
            case RESULT_LOAD_IMAGE_CAR_INSURANCE_COVER_NOTE:
            case RESULT_LOAD_IMAGE_CAR_PERMIT:
            case RESULT_LOAD_IMAGE_CAR_GRANT:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do
                    chooseFrmGallery(requestCode);
                }
                break;

            case CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE:
            case CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE:
            case CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE:
            case CAMERA_CAPTURE_VOCATIONAL_LICENSE_IMAGE_REQUEST_CODE:
            case CAMERA_CAPTURE_CAR_INSURANCE_COVER_NOTE_IMAGE_REQUEST_CODE:
            case CAMERA_CAPTURE_CAR_GRANT_IMAGE_REQUEST_CODE:
            case CAMERA_CAPTURE_CAR_PERMIT_IMAGE_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto(requestCode);
                }

        }
    }
}
