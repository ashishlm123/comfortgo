package com.emts.comfortgo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;
import com.emts.comfortgo.model.TripModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class TripDetailsActivity extends BookingBaseActivity {
    LinearLayout mainLayout;
    ProgressBar progressBar;
    TextView tvErrorText;

    TextView tvTripDate, tvBookingNo, tvTripStatus, tvPickUpLocation, tvDropOffLocation, tvTripDistance,
            tvTripDuration, tvTripFare, tvNetEarning;
    TextView lvlFareType, tvTotalFare, tvCommissionDeduction, tvBookingFee, tvSurcharge, tvCallCenterFee;
    View holderBookingFee, holderSurcharge, holderCallCenterFee;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd, HH:mm a");

    boolean isCancelledTrip;
    View holderEarningSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.trip_details));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        String bookingNo = intent.getStringExtra("bookingNo");
        isCancelledTrip = intent.getBooleanExtra("isCancelled", false);

        init();

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            tripDetailsTask(bookingNo);
        } else {
            mainLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void init() {
        mainLayout = findViewById(R.id.main_layout);
        progressBar = findViewById(R.id.progress_bar);
        tvErrorText = findViewById(R.id.tv_error_text);

        tvTripDate = findViewById(R.id.tv_trip_date_time);
        tvBookingNo = findViewById(R.id.tv_booking_no);
        tvTripStatus = findViewById(R.id.tv_trip_status);
        tvPickUpLocation = findViewById(R.id.tv_pickup_location);
        tvDropOffLocation = findViewById(R.id.tv_drop_off_location);
        tvTripDistance = findViewById(R.id.tv_trip_distance);
        tvTripDuration = findViewById(R.id.tv_trip_time);
        tvNetEarning = findViewById(R.id.tv_net_earning);

        lvlFareType = findViewById(R.id.lvlFareType);
        tvTripFare = findViewById(R.id.tv_trip_fare);

        holderEarningSummary = findViewById(R.id.holderEarningSummary);
        holderBookingFee = findViewById(R.id.holderBookingFee);
        tvBookingFee = findViewById(R.id.tv_booking_fee);

        holderSurcharge = findViewById(R.id.holderSurgeCharge);
        tvSurcharge = findViewById(R.id.tvTollSurcharge);

        holderCallCenterFee = findViewById(R.id.holderCallCenterFee);
        tvCallCenterFee = findViewById(R.id.tv_call_centre_fee);

        tvTotalFare = findViewById(R.id.tvTotalFare);
        tvCommissionDeduction = findViewById(R.id.tv_commission_deduction);
    }

    private void tripDetailsTask(String bookingNo) {
        progressBar.setVisibility(View.VISIBLE);
        mainLayout.setVisibility(View.GONE);

        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("booking_id", bookingNo);

        vHelper.addVolleyRequestListeners(Api.getInstance().tripDetailsApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject tripDetails = res.getJSONObject("trip_detail");

                                String tripDate = tripDetails.getString("datetime");
                                try {
                                    Date date = sdf.parse(tripDate);
                                    tvTripDate.setText(sdf1.format(date));
                                } catch (ParseException e) {
                                    tvTripDate.setText(tripDate);
                                }

                                tvBookingNo.setText(tripDetails.getString("booking_no"));

                                String tripBookingStatus = tripDetails.getString("booking_status");
                                if (isCancelledTrip) {
                                    tvTripStatus.setText(getString(R.string.cancelled));
                                    tvTripStatus.setTextColor(getResources().getColor(R.color.appOrange));
                                } else {
                                    String tripStatus = tripDetails.getString("trip_status");
                                    if (tripStatus.equals(TripModel.TRIP_PENDING)) {
                                        tvTripStatus.setText(getString(R.string.pending));
                                    } else if (tripStatus.equals(TripModel.TRIP_SCHEDULED)) {
                                        tvTripStatus.setText(getString(R.string.scheduled));
                                    } else if (tripStatus.equals(TripModel.TRIP_IN_PROGRESS)) {
                                        tvTripStatus.setText(getString(R.string.in_progress));
                                    } else if (tripStatus.equals(TripModel.TRIP_COMPLETED)) {
                                        tvTripStatus.setText(getString(R.string.completed));
                                    }
                                }

                                tvPickUpLocation.setText(tripDetails.getString("pickup_location"));
                                tvDropOffLocation.setText(tripDetails.getString("dropoff_location"));

                                Double distance = Double.parseDouble(tripDetails.getString("distance"));
                                tvTripDistance.setText(String.format("%.2f", distance) + " " + getString(R.string.km));

                                double timeInDouble = Double.parseDouble(tripDetails.getString("journey_time"));
                                int time = (int) timeInDouble;
                                if (time > 60) {
                                    int hour = time / 60;
                                    int minutes = time % 60;
                                    tvTripDuration.setText(hour + " " + getString(R.string.hour) + " " + minutes + " " + getString(R.string.minutes));
                                } else {
                                    tvTripDuration.setText(time + " " + getString(R.string.minutes));
                                }

                                if (isCancelledTrip) {
                                    holderEarningSummary.setVisibility(View.GONE);
                                    mainLayout.setVisibility(View.VISIBLE);
                                    return;
                                } else {
                                    holderEarningSummary.setVisibility(View.VISIBLE);
                                }

                                //fare, commissions and charges
                                double surCharge = Double.parseDouble(tripDetails.getString("surcharge"));
                                tvSurcharge.setText(getString(R.string.currency_myr) + " " + surCharge);
                                holderSurcharge.setVisibility(View.VISIBLE);
                                double commissionDeduct = Double.parseDouble(tripDetails.getString("comfort_commission"));
                                tvCommissionDeduction.setText(getString(R.string.currency_myr) + " " + commissionDeduct);
                                int carType = Integer.parseInt(tripDetails.getString("car_cat_id"));
                                if (carType == Booking.VEHICLE_TYPE_CFTAXI) {
                                    //its metered
                                    lvlFareType.setText(getString(R.string.metered_fare));
                                    double meteredCharge = Double.parseDouble(tripDetails.getString("received_trip_fare"));
                                    double minFare = Double.parseDouble(tripDetails.getString("fare_min"));
                                    if (meteredCharge <= 0) {
                                        meteredCharge = minFare;
                                    }
                                    tvTripFare.setText(getString(R.string.currency_myr) + " " + meteredCharge);
                                    double bookingFee = Double.parseDouble(tripDetails.getString("booking_fee"));
                                    tvBookingFee.setText(getString(R.string.currency_myr) + " " + bookingFee);
                                    holderBookingFee.setVisibility(View.VISIBLE);

                                    double totalFare = meteredCharge + bookingFee + surCharge;
                                    tvTotalFare.setText(getString(R.string.currency_myr) + " " + String.format("%.2f", totalFare));

                                    double callCenterFee = Double.parseDouble(tripDetails.getString("call_center_fee"));
                                    tvCallCenterFee.setText(getString(R.string.currency_myr) + " " + callCenterFee);
                                    holderCallCenterFee.setVisibility(View.VISIBLE);

                                    tvNetEarning.setText(getString(R.string.currency_myr) + " " + String.format("%.2f", (totalFare - commissionDeduct - callCenterFee)));
                                } else {
                                    // its fixed
                                    lvlFareType.setText(getString(R.string.fixed_fare));
                                    double fixedFare = Double.parseDouble(tripDetails.getString("fare_min"));
                                    tvTripFare.setText(getString(R.string.currency_myr) + " " + fixedFare);
                                    double totalFare = fixedFare + surCharge;
                                    tvTotalFare.setText(getString(R.string.currency_myr) + " " + totalFare);
                                    tvNetEarning.setText(getString(R.string.currency_myr) + " " + String.format("%.2f", (totalFare - commissionDeduct)));

                                    holderBookingFee.setVisibility(View.GONE);
                                    holderCallCenterFee.setVisibility(View.GONE);
                                }

                                mainLayout.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                mainLayout.setVisibility(View.GONE);
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("tripDetailsTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("tripDetailsTask error ex", e.getMessage() + " ");
                        }
                        mainLayout.setVisibility(View.GONE);

                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                }, "tripDetailsTask");
    }
}
