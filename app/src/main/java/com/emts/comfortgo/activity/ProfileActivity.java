package com.emts.comfortgo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class ProfileActivity extends BookingBaseActivity {
    ImageView ivProfilePic, ivNRIC, ivDrivingLicense, ivVocationalLicense, ivCarInsuranceCoverNote, ivCarGrant, ivCarPermit;
    TextView tvDriverID, tvFullName, tvICNumber, tvRace, tvLanguage, tvCity, tvAddress, tvMobileNo,
            tvEmail, tvClass, tvLicenseValidity, tvVehiclePlateNo, tvVehicleModel, tvManufactureYear,
            tvJoinedDate, tvBankName, tvBankAcNo, tvRecipientName;
    TextView tvErrorText;
    ProgressBar progressBar;
    ScrollView mainLayout;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");


    PreferenceHelper prefsHelper;
    JSONObject res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView toolbarTitle = findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.drivers_profile));

        toolbar.inflateMenu(R.menu.edit_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.edt_menu) {
                    Intent intent = new Intent(getApplicationContext(), CompleteEditProfile.class);
                    intent.putExtra("isEdit", true);
                    intent.putExtra("response", res.toString());
                    startActivity(intent);
                    ProfileActivity.this.finish();
                }
                return false;
            }
        });

        mainLayout = findViewById(R.id.scrollview);
        progressBar = findViewById(R.id.progress_bar);
        tvErrorText = findViewById(R.id.tv_error_text);

        init();

        if (NetworkUtils.isInNetwork(getApplication())) {
            getProfileDetailsTask();
        } else {
            mainLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void init() {
        ivProfilePic = findViewById(R.id.iv_profile_pic);
        ivNRIC = findViewById(R.id.nric_photo);
        ivDrivingLicense = findViewById(R.id.license_photo);
        ivVocationalLicense = findViewById(R.id.vocational_license_photo);
        ivCarInsuranceCoverNote = findViewById(R.id.car_insurance_cover_note_photo);
        ivCarGrant = findViewById(R.id.car_grant_photo);
        ivCarPermit = findViewById(R.id.car_permit_photo);

        tvDriverID = findViewById(R.id.tv_driver_id);
        tvFullName = findViewById(R.id.tv_full_name);
        tvICNumber = findViewById(R.id.tv_ic_number);

        tvRace = findViewById(R.id.tv_race);
        tvLanguage = findViewById(R.id.tv_language);
        tvCity = findViewById(R.id.tv_city);
        tvAddress = findViewById(R.id.tv_address);
        tvMobileNo = findViewById(R.id.tv_mobile_no);
        tvEmail = findViewById(R.id.tv_email_address);
        tvClass = findViewById(R.id.tv_class);
        tvLicenseValidity = findViewById(R.id.tv_license_validity);
        tvVehiclePlateNo = findViewById(R.id.tv_vehicle_plate_no);

        tvVehicleModel = findViewById(R.id.tv_vehicle_model);
        tvManufactureYear = findViewById(R.id.tv_registration_year);
        tvJoinedDate = findViewById(R.id.tv_joined_date);

        tvBankName = findViewById(R.id.tv_bank_name);
        tvBankAcNo = findViewById(R.id.tv_bank_account_no);
        tvRecipientName = findViewById(R.id.tv_bank_account_name);
    }

    private void getProfileDetailsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        mainLayout.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().viewProfileApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        mainLayout.setVisibility(View.VISIBLE);
                        tvErrorText.setVisibility(View.GONE);

                        JSONArray languageArray, cityArray, raceArray, bankArray;
                        languageArray = res.getJSONArray("language");
                        cityArray = res.getJSONArray("city");
                        raceArray = res.getJSONArray("race");
//                        bankArray = res.getJSONArray("banks");

                        JSONObject driverDetails = res.getJSONObject("driver_info");

                        String profileDirectory = driverDetails.getString("profile_img_directory");
                        String profilePic = driverDetails.getString("profile_photo");

                        if (!TextUtils.isEmpty(profilePic) && !profilePic.equalsIgnoreCase("null")) {
                            prefsHelper.edit().putString(PreferenceHelper.USER_PROFILE_PIC, profileDirectory + profilePic).apply();
                        }
                        Glide.with(ProfileActivity.this).load(prefsHelper.getAppUserImg()).into(ivProfilePic);

                        prefsHelper.edit().putString(PreferenceHelper.DRIVER_ID, driverDetails.getString("user_name")).apply();
                        tvDriverID.setText(driverDetails.getString("user_name"));

                        prefsHelper.edit().putString(PreferenceHelper.USER_FULL_NAME, driverDetails.getString("full_name")).apply();
                        tvFullName.setText(driverDetails.getString("full_name"));

                        prefsHelper.edit().putString(PreferenceHelper.USER_CREDIT_BALANCE, driverDetails.getString("credit_balance")).apply();

                        tvICNumber.setText(driverDetails.getString("lc_no"));
                        tvRace.setText(getRace(raceArray, driverDetails.getString("race")));
                        tvCity.setText(getCity(cityArray, driverDetails.getString("city")));
                        tvLanguage.setText(getLanguage(languageArray, driverDetails.getString("languages")));
                        tvAddress.setText(driverDetails.getString("home_address"));
                        tvMobileNo.setText(driverDetails.getString("mobile_no"));
                        tvEmail.setText(driverDetails.getString("email"));

                        tvClass.setText(driverDetails.getString("class"));

                        String licenseStartDate = driverDetails.getString("lc_valid_from");
                        String licenseEndDate = driverDetails.getString("lc_valid_to");
                        String joinDate = driverDetails.getString("joined_date");

                        try {
                            Date startDate = sdf.parse(licenseStartDate);
                            Date endDate = sdf.parse(licenseEndDate);
                            Date joinedDate = sdf.parse(joinDate);

                            tvLicenseValidity.setText(sdf1.format(startDate) + "-" + sdf1.format(endDate));
                            tvJoinedDate.setText(sdf1.format(joinedDate));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        tvVehiclePlateNo.setText(driverDetails.getString("plate_no"));
                        tvVehicleModel.setText(driverDetails.getString("vehicle_model"));
                        tvManufactureYear.setText(driverDetails.getString("year_of_mfg"));
                        tvBankName.setText(driverDetails.getString("bank_name"));
                        tvBankAcNo.setText(driverDetails.getString("ac_no"));
                        tvRecipientName.setText(driverDetails.getString("recipient_name"));

                        String nricPhoto = driverDetails.getString("nric_photo");
                        String nricDirectory = driverDetails.getString("nric_img_directory");
                        if (!TextUtils.isEmpty(nricPhoto) && !nricPhoto.equalsIgnoreCase("null")) {
                            Glide.with(ProfileActivity.this).load(nricDirectory + nricPhoto).into(ivNRIC);
                        }

                        String licensePhoto = driverDetails.getString("lc_photo");
                        String licenseDirectory = driverDetails.getString("driving_license_img_directory");
                        if (!TextUtils.isEmpty(licensePhoto) && !licensePhoto.equalsIgnoreCase("null")) {
                            Glide.with(ProfileActivity.this).load(licenseDirectory + licensePhoto).into(ivDrivingLicense);
                        }

                        String vocationalLicensePhoto = driverDetails.getString("vocational_lc");
                        String vocationalDirectory = driverDetails.getString("vocational_img_directory");
                        if (!TextUtils.isEmpty(vocationalLicensePhoto) && !vocationalLicensePhoto.equalsIgnoreCase("null")) {
                            Glide.with(ProfileActivity.this).load(vocationalDirectory + vocationalLicensePhoto).into(ivVocationalLicense);
                        }

                        String carInsuranceCoverNotePhoto = driverDetails.getString("insurance_photo");
                        String carInsuranceDirectory = driverDetails.getString("vehicle_insurance_img_directory");
                        if (!TextUtils.isEmpty(carInsuranceCoverNotePhoto) && !carInsuranceCoverNotePhoto.equalsIgnoreCase("null")) {
                            Glide.with(ProfileActivity.this).load(carInsuranceDirectory + carInsuranceCoverNotePhoto).into(ivCarInsuranceCoverNote);
                        }

                        String carGrantPhoto = driverDetails.getString("car_grant");
                        String carGrantDirectory = driverDetails.getString("car_grant_img_directory");
                        if (!TextUtils.isEmpty(carGrantPhoto) && !carGrantPhoto.equalsIgnoreCase("null")) {
                            Glide.with(ProfileActivity.this).load(carGrantDirectory + carGrantPhoto).into(ivCarGrant);
                        }

                        String carPermitPhoto = driverDetails.getString("tax_permit");
                        String carPermitDirectory = driverDetails.getString("tax_permit_img_directory");
                        if (!TextUtils.isEmpty(carPermitPhoto) && !carPermitPhoto.equalsIgnoreCase("null")) {
                            Glide.with(ProfileActivity.this).load(carPermitDirectory + carPermitPhoto).into(ivCarPermit);
                        }
                    } else {
                        mainLayout.setVisibility(View.GONE);
                        tvErrorText.setText(res.getString("message"));
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("getProfileDetailsTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                mainLayout.setVisibility(View.GONE);
                String errorMsg = getString(R.string.unexpected_error_try_again);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("getProfileDetailsTask error ex", e.getMessage() + " ");
                }
                tvErrorText.setText(errorMsg);
                tvErrorText.setVisibility(View.VISIBLE);
            }
        }, "getProfileDetailsTask");
    }

    private String getRace(JSONArray raceArray, String id) {
        try {
            for (int i = 0; i < raceArray.length(); i++) {
                JSONObject eachModel = raceArray.getJSONObject(i);
                if (id.equals(eachModel.getString("id"))) {
                    return eachModel.getString("race");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getCity(JSONArray cityArray, String id) {
        try {
            for (int i = 0; i < cityArray.length(); i++) {
                JSONObject eachModel = cityArray.getJSONObject(i);
                if (id.equals(eachModel.getString("id"))) {
                    return eachModel.getString("name");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getLanguage(JSONArray languageArray, String id) {
        try {
            for (int i = 0; i < languageArray.length(); i++) {
                JSONObject eachModel = languageArray.getJSONObject(i);
                if (id.equals(eachModel.getString("id"))) {
                    return eachModel.getString("language");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }


}
