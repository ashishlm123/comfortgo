package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RejectBookingActivity extends BookingBaseActivity {
    boolean onGoingRequest;
    Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject_booking);

        ImageView closeIcon = findViewById(R.id.close);
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
            return;
        }
        final Booking booking = (Booking) intent.getSerializableExtra("booking");
        if (booking == null) {
            onBackPressed();
            return;
        }

        final EditText etRejectReason = findViewById(R.id.et_reason);
        final Button btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etRejectReason.getText().toString().trim())) {
                    AlertUtils.showSnack(RejectBookingActivity.this, btnSubmit, getString(R.string.enter_reason_first));
                } else {
//                    rejectJobTask(job.getJobId(), etRejectReason.getText().toString().trim());
                }


            }
        });

        //start timer: on 30 second go back
        mHandler = new Handler();
        mHandler.postDelayed(runnable, 34000);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!onGoingRequest) {
                onBackPressed();
            } else {
                onGoingRequest = false;
                //give him a chance
                mHandler.postDelayed(runnable, 30000);
            }
        }
    };

    @Override
    public void onBackPressed() {
        try {
            VolleyHelper.getInstance(this).cancelRequest("rejectJobTask");
            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
            Logger.e("rejectJobTask onBackPressed Ex", e.getMessage());
        }
        super.onBackPressed();
    }

    private void rejectJobTask(String jobId, String reason) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(RejectBookingActivity.this, getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(RejectBookingActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();


        vHelper.addVolleyRequestListeners("", Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                onGoingRequest = false;
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                    } else {
                        pDialog.dismiss();
                        AlertUtils.simpleAlert(RejectBookingActivity.this, getString(R.string.error),
                                resObj.getString("message"), getString(R.string.ok), "", null);
                    }
                } catch (JSONException e) {
                    Logger.e("rejectJobTask res ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                onGoingRequest = false;
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    AlertUtils.simpleAlert(RejectBookingActivity.this, getString(R.string.error), errorObj.getString("message"),
                            getString(R.string.ok), "", null);
                } catch (JSONException e) {
                    Logger.e("rejectJobTask error ex", e.getMessage() + " ");
                }
            }
        }, "rejectJobTask");
        onGoingRequest = true;
    }

}
