package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.MyInput1;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 7/6/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    MyInput1 etEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(R.string.title_forgot_pass);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etEmail = findViewById(R.id.et_forget_email);

        Button sendPassword = findViewById(R.id.btn_send_pass);
        sendPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    if (!TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                        if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                            forgetPasswordTask(etEmail.getText().toString().trim());
                        } else {
                            AlertUtils.showSnack(ForgotPasswordActivity.this, view, getString(R.string.error_valid_email));
                        }
                    } else {
                        AlertUtils.showSnack(ForgotPasswordActivity.this, view, getString(R.string.enter_email_to_retrive_password));
                    }
                } else {
                    AlertUtils.showSnack(ForgotPasswordActivity.this, view, getString(R.string.no_internet));
                }
            }
        });

    }

    private void forgetPasswordTask(String email) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("email", email);

        //url left
        vHelper.addVolleyRequestListeners(Api.getInstance().forgotPasswordApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                etEmail.setText("");
                                AlertUtils.simpleAlert(ForgotPasswordActivity.this, getString(R.string.success),
                                        res.getString("message"), getString(R.string.ok), "", null);
                            } else {
                                AlertUtils.simpleAlert(ForgotPasswordActivity.this, getString(R.string.error),
                                        res.getString("message"), getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(ForgotPasswordActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                    }
                }, "forgetPasswordTask");
    }
}
