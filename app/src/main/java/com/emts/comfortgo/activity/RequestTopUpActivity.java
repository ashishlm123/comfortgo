package com.emts.comfortgo.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.ImageHelper;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.Utils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.CreditsModel;
import com.emts.comfortgo.model.TransactionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class RequestTopUpActivity extends BookingBaseActivity {
    PreferenceHelper prefsHelper;
    TextView tvFullName, tvContactNo;
    EditText etTransactionDate, etTransactionID;
    RadioButton rbOnlineBanking;
    ImageView btnAddReceiptImg;
    String receiptImagePath = "";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static Uri receiptFileUri;

    Spinner spAmt;
    ArrayList<CreditsModel> creditLists;

    public static final int CAMERA_CAPTURE_RECEIPT_IMAGE_REQUEST_CODE = 14321;
    public static final int RESULT_LOAD_IMAGE_RECEIPT = 14322;

    boolean isTransHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_request_top_up);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.top_up_credit));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        prefsHelper = PreferenceHelper.getInstance(this);

        tvFullName = findViewById(R.id.tv_full_name);
        tvFullName.setText(prefsHelper.getAppUserFullName());
        tvContactNo = findViewById(R.id.tv_contact_no);
        tvContactNo.setText(prefsHelper.getAppUserMobileNo());
        etTransactionDate = findViewById(R.id.et_transaction_date);
        etTransactionID = findViewById(R.id.et_transaction_id);
        rbOnlineBanking = findViewById(R.id.rb_online_banking);
        btnAddReceiptImg = findViewById(R.id.btn_add_receipt_img);
        rbOnlineBanking.setChecked(true);

        creditLists = new ArrayList<>();
        spAmt = findViewById(R.id.sp_amt);
        Button btnRequestTopUp = findViewById(R.id.btn_submit);

        Intent intent = getIntent();

        isTransHistory = intent != null && intent.getBooleanExtra("isHistory", false);

        if (!isTransHistory) {
            Calendar calendar = Calendar.getInstance();
            etTransactionDate.setText(calendar.get(Calendar.YEAR) + "-" + String.format("%02d", (calendar.get(Calendar.MONTH) + 1))
                    + "-" + String.format("%02d", (calendar.get(Calendar.DAY_OF_MONTH))));

            String credits = intent.getStringExtra("credits");

            try {
                JSONArray creditsArray = new JSONArray(credits);
                if (creditsArray.length() > 0) {
                    for (int i = 0; i < creditsArray.length(); i++) {
                        JSONObject eachObject = creditsArray.getJSONObject(i);
                        CreditsModel creditsModel = new CreditsModel();
                        creditsModel.setId(eachObject.getString("id"));
                        creditsModel.setCreditPrice(eachObject.getString("amount"));
                        creditsModel.setCreditQty(eachObject.getString("credits"));
                        creditsModel.setPackageName(eachObject.getString("package_name"));
                        creditLists.add(creditsModel);
                    }
                    ArrayAdapter<CreditsModel> spinnerAdapter = new ArrayAdapter<>(RequestTopUpActivity.this, R.layout.sp_layout_textview, creditLists);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spAmt.setAdapter(spinnerAdapter);
                }
            } catch (JSONException e) {
                Logger.e("creditsArray JSON ex", e.getMessage());
            }

            btnAddReceiptImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showImageChooseDialog(view.getId());
                }
            });


            btnRequestTopUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validation()) {
                        if (NetworkUtils.isInNetwork(getApplicationContext())) {
                            requestTopUpCreditTask();
                        } else {
                            AlertUtils.showSnack(RequestTopUpActivity.this, btnAddReceiptImg, getResources().getString(R.string.no_internet));
                        }
                    }
                }
            });
        } else {
            TransactionModel transactionModel = (TransactionModel) intent.getSerializableExtra("trans");

            etTransactionDate.setEnabled(false);
            etTransactionDate.setText(transactionModel.getTxnDate());

            ArrayList<CreditsModel> amtList = new ArrayList<>();
            CreditsModel creditsModel = new CreditsModel();
            creditsModel.setPackageName(transactionModel.getCreditPackageName());
            amtList.add(creditsModel);
            ArrayAdapter<CreditsModel> spinnerAdapter = new ArrayAdapter<>(RequestTopUpActivity.this,
                    R.layout.sp_layout_textview, amtList);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spAmt.setAdapter(spinnerAdapter);
            spAmt.setEnabled(false);

            etTransactionID.setText(transactionModel.getId());
            etTransactionID.setEnabled(false);
            rbOnlineBanking.setEnabled(false);

            Glide.with(RequestTopUpActivity.this).load(transactionModel.getReceiptImage()).into(btnAddReceiptImg);
            btnAddReceiptImg.setEnabled(false);
        }


    }

    private void requestTopUpCreditTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(RequestTopUpActivity.this, getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("package_id", creditLists.get(spAmt.getSelectedItemPosition()).getId());
        postParams.put("txn_date", etTransactionDate.getText().toString().trim());
        postParams.put("payment_method", "bank");
        postParams.put("ref_no", etTransactionID.getText().toString().trim());

        HashMap<String, String> fileParams = new HashMap<>();
        if (!TextUtils.isEmpty(receiptImagePath) && !receiptImagePath.contains("http")) {
            fileParams.put("receipt_image", receiptImagePath);
        }

        vHelper.addMultipartRequest(Api.getInstance().requestTopupApi, Request.Method.POST, postParams,
                fileParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.simpleAlert(RequestTopUpActivity.this, getString(R.string.success), res.getString("message"),
                                        getString(R.string.ok), "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                onBackPressed();
                                            }
                                        });
                            } else {
                                AlertUtils.simpleAlert(RequestTopUpActivity.this, getString(R.string.error), res.getString("message"),
                                        getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("requestTopUpCreditTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("requestTopUpCreditTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(RequestTopUpActivity.this, getString(R.string.error), errorMsg);
                    }
                }, "requestTopUpCreditTask");

    }

    private boolean validation() {
        if (TextUtils.isEmpty(etTransactionID.getText().toString().trim())) {
            AlertUtils.showSnack(RequestTopUpActivity.this, btnAddReceiptImg, getString(R.string.insert_transaction_id));
            return false;
        }
        if (TextUtils.isEmpty(etTransactionDate.getText().toString().trim())) {
            AlertUtils.showSnack(RequestTopUpActivity.this, btnAddReceiptImg, getString(R.string.insert_transaction_date));
            return false;
        }

        if (TextUtils.isEmpty(receiptImagePath)) {
            AlertUtils.showSnack(RequestTopUpActivity.this, btnAddReceiptImg, getString(R.string.insert_receipt_image));
            return false;
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE_RECEIPT) {

                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                assert selectedImageUri != null;
                CursorLoader cursorLoader = new CursorLoader(RequestTopUpActivity.this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);

                Bitmap bm = ImageHelper.decodeImageFileWithCompression(selImgPath,
                        (int) Utils.pxFromDp(getApplicationContext(), 85f), (int) Utils.pxFromDp(getApplicationContext(), 85f));

                btnAddReceiptImg.setImageBitmap(bm);
                receiptImagePath = selImgPath;
                ImageHelper.saveBitMapToFile(receiptImagePath, bm);
            } else if (requestCode == CAMERA_CAPTURE_RECEIPT_IMAGE_REQUEST_CODE) {
                new ImageTask().execute();
            }
        }
    }

    class ImageTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog = AlertUtils.showProgressDialog(RequestTopUpActivity.this, getString(R.string.please_wait));

        @Override
        protected Void doInBackground(Void... voids) {
            ImageHelper.saveBitMapToFile(receiptFileUri.getPath(), ImageHelper.bitmapFromFile(receiptFileUri.getPath()));
            receiptImagePath = receiptFileUri.getPath();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                Glide.with(RequestTopUpActivity.this).load(receiptFileUri).into(btnAddReceiptImg);
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    private void showImageChooseDialog(final int imagViewID) {
        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(RequestTopUpActivity.this);
        builder.setTitle(R.string.choose_media);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getString(R.string.take_photo))) {
                    int imageRequestCode = 0;
                    if (imagViewID == R.id.btn_add_receipt_img) {
                        imageRequestCode = CAMERA_CAPTURE_RECEIPT_IMAGE_REQUEST_CODE;
                    }
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(RequestTopUpActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                imageRequestCode);

                    } else {
                        takePhoto(imageRequestCode);
                    }
                } else if (options[item].equals(getString(R.string.choose_photo))) {
                    int imageRequestCode = 0;
                    if (imagViewID == R.id.btn_add_receipt_img) {
                        imageRequestCode = RESULT_LOAD_IMAGE_RECEIPT;
                    }
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(RequestTopUpActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                imageRequestCode);

                    } else {
                        chooseFrmGallery(imageRequestCode);
                    }
                } else if (options[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(int requestImageCode) {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, requestImageCode);
    }

    public void takePhoto(int imageRequestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        // start the image capture Intent
        if (imageRequestCode == CAMERA_CAPTURE_RECEIPT_IMAGE_REQUEST_CODE) {
            receiptFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, receiptFileUri);
        }
        startActivityForResult(intent, imageRequestCode);

    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(getActivity(),
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(getActivity(),
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }

    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_comfort_go");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_comfort_go" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }
        return mediaFile;
    }


}
