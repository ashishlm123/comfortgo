package com.emts.comfortgo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.adapter.CashOutHistoryAdapter;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.CashOutHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CashOutHistoryActivity extends BookingBaseActivity {

    PreferenceHelper prefsHelper;
    ArrayList<CashOutHistoryModel> cashOutHistoryLists;
    CashOutHistoryAdapter cashOutHistoryAdapter;
    RecyclerView rvCashOutHistoryListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cash_out_history);

        prefsHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.cash_out_history));

        progressBar = findViewById(R.id.progress_bar);
        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
        tvErrorText = findViewById(R.id.error_text);

        rvCashOutHistoryListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvCashOutHistoryListings.setLayoutManager(linearLayoutManager);

        cashOutHistoryLists = new ArrayList<>();
        cashOutHistoryAdapter = new CashOutHistoryAdapter(CashOutHistoryActivity.this, cashOutHistoryLists);
        rvCashOutHistoryListings.setAdapter(cashOutHistoryAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            cashOutHistoryListingTask();
        } else {
            rvCashOutHistoryListings.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            tvErrorText.setText(R.string.no_internet);
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void cashOutHistoryListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvCashOutHistoryListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().cashOutHistory, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                                JSONArray historyArray = res.getJSONArray("cashout_history");
                                for (int i = 0; i<historyArray.length(); i++){
                                    JSONObject eachHistory = historyArray.getJSONObject(i);
                                    CashOutHistoryModel cashOutHistoryModel = new CashOutHistoryModel();
                                    cashOutHistoryModel.setCashOutAmt(eachHistory.getString("amount"));
                                    cashOutHistoryModel.setCashOutDate(eachHistory.getString("transaction_date"));
                                    cashOutHistoryModel.setCashOutRequestId(eachHistory.getString("invoice_id"));
                                    cashOutHistoryModel.setCashOutStatus(eachHistory.getString("status"));

                                    cashOutHistoryLists.add(cashOutHistoryModel);
                                }

                                cashOutHistoryAdapter.notifyDataSetChanged();
                                rvCashOutHistoryListings.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                                rvCashOutHistoryListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("cashOutHistoryListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("cashOutHistoryListingTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(CashOutHistoryActivity.this, getString(R.string.error), errorMsg);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        rvCashOutHistoryListings.setVisibility(View.GONE);
                    }
                }, "cashOutHistoryListingTask");
    }

}
