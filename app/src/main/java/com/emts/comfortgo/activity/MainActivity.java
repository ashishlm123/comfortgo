package com.emts.comfortgo.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.GetBalanceCallback;
import com.emts.comfortgo.R;
import com.emts.comfortgo.fragment.ContactUsFrag;
import com.emts.comfortgo.fragment.MyWalletFrag;
import com.emts.comfortgo.fragment.NotificationFrag;
import com.emts.comfortgo.fragment.SettingsFrag;
import com.emts.comfortgo.fragment.TripFragment;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;
import com.emts.comfortgo.periodicupdate.LocUpdateScheduler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends BookingBaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    PreferenceHelper prefsHelper;
    FragmentManager fm;
    DrawerLayout drawer;
    TextView toolbarTitle, tvCreditBalance, tvDriverEmail, tvDriverName, tvDriverType, tvVehicleType, tvPlateNo;
    ImageView profilePic;
    ImageView ivTitle;
    View layTypeHolder;

    ToggleButton statusSwitch;
    View views;

    MenuItem notiMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        prefsHelper = PreferenceHelper.getInstance(this);

        if (NetworkUtils.isInNetwork(getApplicationContext()) && prefsHelper.isLogin()) {
            GetBalanceCallback.getMyBalanceTask(this, new GetBalanceCallback.BalanceSuccessCallback() {
                @Override
                public void onBalance(boolean isSuccess, String msg) {
                    if (isSuccess) {
                        updateUserDetail();

                        double balance = Double.parseDouble(prefsHelper.getString(PreferenceHelper.USER_CREDIT_BALANCE, "00"));
                        if (balance < 10) {
                            if (!prefsHelper.getString(PreferenceHelper.FIRST_TIME_BALANCE_ALERT, "")
                                    .equals(new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date()))) {
                                //show low balance alert
                                // however, Cftaxi Diamond driver do not require credit.
                                if (!msg.equals(GetBalanceCallback.DIAMOND_DRIVER)) {
                                    showLowBalanceAlert();
                                }

                                prefsHelper.edit().putString(PreferenceHelper.FIRST_TIME_BALANCE_ALERT,
                                        new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date())).commit();
                            }
                        }
                    }
                }
            });
        }

        checkVersion();
        //loc update scheduler
        LocUpdateScheduler.schedule(this, true);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.custom_toolbar_title);
        ivTitle = findViewById(R.id.iv_title);

        ImageView navIcon = toolbar.findViewById(R.id.iv_nav_icon);
        navIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(Gravity.START))
                    drawer.closeDrawer(Gravity.START);
                else
                    drawer.openDrawer(Gravity.START);
            }
        });
        statusSwitch = findViewById(R.id.sw_switch);
        statusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (!b) {
                    AlertUtils.simpleAlert(MainActivity.this, getString(R.string.update_online_status),
                            getString(R.string.will_not_receive_booking_if_offline),
                            getString(R.string.ok), getString(R.string.cancel), new AlertUtils.OnAlertButtonClickListener() {
                                @Override
                                public void onAlertButtonClick(boolean isPositiveButton) {
                                    if (isPositiveButton) {
                                        if (NetworkUtils.isInNetwork(getApplicationContext()))
                                            setDriverStatusTask(false);
                                        else
                                            AlertUtils.showSnack(MainActivity.this, toolbar, getResources().getString(R.string.no_internet));
                                    } else {
                                        statusSwitch.setChecked(true);
                                    }
                                }
                            });
                } else {
                    if (NetworkUtils.isInNetwork(getApplicationContext()))
                        setDriverStatusTask(true);
                    else
                        AlertUtils.showSnack(MainActivity.this, toolbar, getResources().getString(R.string.no_internet));
                }
            }

        });

        drawer = findViewById(R.id.drawer_layout);
        fm = getSupportFragmentManager();
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        views = navigationView.getHeaderView(0);
        setHeaderData();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        Intent intent = getIntent();
        //In case of booking push there will be booking on intent
        Booking booking = (Booking) intent.getSerializableExtra("booking");
        if (booking != null) {
            //case from notification click....booking notification while app in bg
            Logger.e("Booking Alert", "111. From booking notification, contains booking extra: showing alert....");
            showBookingAlert(this, booking);
            //from recent showing alert
            intent.putExtra("booking", (Serializable) null);
        }
        if (intent.getBooleanExtra("helpCenter", false)) {
            openContactUsFragment();
            return;
        }
        if (intent.getBooleanExtra("adminAlert", false)) {
            openNotificationFragment();
            return;
        }
        fm.beginTransaction().replace(R.id.content_frame, new TripFragment(), "TripFragment").commit();

        navigationView.post(new Runnable() {
            @Override
            public void run() {
                Menu menu = navigationView.getMenu();
                notiMenuItem = menu.findItem(R.id.nav_notification);
                for (int i = 0; i < menu.size(); i++) {
                    if (menu.getItem(i).getItemId() != R.id.nav_notification) {
                        menu.getItem(i).getIcon().setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
                    }
                }

                if (NetworkUtils.isInNetwork(MainActivity.this)) {
                    getNotificationCountTask();
                }
            }
        });
//        BookingParser.generateNotification(this);
    }

    private void showLowBalanceAlert() {
        AlertUtils.simpleAlert(MainActivity.this, getString(R.string.balance_alert),
                getString(R.string.credit_low), getString(R.string.ok), "",
                null);
    }

    public void openNotificationFragment() {
        fm.beginTransaction().replace(R.id.content_frame, new NotificationFrag(), "Notification").commit();
        setToolbarTitle("");
    }

    public void openContactUsFragment() {
        fm.beginTransaction().replace(R.id.content_frame, new ContactUsFrag(), "Contact Us").commit();
        setToolbarTitle("");
    }

    @Override
    public void onBackPressed() {
        try {
            getIntent().putExtra("booking", (Serializable) null);
        } catch (Exception e) {
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            setToolbarTitle("");
        } else {
            VolleyHelper.getInstance(this).cancelAllRequests();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        recycle();
        super.onDestroy();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();

        updateUserDetail();
    }

    private void updateUserDetail() {
        tvCreditBalance.setText(getString(R.string.currency_myr) + " " + prefsHelper.getUserCreditBalance());
        tvDriverName.setText(prefsHelper.getAppUserFullName());
        tvDriverEmail.setText(prefsHelper.getDriverEmail());
        Glide.with(MainActivity.this)
                .load(prefsHelper.getAppUserImg())
                .apply(RequestOptions.circleCropTransform())
                .into(profilePic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });

        tvDriverType.setText(prefsHelper.getDriverType());
        if (TextUtils.isEmpty(prefsHelper.getDriverType()) && TextUtils.isEmpty(prefsHelper.getVehicleType())) {
            layTypeHolder.setVisibility(View.GONE);
        } else {
            layTypeHolder.setVisibility(View.VISIBLE);
            tvDriverType.setText(prefsHelper.getDriverType());
            if (!TextUtils.isEmpty(prefsHelper.getVehicleType())) {
                if (!TextUtils.isEmpty(prefsHelper.getDriverType())) {
                    tvVehicleType.setText(", " + prefsHelper.getVehicleType());
                } else {
                    tvVehicleType.setText(prefsHelper.getVehicleType());
                }
            }
        }

        if (!TextUtils.isEmpty(prefsHelper.getUserVehiclePlateNo())
                && !prefsHelper.getUserVehiclePlateNo().equalsIgnoreCase("null")) {
            tvPlateNo.setText(getString(R.string.plateno) + ": " + prefsHelper.getUserVehiclePlateNo());
        }
        statusSwitch.setChecked(!prefsHelper.getBoolean(PreferenceHelper.OFFLINE, false));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = fm.findFragmentById(R.id.content_frame);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent = null;
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.nav_booking_trip:
                fragment = new TripFragment();
                break;

            case R.id.nav_profile:
                intent = new Intent(getApplicationContext(), EnterProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_trip_history:
                intent = new Intent(getApplicationContext(), TripHistoryActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_credit_wallet:
                fragment = new MyWalletFrag();
                break;
            case R.id.nav_settings:
                fragment = new SettingsFrag();
                break;
            case R.id.nav_notification:
                fragment = new NotificationFrag();
                break;
            case R.id.nav_contact_us:
                fragment = new ContactUsFrag();
                break;
            case R.id.nav_logout:
                if (NetworkUtils.isInNetwork(MainActivity.this)) {
                    logoutTask();
                    PreferenceHelper.getInstance(MainActivity.this).edit().clear().apply();
                    Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);
                } else {
                    AlertUtils.showSnack(MainActivity.this, toolbar, getString(R.string.no_internet));
                }
                break;
        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            drawer.closeDrawer(GravityCompat.START);
            setToolbarTitle("");
        }
        return false;
    }

    private void logoutTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().logoutApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                            } else {
                            }
                        } catch (JSONException e) {
                            Logger.e("logoutTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("logoutTask error ex", e.getMessage() + " ");
                        }
                        Logger.e("logoutTask error msg", errorMsg);
                    }
                }, "logoutTask");
    }

    public void setToolbarTitle(final String title) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                ivTitle.setVisibility(View.GONE);
                toolbarTitle.setVisibility(View.VISIBLE);
                if (fragment instanceof MyWalletFrag) {
                    toolbarTitle.setText(getString(R.string.my_wallet));
                } else if (fragment instanceof NotificationFrag) {
                    toolbarTitle.setText(getString(R.string.notification));
                } else if (fragment instanceof TripFragment) {
                    if (TextUtils.isEmpty(title)) {
                        toolbarTitle.setVisibility(View.GONE);
                        ivTitle.setVisibility(View.VISIBLE);
                    } else {
                        toolbarTitle.setText(title);
                    }
                } else if (fragment instanceof ContactUsFrag) {
                    toolbarTitle.setText(getString(R.string.contact_us));
                } else if (fragment instanceof SettingsFrag) {
                    toolbarTitle.setText(getString(R.string.settings));
                }
            }

        }, 200);
    }

    @SuppressLint("SetTextI18n")
    private void setHeaderData() {
        RelativeLayout layAppHeader = views.findViewById(R.id.lay_header);
        profilePic = layAppHeader.findViewById(R.id.iv_driver_pic);

        Logger.e("profile pic", prefsHelper.getAppUserImg());
        Glide.with(MainActivity.this)
                .load(prefsHelper.getAppUserImg())
                .apply(RequestOptions.circleCropTransform())
                .into(profilePic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });

        tvDriverName = layAppHeader.findViewById(R.id.tv_driver_name);
        tvDriverName.setText(prefsHelper.getAppUserFullName());
        tvDriverName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });

        layTypeHolder = layAppHeader.findViewById(R.id.lay_type_holder);
        tvDriverEmail = layAppHeader.findViewById(R.id.tv_driver_email);
        tvDriverEmail.setText(prefsHelper.getDriverEmail());

        tvCreditBalance = layAppHeader.findViewById(R.id.tv_credit_balance);
        tvCreditBalance.setText(getString(R.string.currency_myr) + " " + prefsHelper.getUserCreditBalance());

        tvDriverType = layAppHeader.findViewById(R.id.tv_driver_type);
        tvDriverType.setText(prefsHelper.getDriverType());

        tvVehicleType = layAppHeader.findViewById(R.id.tv_vehicle_type);
        if (TextUtils.isEmpty(prefsHelper.getDriverType()) && TextUtils.isEmpty(prefsHelper.getVehicleType())) {
            layTypeHolder.setVisibility(View.GONE);
        } else {
            layTypeHolder.setVisibility(View.VISIBLE);
            tvDriverType.setText(prefsHelper.getDriverType());
            if (!TextUtils.isEmpty(prefsHelper.getVehicleType())) {
                if (!TextUtils.isEmpty(prefsHelper.getDriverType())) {
                    tvVehicleType.setText(", " + prefsHelper.getVehicleType());
                } else {
                    tvVehicleType.setText(prefsHelper.getVehicleType());
                }
            }
        }

        tvPlateNo = layAppHeader.findViewById(R.id.plateNo);
        if (!TextUtils.isEmpty(prefsHelper.getUserVehiclePlateNo())
                && !prefsHelper.getUserVehiclePlateNo().equalsIgnoreCase("null")) {
            tvPlateNo.setText(getString(R.string.plateno) + ": " + prefsHelper.getUserVehiclePlateNo());
        }
    }

    private void setDriverStatusTask(final boolean isOnline) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        if (isOnline)
            postParams.put("new_status", "1");
        else
            postParams.put("new_status", "0");

        vHelper.addVolleyRequestListeners(Api.getInstance().changeDriverStatusApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        prefsHelper.edit().putBoolean(PreferenceHelper.OFFLINE, !isOnline).commit();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (!res.getBoolean("status")) {
                                AlertUtils.simpleAlert(MainActivity.this, getString(R.string.error), res.getString("message"),
                                        getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("setStatusTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("setStatusTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(MainActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                    }
                }, "setStatusTask");
    }

    private void getNotificationCountTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(MainActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().notificationApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                int notiCount = 0;
                                JSONArray notArray = res.getJSONArray("all_notifications");
                                for (int i = 0; i < notArray.length(); i++) {
                                    JSONObject eachNot = notArray.getJSONObject(i);
                                    if (!eachNot.getString("status").equals("1")) {
                                        notiCount++;
                                    }
                                }
                                setNotificationBadge(notiCount);
                            }
                        } catch (JSONException e) {
                            Logger.e("getNotificationCountTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getNotificationCountTask error ex", e.getMessage() + " ");
                        }
                        Logger.e("getNOtificationCountTask error msg", errorMsg);
                    }
                }, "getNotificationCountTask");
    }

    private void setNotificationBadge(int notiCount) {
        Logger.e("setNotificationBadgeCount", "Count :" + notiCount);
        if (notiCount > 0) {
            notiMenuItem.setIcon(R.drawable.icon_notification_dot);
        } else {
            notiMenuItem.setIcon(R.drawable.icon_notification);
        }
//        if (Build.VERSION.SDK_INT > 25){
//            notiMenuItem.setIconTintList(null);
//        }
    }

    BalanceUpdateReceiver balanceUpdateReceiver = new BalanceUpdateReceiver();

    public class BalanceUpdateReceiver extends BroadcastReceiver {
        public static final String BALANCE_UPDATE_BROADCAST = "com.emts.comfortgo.BALANCE_UPDATE";

        @Override
        public void onReceive(Context context, Intent intent) {
            setHeaderData();
            Fragment fragment = fm.findFragmentById(R.id.content_frame);
            if (fragment instanceof MyWalletFrag) {
                ((MyWalletFrag) fragment).updateBalance();
            }
        }
    }

    @Override
    protected void onStop() {
        unregisterReceiver(balanceUpdateReceiver);
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(balanceUpdateReceiver, new IntentFilter(BalanceUpdateReceiver.BALANCE_UPDATE_BROADCAST));
    }
}
