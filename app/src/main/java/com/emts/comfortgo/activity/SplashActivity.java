package com.emts.comfortgo.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.emts.comfortgo.BookingParser;
import com.emts.comfortgo.GetBalanceCallback;
import com.emts.comfortgo.R;
import com.emts.comfortgo.fcm.MyFireBaseMessagingService;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.model.Booking;
import com.emts.comfortgo.model.NotificationModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;

import io.nlopez.smartlocation.SmartLocation;


/**
 * Created by Srijana on 7/6/2017.
 */

public class SplashActivity extends AppCompatActivity {
    private final int splashTimer = 3000;
    PreferenceHelper prefHelper;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 342;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getApplicationContext());
        super.onCreate(savedInstanceState);

//        check for fcm system tray notification launch
        if (checkIfFCM()) {
            finish();
            return;
        }

        prefHelper = PreferenceHelper.getInstance(this);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getLocationPermission()) {
                    checkGPSDialog();
                }
            }
        }, splashTimer);

        //update devicePush toke if not
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();
                Logger.e("Splash Device Token", deviceToken);
//                        if (!PreferenceHelper.getInstance(SplashActivity.this).getBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false)) {
                if (prefHelper.isLogin()) {
                    MyFireBaseMessagingService.registerDeviceTokenToServer(deviceToken, SplashActivity.this);
                }
//                        }
            }
        });

    }

    private boolean checkIfFCM() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return false;
        }

        String notType = bundle.getString("note_type");
        //bundle must contain all info sent in "data" field of the notification
        boolean isBookingNotification = false;
        if (notType == null) return false;
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        HashMap<String, String> dataMap = new HashMap<>();
        for (String key : bundle.keySet()) {
            dataMap.put(key, bundle.get(key).toString()); //To Implement
        }
        switch (notType) {
            case NotificationModel.NOT_BOOKING:
                //send notification
                Booking booking = BookingParser.bookingFromPush(dataMap);

                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("booking", booking);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                Logger.e("MyFirebaseActivity Booking Alert",
                        "Activity is not in forGround came from system tray");
                isBookingNotification = true;
                break;
            case NotificationModel.NOT_TYPE_TRIP_COMPLETE:
                //goto trip of the day
                intent = new Intent(getApplicationContext(), TripDayActivity.class);
                intent.putExtra("tripDate", dataMap.get("date"));
                break;
            case NotificationModel.NOT_TYPE_UPDATE_APPROVE:
                //goto view profile
                intent = new Intent(getApplicationContext(), ProfileActivity.class);
                break;
            case NotificationModel.NOT_TYPE_UPDATE_REJECT:
                //goto help center
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("helpCenter", true);
                break;
            case NotificationModel.NOT_TYPE_TRIP_CANCEL:
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("acceptBooking", true); //NOT really accept booking but

                // that extra will open mybookings fragment so reuse
                break;
            case NotificationModel.NOT_ADMIN_ALERT:
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("adminAlert", true);
                break;
            case NotificationModel.NOT_DRIVER_APPROVED:
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                break;
            case NotificationModel.NOT_CREDIT_ACCEPTED:
                return false;//this will stay in splash and proceed
            case NotificationModel.NOT_CREDIT_REJECTED:
                return false;//this will stay in splash and proceed
        }
        startActivity(intent);

        return true;
    }

    private void checkGPSDialog() {
        if (SmartLocation.with(getApplicationContext()).location().state().isGpsAvailable()) {
            if (prefHelper.isLogin()) {
                if (prefHelper.profileComplete()) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
                SplashActivity.this.finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        } else {
            showGPSEnableAlert();
        }
    }


    private boolean getLocationPermission() {
        if (ContextCompat.checkSelfPermission(SplashActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    checkGPSDialog();
                } else {
                    AlertUtils.showToast(getApplicationContext(), getString(R.string.allow_permission_to_proceed));
                    getLocationPermission();
                }
            }
        }
    }

    Dialog dialog;

    private void showGPSEnableAlert() {
        if (dialog != null && dialog.isShowing()) return;
        AlertDialog.Builder alert = new AlertDialog.Builder(SplashActivity.this);
        alert.setCancelable(false);
        alert.setCancelable(false)
                .setTitle(R.string.title_enable_gps)
                .setMessage(R.string.gps_before_procced)
                .setPositiveButton(R.string.proceed, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        checkGPSDialog();
                    }
                })
                .setNegativeButton(R.string.setting, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
        dialog = alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!SmartLocation.with(getApplicationContext()).location().state().isGpsAvailable()) {
            showGPSEnableAlert();
        }
    }


}
