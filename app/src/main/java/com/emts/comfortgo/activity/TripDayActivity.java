package com.emts.comfortgo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.adapter.EachTripAdapter;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.TripModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

public class TripDayActivity extends BookingBaseActivity {
    ArrayList<TripModel> eachTripLists, searchLists;
    EachTripAdapter eachTripAdapter;
    RecyclerView rvEachTripsListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;
    EditText edtBookingNo;
    TextView tvTotalDayEarning, tvTotalTripMade;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_day);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.trip_per_day));

        Intent intent = getIntent();
        String tripDate = intent.getStringExtra("tripDate");
        String totalDayEarning = intent.getStringExtra("eachDayTotal");

        tvTotalDayEarning = findViewById(R.id.tv_total_earning_from_trip);
        if (totalDayEarning != null && !totalDayEarning.equalsIgnoreCase("null")) {
            tvTotalDayEarning.setText(getString(R.string.currency_myr) + " " + totalDayEarning);
        }

        progressBar = findViewById(R.id.progress_bar);
        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
        tvErrorText = findViewById(R.id.error_text);
        edtBookingNo = findViewById(R.id.edt_search_booking_no);

        tvTotalTripMade = findViewById(R.id.tv_total_trip_made);

        rvEachTripsListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvEachTripsListings.setLayoutManager(linearLayoutManager);

        eachTripLists = new ArrayList<>();
        searchLists = new ArrayList<>();
        eachTripAdapter = new EachTripAdapter(TripDayActivity.this, searchLists);
        rvEachTripsListings.setAdapter(eachTripAdapter);

        TextView tvTripDate = findViewById(R.id.tv_trip_date);

        try {
            Date date = sdf.parse(tripDate);
            tvTripDate.setText(sdf1.format(date));
        } catch (ParseException e) {
            tvTripDate.setText(tripDate);
        }
        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            tripDayTasks(tripDate);
        } else {
            AlertUtils.showSnack(TripDayActivity.this, toolbar, getResources().getString(R.string.no_internet));
        }

        edtBookingNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    searchLists.clear();
                    for (int j = 0; j < eachTripLists.size(); j++) {
                        if (eachTripLists.get(j).getTripBookingNo().contains(charSequence)) {
                            searchLists.add(eachTripLists.get(j));
                        }
                    }
                    eachTripAdapter.notifyDataSetChanged();
                } else {
                    searchLists.clear();
                    searchLists.addAll(eachTripLists);
                    eachTripAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void tripDayTasks(String tripDate) {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvEachTripsListings.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("date", tripDate);

        vHelper.addVolleyRequestListeners(Api.getInstance().eachTripListsApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray tripsArray = res.getJSONArray("trips_per_day");
                                searchLists.clear();
                                eachTripLists.clear();

                                double totalTripIncome = 0.0;
                                for (int i = 0; i < tripsArray.length(); i++) {
                                    JSONObject eachTrips = tripsArray.getJSONObject(i);
                                    TripModel tripModel = new TripModel();
                                    tripModel.setTripId(eachTrips.getString("booking_id"));
                                    tripModel.setTripDate(eachTrips.getString("trip_date"));
                                    tripModel.setTripBookingNo(eachTrips.getString("booking_no"));
                                    tripModel.setTripStatus(eachTrips.getString("trip_status"));

                                    tripModel.setTripBookingStatus(eachTrips.getString("booking_status"));

                                    String income = eachTrips.getString("trip_income");
                                    tripModel.setTotalIncome(income);
                                    totalTripIncome = totalTripIncome + Double.parseDouble(income);
                                    eachTripLists.add(tripModel);
                                }
//                            // cancled also
                                JSONArray canceledArray = res.getJSONArray("cancelled_trips_per_day");
                                for (int i = 0; i < canceledArray.length(); i++) {
                                    JSONObject eachTrips = canceledArray.getJSONObject(i);
                                    TripModel tripModel = new TripModel();
                                    tripModel.setTripId(eachTrips.getString("booking_id"));
                                    tripModel.setTripDate(eachTrips.getString("trip_date"));
                                    tripModel.setTripBookingNo(eachTrips.getString("booking_no"));
                                    tripModel.setTripBookingStatus("2");
                                    tripModel.setTripStatus(TripModel.TRIP_CANCELLED);

                                    eachTripLists.add(tripModel);
                                }
                                //sort all trips here by date
                                Collections.sort(eachTripLists, new Comparator<TripModel>() {
                                    @Override
                                    public int compare(TripModel tripModel, TripModel t1) {
                                        if (getDateTime(tripModel.getTripDate()) == null || getDateTime(t1.getTripDate()) == null)
                                            return 0;
                                        return getDateTime(t1.getTripDate()).compareTo(getDateTime(tripModel.getTripDate()));
                                    }
                                });

                                tvTotalTripMade.setText(String.valueOf(tripsArray.length()));
                                searchLists.addAll(eachTripLists);

                                if (searchLists.size() > 0) {
                                    eachTripAdapter.notifyDataSetChanged();
                                    rvEachTripsListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                } else {
                                    tvErrorText.setText(getString(R.string.trip_not_available));
                                    tvErrorText.setVisibility(View.VISIBLE);
                                    rvEachTripsListings.setVisibility(View.GONE);
                                }
                            } else {
                                rvEachTripsListings.setVisibility(View.GONE);
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("tripDayTasks json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("tripDayTasks error ex", e.getMessage() + " ");
                        }
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        rvEachTripsListings.setVisibility(View.GONE);
                    }
                }, "tripDayTasks");
    }

    private Date getDateTime(String tripDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(tripDate);
        } catch (ParseException e) {
            return null;
        }
    }
}
