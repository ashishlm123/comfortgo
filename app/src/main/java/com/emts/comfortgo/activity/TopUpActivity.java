package com.emts.comfortgo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.adapter.TopUpCreditAdapter;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.CreditsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TopUpActivity extends BookingBaseActivity {
    ArrayList<CreditsModel> creditLists;
    TopUpCreditAdapter topUpCreditAdapter;
    RecyclerView rvCreditListings;
    ProgressBar progressBar;
    TextView tvErrorText;
    ScrollView scrollView;
    TextView tvCreditBalance, tvBankName, tvBankAcNo, tvAccountName;
    JSONArray creditsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(R.string.top_up_credit);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = findViewById(R.id.progress_bar);
        tvErrorText = findViewById(R.id.tv_error_text);
        scrollView = findViewById(R.id.scroll_view);

        tvCreditBalance = findViewById(R.id.tv_credit_balance);
        tvCreditBalance.setText(getString(R.string.currency_myr) + " " + PreferenceHelper.getInstance(getApplicationContext())
                .getUserCreditBalance());
        tvAccountName = findViewById(R.id.tv_bank_account_name);
        tvBankName = findViewById(R.id.tv_bank_name);
        tvBankAcNo = findViewById(R.id.tv_bank_account_no);
        //this is comfort go's bank detail so show this-->
        tvBankName.setText(getString(R.string.public_bank));
        tvBankAcNo.setText("3210408536");
        tvAccountName.setText(getString(R.string.comofrt_go_sdn_bhd));

        rvCreditListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvCreditListings.setLayoutManager(linearLayoutManager);

        creditLists = new ArrayList<>();
        topUpCreditAdapter = new TopUpCreditAdapter(TopUpActivity.this, creditLists);
        rvCreditListings.setAdapter(topUpCreditAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            getCreditDetailsTask();
        } else {
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            rvCreditListings.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }

        Button btnTopUpRequest = findViewById(R.id.btn_request_top_up);
        btnTopUpRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RequestTopUpActivity.class);
                intent.putExtra("credits", creditsArray.toString());
                TopUpActivity.this.finish();
                startActivity(intent);
            }
        });
    }

    private void getCreditDetailsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);

        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();

        volleyHelper.addVolleyRequestListeners(Api.getInstance().creditDetailsApi,
                Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                                creditsArray = res.getJSONArray("packages");
                                if (creditsArray.length() > 0) {
                                    for (int i = 0; i < creditsArray.length(); i++) {
                                        JSONObject eachObject = creditsArray.getJSONObject(i);
                                        CreditsModel creditsModel = new CreditsModel();
                                        creditsModel.setId(eachObject.getString("id"));
                                        creditsModel.setCreditPrice(eachObject.getString("amount"));
                                        creditsModel.setCreditQty(eachObject.getString("credits"));
                                        creditsModel.setPackageName(eachObject.getString("package_name"));
                                        creditLists.add(creditsModel);
                                    }
                                    scrollView.setVisibility(View.VISIBLE);
                                    topUpCreditAdapter.notifyDataSetChanged();
                                    rvCreditListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);

//                                    JSONObject bankDetails = res.getJSONObject("bank_details");
//                                    tvBankName.setText(bankDetails.getString("bank_name"));
//                                    tvBankAcNo.setText(bankDetails.getString("ac_no"));
//                                    tvAccountName.setText(bankDetails.getString("recipient_name"));
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    tvErrorText.setText(res.getString("message"));
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                scrollView.setVisibility(View.GONE);
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("getCreditDetailsTask json ex", e.getMessage());
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        scrollView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        rvCreditListings.setVisibility(View.GONE);
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getCreditDetailsTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(TopUpActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                }, "getCreditDetailsTask");

    }

}
