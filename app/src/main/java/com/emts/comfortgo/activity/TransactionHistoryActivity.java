package com.emts.comfortgo.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;
import com.emts.comfortgo.adapter.TransactionAdapter;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.TransactionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TransactionHistoryActivity extends BookingBaseActivity {
    ArrayList<TransactionModel> transactionHistoryLists;
    TransactionAdapter transactionAdapter;
    RecyclerView rvTransactionHistoryListings;
    ProgressBar progressBar;
    TextView tvErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.transaction_history));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = findViewById(R.id.progress_bar);
        tvErrorText = findViewById(R.id.error_text);

        rvTransactionHistoryListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvTransactionHistoryListings.setLayoutManager(linearLayoutManager);

        transactionHistoryLists = new ArrayList<>();
        transactionAdapter = new TransactionAdapter(TransactionHistoryActivity.this, transactionHistoryLists);
        rvTransactionHistoryListings.setAdapter(transactionAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            transactionHistoryListingsTask();
        } else {
            progressBar.setVisibility(View.GONE);
            rvTransactionHistoryListings.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void transactionHistoryListingsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvTransactionHistoryListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().transactionHistoryApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject jsonObj = res.getJSONObject("transaction_history");
                                JSONArray transactionArray = jsonObj.getJSONArray("top_up");

                                if (transactionArray.length() > 0) {
                                    for (int i = 0; i < transactionArray.length(); i++) {
                                        JSONObject eachTransaction = transactionArray.getJSONObject(i);
                                        TransactionModel transactionModel = new TransactionModel();
                                        transactionModel.setId(eachTransaction.getString("id"));
                                        transactionModel.setTxnDate(eachTransaction.getString("transaction_date"));
                                        transactionModel.setTxnType(eachTransaction.getString("txn_type"));

                                        if (eachTransaction.has("top_up_id")) {
                                            transactionModel.setTopUpID(eachTransaction.getString("top_up_id"));
                                        }
                                        if (eachTransaction.has("credit_purchase")) {
                                            transactionModel.setCreditPurchaseAmt(eachTransaction.getString("credit_purchase"));
                                        }
                                        if (eachTransaction.has("status")) {
                                            transactionModel.setTxnStatus(eachTransaction.getString("status"));
                                        }
                                        if (eachTransaction.has("booking_no")) {
                                            transactionModel.setBookingNo(eachTransaction.getString("booking_no"));
                                        }

                                        transactionModel.setReceiptImage(eachTransaction.optString("receipt_image"));
                                        transactionModel.setCreditPackageName(eachTransaction.optString("package_name"));
                                        transactionModel.setCommissionDeductionAmt(eachTransaction.optString("deduction_amt"));
                                        transactionHistoryLists.add(transactionModel);
                                    }
                                    transactionAdapter.notifyDataSetChanged();
                                    rvTransactionHistoryListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                } else {
                                    rvTransactionHistoryListings.setVisibility(View.GONE);
                                    tvErrorText.setText(getString(R.string.no_transactions));
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                rvTransactionHistoryListings.setVisibility(View.GONE);
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("transactionHistoryListingsTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        rvTransactionHistoryListings.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("transactionHistoryListingsTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(TransactionHistoryActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                }, "transactionHistoryListingsTask");
    }

}
