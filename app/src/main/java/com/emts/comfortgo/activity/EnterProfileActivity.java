package com.emts.comfortgo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.R;

public class EnterProfileActivity extends BookingBaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.profile));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        CardView menuProfile, menuChangePassword;
        menuProfile = findViewById(R.id.menu_profile);
        TextView tvProfile = menuProfile.findViewById(R.id.tv_menu_title);
        tvProfile.setText(getString(R.string.my_profile));
        menuProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });

        menuChangePassword = findViewById(R.id.menu_change_password);
        TextView tvChangePassword = menuChangePassword.findViewById(R.id.tv_menu_title);
        tvChangePassword.setText(getString(R.string.change_password));
        menuChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
    }
}
