package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.BookingBaseActivity;
import com.emts.comfortgo.GetBalanceCallback;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.VolleyHelper;
import com.emts.comfortgo.model.Booking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class DropOffConfirmActivity extends BookingBaseActivity {
    TextView tvFixedFare;
    ProgressDialog pDialog;
    TextWatcher textWatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_confirm_dropoff);

        Intent intent = getIntent();
        if (intent == null) return;
        final Booking booking = (Booking) intent.getSerializableExtra("booking");
        if (booking == null) return;

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tvToolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        tvToolbarTitle.setText(getString(R.string.drop_off));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        View holderFixedFare, holderMeteredFare;
        holderFixedFare = findViewById(R.id.holderFixedFare);
        tvFixedFare = holderFixedFare.findViewById(R.id.tvFixedFare);

        holderMeteredFare = findViewById(R.id.holderMeteredFare);
        final EditText etMeteredFare = holderMeteredFare.findViewById(R.id.etMeteredFare);

        final EditText etSurCharge = findViewById(R.id.etSurCharge);

        View holderBookingFee = findViewById(R.id.holderBookingFee);
        TextView tvBookingFee = holderBookingFee.findViewById(R.id.tvBookingFee);

        final TextView tvTotalFare = findViewById(R.id.tvTotalFare);

        Button btnConfirm = findViewById(R.id.btnConfirmDropoff);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
//                if (booking.getFareType().equalsIgnoreCase(Booking.FARE_TYPE_METERED)) {
//                    if (TextUtils.isEmpty(etMeteredFare.getText().toString().trim())) {
//                        AlertUtils.showSnack(DropOffConfirmActivity.this, view, "Please enter the metered fare");
//                        return;
//                    }
//                }

                if (pDialog == null) {
                    pDialog = AlertUtils.showProgressDialog(DropOffConfirmActivity.this, getString(R.string.please_wait));
                } else {
                    pDialog.show();
                }

                final SmartLocation smartLocation = SmartLocation.with(DropOffConfirmActivity.this);
                final SmartLocation.LocationControl locationControl = smartLocation.location();
                if (locationControl.state().isGpsAvailable()) {
                    locationControl.oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            if (NetworkUtils.isInNetwork(DropOffConfirmActivity.this)) {
                                endTripTask(location.getLatitude(), location.getLongitude(), booking,
                                        etMeteredFare.getText().toString().trim(), etSurCharge.getText().toString().trim());
                                locationControl.stop();
                            } else {
                                AlertUtils.showSnack(DropOffConfirmActivity.this, view, getString(R.string.no_internet));
                            }
                        }
                    });
                } else {
                    AlertUtils.simpleAlert(DropOffConfirmActivity.this, getString(R.string.title_enable_gps),
                            getString(R.string.gps_before_procced), getString(R.string.ok), "", null);
                }
            }
        });

        if (booking.getFareType().equals(Booking.FARE_TYPE_FIXED)) {
            tvFixedFare.setText(String.format("%.2f", Double.parseDouble(booking.getTripFare())));
            tvTotalFare.setText(getString(R.string.currency_rm) + " " + String.format("%.2f", Double.parseDouble(booking.getTripFare())));

            holderFixedFare.setVisibility(View.VISIBLE);
            holderMeteredFare.setVisibility(View.GONE);
            holderBookingFee.setVisibility(View.GONE);
        } else {
            tvBookingFee.setText(String.format("%.2f", Double.parseDouble(booking.getBookingFee())) + " ");
            tvTotalFare.setText(getString(R.string.currency_rm) + " " + String.format("%.2f", Double.parseDouble(booking.getBookingFee())));

            holderFixedFare.setVisibility(View.GONE);
            holderMeteredFare.setVisibility(View.VISIBLE);
            holderBookingFee.setVisibility(View.VISIBLE);
        }


        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Logger.e("onTextChanged", "called : " + charSequence);
                double meteredFare = 0.0, surCharge = 0.0;
                if (etMeteredFare.getText().toString().trim().length() > 0) {
                    if (!etMeteredFare.getText().toString().startsWith(".")) {
                        meteredFare = Double.parseDouble(etMeteredFare.getText().toString().trim());
                    } else {
                        meteredFare = Double.parseDouble(booking.getTripFare());
                    }
//                    etMeteredFare.removeTextChangedListener(textWatcher);
//                    etMeteredFare.setText(String.format("%.2f", meteredFare));
//                    etMeteredFare.addTextChangedListener(textWatcher);
                } else {
                    //if amount is not entered use min fare
                    meteredFare = Double.parseDouble(booking.getTripFare());
                }
                if (etSurCharge.getText().toString().trim().length() > 0) {
                    if (!etSurCharge.getText().toString().startsWith(".")) {
                        surCharge = Double.parseDouble(etSurCharge.getText().toString().trim());
                    }
//                    etSurCharge.removeTextChangedListener(textWatcher);
//                    etSurCharge.setText(String.format("%.2f", surCharge));
//                    etSurCharge.addTextChangedListener(textWatcher);
                }

                double total = 0.0;
                if (booking.getFareType().equalsIgnoreCase(Booking.FARE_TYPE_FIXED)) {
                    total = Double.parseDouble(booking.getTripFare()) + surCharge;
                } else {
                    total = Double.parseDouble(booking.getBookingFee()) + surCharge + meteredFare;
                }
                tvTotalFare.setText(getString(R.string.currency_rm) + " " + String.format("%.2f", total));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        etSurCharge.addTextChangedListener(textWatcher);
        etMeteredFare.addTextChangedListener(textWatcher);
        etMeteredFare.setText(""); //required here to calculate total

        etSurCharge.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (etSurCharge.getText().toString().trim().length() > 0) {
                    if (!etSurCharge.getText().toString().startsWith(".")) {
                        double surCharge = Double.parseDouble(etSurCharge.getText().toString().trim());

                        etSurCharge.removeTextChangedListener(textWatcher);
                        etSurCharge.setText(String.format("%.2f", surCharge));
                        etSurCharge.addTextChangedListener(textWatcher);
                    }
                }
            }
        });

        etMeteredFare.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (etMeteredFare.getText().toString().trim().length() > 0) {
                    if (!etMeteredFare.getText().toString().startsWith(".")) {
                        double meteredFare = Double.parseDouble(etMeteredFare.getText().toString().trim());

                        etMeteredFare.removeTextChangedListener(textWatcher);
                        etMeteredFare.setText(String.format("%.2f", meteredFare));
                        etMeteredFare.addTextChangedListener(textWatcher);
                    }
                }
            }
        });
    }

    private void endTripTask(double latitude, double longitude, Booking booking, String meterFare, String surcharge) {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(DropOffConfirmActivity.this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));
        postParams.put("action", "end");
        if (!TextUtils.isEmpty(surcharge)) {
            postParams.put("surcharge", surcharge);
        }
        if (!TextUtils.isEmpty(meterFare)) {
            postParams.put("metered_fare", meterFare);
        }

        volleyHelper.addVolleyRequestListeners(Api.getInstance().startEndTripApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                emitJobAccept(response);
                                //goto home
                                final String successMsg = responseData.getString("message");
                                GetBalanceCallback.getMyBalanceTask(getApplicationContext(), new GetBalanceCallback.BalanceSuccessCallback() {
                                    @Override
                                    public void onBalance(boolean isSuccess, String msg) {
                                        if (isSuccess) {
                                            AlertUtils.simpleAlert(DropOffConfirmActivity.this, false,
                                                    getString(R.string.trip_completed), successMsg, getString(R.string.ok),
                                                    "", new AlertUtils.OnAlertButtonClickListener() {
                                                        @Override
                                                        public void onAlertButtonClick(boolean isPositiveButton) {
                                                            setResult(RESULT_OK);
                                                            DropOffConfirmActivity.this.finish();
                                                        }
                                                    });
                                        }
                                    }
                                });
                            } else {
                                AlertUtils.simpleAlert(DropOffConfirmActivity.this, false, getString(R.string.error),
                                        responseData.getString("message"), getString(R.string.ok),
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("endTripTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("endTripTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(DropOffConfirmActivity.this, getString(R.string.error),
                                errorMsg, getString(R.string.ok),
                                "", null);
                    }
                }, "endTripTask");

    }
}
