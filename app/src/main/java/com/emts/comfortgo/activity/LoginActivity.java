package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.ComfortBaseActivity;
import com.emts.comfortgo.MyInput1;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Srijana on 7/6/2017.
 */

public class LoginActivity extends ComfortBaseActivity {
    MyInput1 edtMobile, edtPassWord;
    TextView errorMobile, errorPassword, tvSignUp;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getApplicationContext());
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        checkVersion();
        prefsHelper = PreferenceHelper.getInstance(this);

        Button btnLogin = findViewById(R.id.btn_login);
        final TextView tvForgotPassword = findViewById(R.id.tv_forgot_password);
        edtMobile = findViewById(R.id.edt_mobile);
        edtPassWord = findViewById(R.id.edt_password);

        errorMobile = findViewById(R.id.error_email);
        errorPassword = findViewById(R.id.error_password);
        tvSignUp = findViewById(R.id.tv_sign_up);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });

        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String deviceId = instanceIdResult.getToken();
                                loginTask(deviceId);
                            }
                        });
                    } else {
                        AlertUtils.hideInputMethod(LoginActivity.this, view);
                        AlertUtils.simpleAlert(LoginActivity.this, getString(R.string.error), getString(R.string.no_internet));
                    }
                }
            }
        });

        CheckBox cbRemember = findViewById(R.id.cbRememberMe);
        cbRemember.setTypeface(ResourcesCompat.getFont(getApplicationContext(), R.font.gotham_medium));
    }

    private void loginTask(String deviceId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("mobile_no", edtMobile.getText().toString().trim());
        postParams.put("password", edtPassWord.getText().toString().trim());
        postParams.put("device_id", deviceId);

        vHelper.addVolleyRequestListeners(Api.getInstance().loginApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();
                                if (res.getBoolean("profile_completeness")) {
                                    JSONObject userObject = res.getJSONObject("user_info");
                                    prefsHelper.edit().putBoolean(PreferenceHelper.OFFLINE, false).commit();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    SharedPreferences.Editor editor = prefsHelper.edit();
                                    editor.putString(PreferenceHelper.USER_ID, userObject.getString("id"));
                                    editor.putString(PreferenceHelper.USER_TOKEN, userObject.getString("user_token"));
                                    editor.putString(PreferenceHelper.USER_EMAIL, userObject.getString("email"));
                                    editor.putString(PreferenceHelper.DRIVER_ID, userObject.getString("user_name"));
                                    editor.putString(PreferenceHelper.USER_FULL_NAME, userObject.getString("full_name"));
                                    editor.putString(PreferenceHelper.USER_MOBILE_NO, userObject.getString("mobile_no"));
                                    editor.putString(PreferenceHelper.USER_VEHICLE_PLATE_NO, userObject.getString("plate_no"));
                                    editor.putString(PreferenceHelper.VEHICLE_TYPE, userObject.getString("v_type"));
                                    editor.putString(PreferenceHelper.DRIVER_TYPE, userObject.getString("d_type"));
                                    editor.putString(PreferenceHelper.USER_CREDIT_BALANCE, userObject.getString("credit_balance"));
                                    String profilePic = userObject.getString("profile_photo");
                                    if (!TextUtils.isEmpty(profilePic) && !profilePic.equalsIgnoreCase("null")) {
                                        editor.putString(PreferenceHelper.USER_PROFILE_PIC, userObject.getString("directory")
                                                + profilePic);
                                    }
                                    editor.commit();

                                    prefsHelper.edit().putBoolean(PreferenceHelper.PROFILE_COMPLETENESS, true).apply();
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else {
                                    if (res.getString("otp_status").equals("1")) {

                                        Intent intent = new Intent(getApplicationContext(), CompleteEditProfile.class);

                                        JSONArray languageArray = res.getJSONArray("language");
                                        JSONArray cityArray = res.getJSONArray("city");
                                        JSONArray raceArray = res.getJSONArray("race");
                                        JSONArray bankArray = res.getJSONArray("banks");

                                        intent.putExtra("languages", languageArray.toString());
                                        intent.putExtra("cities", cityArray.toString());
                                        intent.putExtra("races", raceArray.toString());
                                        intent.putExtra("banks", bankArray.toString());

                                        prefsHelper.edit().putBoolean(PreferenceHelper.PROFILE_COMPLETENESS, false).apply();

                                        prefsHelper.edit().putString(PreferenceHelper.USER_ID, res.getString("user_info")).apply();
                                        prefsHelper.edit().putString(PreferenceHelper.USER_TOKEN, res.getString("user_token")).apply();
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(getApplicationContext(), OtpVerificationActivity.class);
                                        prefsHelper.edit().putString(PreferenceHelper.USER_ID, res.getString("user_info")).apply();
                                        prefsHelper.edit().putString(PreferenceHelper.USER_TOKEN, res.getString("user_token")).apply();
                                        prefsHelper.edit().putBoolean(PreferenceHelper.PROFILE_COMPLETENESS, false).apply();
                                        startActivity(intent);
                                    }
                                }
                            } else {
                                final String errorMsg = res.getString("message");
                                AlertUtils.simpleAlert(LoginActivity.this, getString(R.string.error), errorMsg,
                                        getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("loginTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("loginTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(LoginActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                    }
                }, "loginTask");
    }

    private boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(edtMobile.getText().toString().trim())) {
            isValid = false;
            errorMobile.setVisibility(View.VISIBLE);
            errorMobile.setText(R.string.error_field_required);
        } else {
            errorMobile.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(edtPassWord.getText().toString().trim())) {
            isValid = false;
            errorPassword.setVisibility(View.VISIBLE);
            errorPassword.setText(R.string.error_field_required);
        } else {

            if (edtPassWord.getText().toString().trim().length() >= 6) {
                errorPassword.setVisibility(View.GONE);
            } else {
                isValid = false;
                errorPassword.setText(getString(R.string.password_should_be_more_than_six));
                errorPassword.setVisibility(View.VISIBLE);
            }
        }

        return isValid;
    }

    //checking if the password is alphanumeric or not
    public static boolean containAlphanumeric(final String str) {
        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=()!?<>';:|`~*/]).{5,13}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z]).{5,13}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }
}
