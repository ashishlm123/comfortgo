package com.emts.comfortgo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.comfortgo.R;
import com.emts.comfortgo.helper.AlertUtils;
import com.emts.comfortgo.helper.Api;
import com.emts.comfortgo.helper.Logger;
import com.emts.comfortgo.helper.NetworkUtils;
import com.emts.comfortgo.helper.PreferenceHelper;
import com.emts.comfortgo.helper.VolleyHelper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class OtpVerificationActivity extends AppCompatActivity {
    EditText etOtp1, etOtp2, etOtp3, etOtp4, etOtp5, etOtp6;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        prefsHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(getString(R.string.otp_verificaiton));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etOtp1 = findViewById(R.id.etOtp1);
        etOtp2 = findViewById(R.id.etOtp2);
        etOtp3 = findViewById(R.id.etOtp3);
        etOtp4 = findViewById(R.id.etOtp4);
        etOtp5 = findViewById(R.id.etOtp5);
        etOtp6 = findViewById(R.id.etOtp6);

        etOtp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etOtp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etOtp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etOtp3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etOtp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etOtp4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etOtp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etOtp5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etOtp5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etOtp6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (NetworkUtils.isInNetwork(getApplication())) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String deviceId = instanceIdResult.getToken();
                    generateOtpTask(deviceId);
                }
            });
        } else {
            AlertUtils.showSnack(OtpVerificationActivity.this, etOtp1, getResources().getString(R.string.no_internet));
        }

        Button btnVerify = findViewById(R.id.btn_verify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(getApplication())) {
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String deviceId = instanceIdResult.getToken();
                                otpVerifyTask(deviceId);
                            }
                        });
                    } else {
                        AlertUtils.showSnack(OtpVerificationActivity.this, etOtp1, getResources().getString(R.string.no_internet));
                    }
                }
            }
        });

        TextView tvResend = findViewById(R.id.tv_resend);
        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                        @Override
                        public void onSuccess(InstanceIdResult instanceIdResult) {
                            String deviceId = instanceIdResult.getToken();
                            generateOtpTask(deviceId);
                        }
                    });
                } else {
                    AlertUtils.showSnack(OtpVerificationActivity.this, etOtp1, getResources().getString(R.string.no_internet));
                }
            }
        });

    }

    private void otpVerifyTask(String deviceID) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(OtpVerificationActivity.this,
                getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("user_id", prefsHelper.getUserId());
        postParams.put("user_token", prefsHelper.getUserToken());
        postParams.put("device_id", deviceID);
        postParams.put("otp", etOtp1.getText().toString().trim() + etOtp2.getText().toString().trim() +
                etOtp3.getText().toString().trim() + etOtp4.getText().toString().trim() +
                etOtp5.getText().toString().trim() + etOtp6.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().verifyOtp, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showToast(OtpVerificationActivity.this, res.getString("message"));
                                Intent intent = new Intent(getApplicationContext(), CompleteEditProfile.class);

                                JSONArray languageArray = res.getJSONArray("language");
                                JSONArray cityArray = res.getJSONArray("city");
                                JSONArray raceArray = res.getJSONArray("race");
                                JSONArray bankArray = res.getJSONArray("banks");

                                intent.putExtra("languages", languageArray.toString());
                                intent.putExtra("cities", cityArray.toString());
                                intent.putExtra("races", raceArray.toString());
                                intent.putExtra("banks", bankArray.toString());

                                startActivity(intent);
                            } else {
                                AlertUtils.showSnack(OtpVerificationActivity.this, etOtp1, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("otpVerifyTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("otpVerifyTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(OtpVerificationActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                    }
                }, "otpVerifyTask");
    }

    private void generateOtpTask(String deviceId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(OtpVerificationActivity.this, getString(R.string.please_wait));

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("user_id", prefsHelper.getUserId());
        postParams.put("user_token", prefsHelper.getUserToken());
        postParams.put("device_id", deviceId);

        vHelper.addVolleyRequestListeners(Api.getInstance().getOtp, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

//                        AlertUtils.simpleAlert(OtpVerificationActivity.this, "Success",
//                                "To:do, use " + res.getString("otp") + " for now ", "OK",
//                                "", null);
                                AlertUtils.simpleAlert(OtpVerificationActivity.this, getString(R.string.success),
                                        res.getString("message"), getString(R.string.ok),
                                        "", null);
                            } else {
                                AlertUtils.simpleAlert(OtpVerificationActivity.this, getString(R.string.error),
                                        res.getString("message"), getString(R.string.ok), "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("generateOtpTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = getString(R.string.unexpected_error_try_again);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("generateOtpTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(OtpVerificationActivity.this, getString(R.string.error), errorMsg,
                                getString(R.string.ok), "", null);
                    }
                }, "generateOtpTask");
    }

    private boolean validation() {
        boolean isValid;

        if ((etOtp1.getText().toString().trim().length() + etOtp2.getText().toString().trim().length() +
                etOtp3.getText().toString().trim().length() + etOtp4.getText().toString().trim().length() +
                etOtp5.getText().toString().trim().length() + etOtp6.getText().toString().trim().length()) == 6) {
            isValid = true;
        } else {
            isValid = false;
            AlertUtils.showSnack(OtpVerificationActivity.this, etOtp1, getString(R.string.all_fields_required));
        }

        return isValid;
    }
}
