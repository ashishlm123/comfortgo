package com.emts.comfortgo.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {
    private static String apiVersion = "v1.0";
    private static Api api;

    public final String apiKey = "sdkfhiuews354983888";

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }
    }

//    private String getHost() {
//        return "http://202.166.198.151:8888/";
//    }

    private String getHost() {
//        return "http://nepaimpressions.com/dev/";
        return "http://ugbattle.com";
    }

    public String socketUrl = "http://202.166.198.151:3030";
//    public String socketUrl = "http://13.229.126.174:8888";

    private String getApi() {
        return getHost() + "/app/"; //local api loc
//        return getHost() + "comfort_go/app/"; //local api loc
    }

    public final String loginApi = getApi() + "users/login";
    public final String signUpApi = getApi() + "users/signup";
    public final String getOtp = getApi() + "users/send_otp";
    public final String verifyOtp = getApi() + "users/verify_otp";
    public final String forgotPasswordApi = getApi() + "users/forget_password";
    public final String changePasswordApi = getApi() + "mycontroller/change_password";
    public final String viewProfileApi = getApi() + "mycontroller/view_profile";
    public final String editProfileApi = getApi() + "mycontroller/edit_profile";
    public final String changeDriverStatusApi = getApi() + "mycontroller/change_status";
    public final String logoutApi = getApi() + "mycontroller/logout";
    public final String faqApi = getApi() + "mycontroller/get_faq";
    public final String termsAndConditionsApi = getApi() + "mycontroller/get_terms_condition";
    public final String emergencyHotLineApi = getApi() + "mycontroller/get_emergency_hotlines";
    public final String helpCenterApi = getApi() + "mycontroller/send_help_notification";
    public final String getUserWalletBalance = getApi() + "mycontroller/get_earnings";
    public final String requestCashOut = getApi() + "mycontroller/request_cashout";
    public final String cashOutHistory = getApi() + "mycontroller/cashout_history";
    public final String tripHistoryApi = getApi() + "booking/trips_datewise";
    public final String eachTripListsApi = getApi() + "booking/trips_per_day";
    public final String tripDetailsApi = getApi() + "booking/trips_history_detail";
    public final String updatePushToken = getApi() + "mycontroller/update_device_id";
    public final String acceptBooking = getApi() + "booking/accept_booking";
    public final String rejectBooking = getApi() + "booking/reject_booking";
    public final String cancelBooking = getApi() + "booking/cancel_trip";
    public final String getMyBookingsApi = getApi() + "mycontroller/my_trip";
    public final String creditDetailsApi = getApi() + "mycontroller/get_package_details";
    public final String transactionHistoryApi = getApi() + "mycontroller/transaction_history";
    public final String requestTopupApi = getApi() + "mycontroller/request_topup";

    public final String arriveForPickUp = getApi() + "booking/arrive_for_pickup";
    public final String arriveForDropOff = getApi() + "booking/arrive_for_dropoff";
    public final String startEndTripApi = getApi() + "booking/start_end_trip";

    public final String locationUpdateApi = getApi() + "mycontroller/update_driver_location";
    public final String versionCheckApi = getApi() + "check_version";
    public final String aboutUsApi = getApi() + "mycontroller/about_us";
    public final String termsOfServicesApi = getApi() + "mycontroller/get_terms_condition";
    public final String notificationApi = getApi() + "mycontroller/my_notifications";
    public final String notReadUnReadApi = getApi() + "mycontroller/read_unread_notification";
    public final String autoCloseApi = getApi() + "mycontroller/auto_close_booking";

    public final String getMyBalanceApi = getApi() + "mycontroller/get_my_balance";

}